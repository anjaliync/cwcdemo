import re, string

# numbers as words (1-10)
nw = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']

# augments a response based on the planner query
def augment(query, response):
    # no augmentation needed for not-connected
    if 'not-connected' in query.split('\\s+')[0].lower():
      return response

    (parameters, ppresponse) = getNumbers(response)

    # no or multiple numbers in response
    if len(parameters) < 1 or len(parameters) > 2:
      return None

    query = query.replace('(','').replace(')','')

    name = query.split()[1]   # shape name
    dim  = query.split()[3]   # requested parameter name

    # reject if user described square with mismatched height/width values
    if name == 'square' and len(parameters) == 2 and parameters[0] != parameters[1]:
      return None

    # reject if row or column has multiple parameters
    if (name == 'row' or name == 'column') and len(parameters) > 1:
      return None

    # single-value size parameters for squares 
    if dim == 'size' and name == 'square':
      return "Build a "+str(parameters[0])+" by "+str(parameters[0])+" "+name+"."

    # single-value size parameters for new shapes: single parameter or multiple equal parameters detected in input
    if dim == 'size' and name != 'rectangle' and len(parameters) == 1:
      return "Build a "+name+" of "+dim+" "+str(parameters[0])+"."

    # allows for new shapes with one parameter 
    if name != 'square' and dim != 'size':
      return "Build a "+name+" of "+dim+" "+str(parameters[0])+"."

    # get the ordering of height/width labels
    labels = getLabels(ppresponse)
    if len(labels) > 2 or (name == 'rectangle' and len(parameters) < 2):
      return None

    sfx = (" and "+labels[1]+" "+str(parameters[1])+" blocks." if len(parameters) > 1 else ".")
    return "Build a "+name+" of "+labels[0]+" "+str(parameters[0])+" blocks"+sfx

# gets the list of numbers in a response. also returns the formatted response string 
# where punctuation is removed and %dx%d-type values are broken up using spaces
def getNumbers(response):
    response = response.translate(None, string.punctuation)

    # find %dx%d values and separate
    matches = re.findall('[0-9]+x[0-9]+', response)  
    for m in matches:
        response = separate(response, m)

    return ([s for s in response.split() if isNumber(s)], response)

# gets the height/width labels for each of the arguments in the parameter list
def getLabels(response):
    tokens = response.split()
    parameters = []

    # user specified ordering explicitly (using 'height/width' or 'tall/wide')
    for token in tokens:
      if token.lower() == 'height' or token.lower() == 'width':
        parameters.append(token.lower())
      elif token.lower() == 'tall':
        parameters.append('height')
      elif token.lower() == 'wide':
        parameters.append('width')

    if len(parameters) == 2:
      return parameters

    # "%x x %y" and "%x by %y" means width = %x, height = %y
    for i in range(len(tokens)-1):
      if isNumber(tokens[i]) and (tokens[i+1].lower() == 'x' or tokens[i+1].lower() == 'by'):
        parameters.append('width')
        parameters.append('height')

    if len(parameters) == 2:
      return parameters

    # %x blocks by %y blocks means width = %x, height = %y
    for i in range(len(tokens)-2):
      if isNumber(tokens[i]) and tokens[i+1].lower() == 'blocks' and tokens[i+2].lower() == 'by':
        parameters.append('width')
        parameters.append('height')

    if len(parameters) == 2:
      return parameters

    # everything else failed, so just assume height comes first (!!! this is a big assumption when it comes to new shapes)
    return ['width', 'height']

# determines whether or not input token is a numeric or number word
def isNumber(token):
  return token.isdigit() or token in nw

# separates %dx%d into %d x %d
def separate(response, substring):
    return response.replace(substring, substring.split('x')[0]+' x '+substring.split('x')[1])
