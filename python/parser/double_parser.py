from __future__ import print_function
import tensorflow as tf
import pickle, sys, re
import config, data_utils, pointer_parser_model, clarification_heuristic, parser_model


if len(sys.argv) != 6:
    print("Usage: python parse_input.py <in-vocab model> <in-vocab conf> <out-vocab model> <out-vocab conf> <output file>")
    sys.exit(1)

in_vocab_model_path, in_vocab_conf_path, out_vocab_model_path, out_vocab_conf_path, output_path = sys.argv[1:6]
in_vocab_conf_in = open(in_vocab_conf_path, 'r')
in_vocab_conf = pickle.load(in_vocab_conf_in)
in_vocab_conf_in.close()
out_vocab_conf_in = open(out_vocab_conf_path, 'r')
out_vocab_conf = pickle.load(out_vocab_conf_in)
out_vocab_conf_in.close()

in_vocab_graph = tf.Graph()
in_vocab_sess = tf.Session(graph=in_vocab_graph)
with in_vocab_sess.as_default():
    with in_vocab_graph.as_default():
        in_vocab_model = parser_model.MultiSentParseModel(in_vocab_conf, None)
        in_vocab_sess.run(tf.initialize_all_variables())
        in_vocab_model.saver.restore(in_vocab_sess, in_vocab_model_path)

out_vocab_graph = tf.Graph()
out_vocab_sess = tf.Session(graph=out_vocab_graph)
with out_vocab_sess.as_default():
    with out_vocab_graph.as_default():
        out_vocab_model = pointer_parser_model.MultiSentPointerParseModel(out_vocab_conf, None)
        out_vocab_sess.run(tf.initialize_all_variables())
        out_vocab_model.saver.restore(out_vocab_sess, out_vocab_model_path)
prev_in_vocab_states = []
prev_out_vocab_states = []
state_num = 0
prev_parses = []

print("\n")
print('ready')
while True:
    line = sys.stdin.readline()
    print("RECEIVED INPUT1:"+line)
    flag = int(line.strip())
    if flag == 1:
        in_vocab_model.prev_encoder_state = None
        out_vocab_model.prev_encoder_state = None
        prev_in_vocab_states = []
        prev_out_vocab_states = []
        state_num = 0
        prev_parses = []
    elif flag == 2:
        line = sys.stdin.readline()
        delete_num = int(line.strip())
        delete_inds = range(state_num-delete_num, state_num)
        state_num -= delete_num
        print("DELETING PARSES: ",prev_parses[(-1*delete_num):])
        prev_parses = prev_parses[:(-1*delete_num)]
        while len(prev_in_vocab_states) > 0 and prev_in_vocab_states[-1][0] in delete_inds:
            del prev_in_vocab_states[-1]
        while len(prev_out_vocab_states) > 0 and prev_out_vocab_states[-1][0] in delete_inds:
            del prev_out_vocab_states[-1]
        if len(prev_in_vocab_states) > 0:
            in_vocab_model.prev_encoder_state = prev_in_vocab_states[-1][1]
        else:
            in_vocab_model.prev_encoder_state = None
        if len(prev_out_vocab_states) > 0:
            out_vocab_model.prev_encoder_state = prev_out_vocab_states[-1][1]
        else:
            out_vocab_model.prev_encoder_state = None
    else:

        line = sys.stdin.readline().strip()
        print("RECEIVED INPUT2:"+line)

        if flag == 3:
            response = sys.stdin.readline().strip()
            line = clarification_heuristic.augment(line, response)
            print("NEW VERSION OF LINE: "+str(line))
            if line == None:
                with open(output_path, "w") as fout:
                    fout.write("2\n")
                    fout.flush()
                print('ready')
                continue
        if line[-1] != ".":
            line += "."
            print("ADDED PERIOD, NOW: "+line)

        with in_vocab_sess.as_default():
            itWorked, result = in_vocab_model.parse(in_vocab_sess, line)

        if itWorked:
            prev_in_vocab_states.append((state_num, in_vocab_model.prev_encoder_state))
            state_num += 1
            result = map(lambda x:"".join(x), result[0])
            print(result)
            prev_parses.append(result)
            try:
                with open(output_path, "w") as fout:
                    fout.write("0\n")
                    for entry in result:
                        fout.write(entry+"\n")
                    fout.flush()
            except:
                with open(output_path, "w") as fout:
                    fout.write("2\n")
                    fout.write(result)
                    fout.flush()
            

        else:
            print("OUT OF VOCAB FOUND. USING OTHER PARSER")
            with out_vocab_sess.as_default():
                itWorked, result = out_vocab_model.parse(out_vocab_sess, line)

            result = map(lambda x:"".join(x), result[0])
            print(result)
            prev_out_vocab_states.append((state_num, out_vocab_model.prev_encoder_state))
            state_num += 1
            prev_parses.append(result)
            try:
                with open(output_path, "w") as fout:
                    fout.write("0\n")
                    for entry in result:
                        fout.write(entry+"\n")
                    fout.flush()
            except:
                with open(output_path, "w") as fout:
                    fout.write("2\n")
                    fout.write(result)
                    fout.flush()
    print('ready')
