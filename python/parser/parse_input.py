from __future__ import print_function
import tensorflow as tf
import pickle, sys, re
import config, data_utils, pointer_parser_model, clarification_heuristic


if len(sys.argv) != 4:
    print("Usage: python parse_input.py <model file> <config file> <output file>")
    sys.exit(1)

test_model_path, test_conf_path, output_path = sys.argv[1:4]
conf_in = open(test_conf_path, 'r')
conf = pickle.load(conf_in)
conf_in.close()

with tf.Session() as sess:
    model = pointer_parser_model.MultiSentPointerParseModel(conf, None)
    sess.run(tf.initialize_all_variables())
    model.saver.restore(sess, test_model_path)
    prev_states = []
    prev_parses = []

    print("\n")
    print('ready')
    while True:
        line = sys.stdin.readline()
        print("RECEIVED INPUT1:"+line)
        flag = int(line.strip())
        if flag == 1:
            model.prev_encoder_state = None
            prev_state = []
            prev_parses = []
        elif flag == 2:
            line = sys.stdin.readline()
            delete_num = int(line.strip())
            print("DELETING PARSES: ",prev_parses[(-1*delete_num):])
            prev_states = prev_states[:(-1*delete_num)]
            prev_parses = prev_parses[:(-1*delete_num)]
            if prev_parses:
                model.prev_encoder_state = prev_states[-1]
            else:
                model.prev_encoder_state = None
        else:

            line = sys.stdin.readline().strip()
            print("RECEIVED INPUT2:"+line)

            if flag == 3:
                response = sys.stdin.readline().strip()
                line = clarification_heuristic.augment(line, response)
                print("NEW VERSION OF LINE: "+line)
                if line == None:
                    with open(output_path, "w") as fout:
                        fout.write("2\n")
                        fout.flush()
                    continue
            if line[-1] != ".":
                line += "."
                print("ADDED PERIOD, NOW: "+line)
            itWorked, result = model.parse(sess, line)

            if itWorked:
                prev_states.append(model.prev_encoder_state)
                result = map(lambda x:"".join(x), result[0])
                print(result)
                prev_parses.append(result)
                try:
                    with open(output_path, "w") as fout:
                        fout.write("0\n")
                        for entry in result:
                            fout.write(entry+"\n")
                        fout.flush()
                except:
                    with open(output_path, "w") as fout:
                        fout.write("2\n")
                        fout.write(result)
                        fout.flush()
                

            else:
                prev_parses.append(result)
                with open(output_path, "w") as fout:
                    fout.write("1\n")
                    fout.write("\n".join(result))
                    fout.flush()
        print('ready')
