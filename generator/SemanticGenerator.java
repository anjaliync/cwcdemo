package generator;

import semanticsOLD.generation.ChartEdge;
import semanticsOLD.generation.ChartGenerator;
import semanticsOLD.lambdaFOL.Expression;
import semanticsOLD.lambdaFOL.LambdaAbstraction;
import semanticsOLD.lambdaFOL.SemLexicon;
import semanticsOLD.parsing.SemanticCKYParser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

/**
 * The SemanticGenerator class should be modified by Anjali/Ryan
 * to dynamically generate responses as appropriate.
 * 
 * @author ramusa2
 *
 */
public class SemanticGenerator implements FreeCellGenerator {
	
	protected ChartGenerator generator;
	protected SemanticCKYParser parser;
	protected SemLexicon       lexicon;
	protected Random rand = new Random();
	
	public SemanticGenerator(String lexiconFile) {
		Expression.readLexicon(lexiconFile);
		this.lexicon = Expression.lexicon;
		this.generator = new ChartGenerator(Expression.lexicon);
		this.parser = new SemanticCKYParser(lexiconFile);
	}

	@Override
	public String actionApplied(String... actions) {
		// "I moved"
		if(actions.length == 0) {
			return produceStatement(logicalForm("ιx57(robot(x57) ∧ (moved(x57) ∧ agent(x57)))"));
		}
		
		// perform one action
		if(actions.length == 1) {
			Collection<Expression> expressions = parseSentence(actions[0]);
			if (expressions.isEmpty()) {
				System.err.println("Unable to parse action: "+actions[0]);
				return String.format("I performed the action: %s", actions[0]);
			}
			return String.format("I performed the action: %s", logicalForm(expressions.iterator().next())); // assumes first parse is correct
		}
		
		// perform multiple actions
		StringBuilder sb = new StringBuilder();
		sb.append("I performed the following actions:");
		for(String action : actions) {
			Collection<Expression> expressions = parseSentence(action);
			if (expressions.isEmpty()) {
				System.err.println("Unable to parse action: "+action);
				sb.append(String.format("\n  %s", action));
			}
			else sb.append(String.format("\n  %s", logicalForm(expressions.iterator().next())));
		}
		return sb.toString();
	}

	@Override
	public String ambiguousTarget(String originalEntity, String... possibleMatches) {
		StringBuilder sb = new StringBuilder();
		if (rand.nextInt(2) < 1) sb.append(String.format("Which %s are you referring to? Do you mean ", originalEntity));
		else 					 sb.append(String.format("I don't know which %s you mean. Are you referring to ", originalEntity));
		
		// list possible groundings by parsing input entity strings
		for(int i = 0; i < possibleMatches.length; i++) {
			Expression e = getEntityExpression(possibleMatches[i]);
			if (i == possibleMatches.length-1) sb.append("or ");
			if (e != null) sb.append(String.format("%s", logicalForm(e)));
			else sb.append(String.format("%s", possibleMatches[i]));
			if (i == possibleMatches.length-1) sb.append("?");
			else if (possibleMatches.length > 2) sb.append(", ");
			else sb.append(" ");
		}
		return sb.toString();
	}

	@Override
	public String invalidMove() {
		int r = rand.nextInt(3);
		
		if (r < 1)      return "I apologize, but I don't believe that is a valid move.";
		else if (r < 2) return "I don't think that move is allowed.";
		else            return "That move is illegal.";
	}

	@Override
	public String movePerformed(Action action) {
		// "I moved"
		if (action.emptyArguments()) {
			switch (action.getPredicate()) {
			case MOVE:
				return produceStatement(logicalForm("ιx217(robot(x217) ∧ ∃x216(card(x216) ∧ (agent(x217) ∧ moved(x217, x216) ∧ patient(x216))))"));
			case FINISH:
				return produceStatement(logicalForm("ιx271(robot(x271) ∧ ∃x270(card(x270) ∧ ιx225(agent(x271) ∧ destination(x225) ∧ foundation(x225) ∧ moved(x271, x270, x225) ∧ patient(x270))))"));
			case FINISH_FROM_CELL:
				return produceStatement(logicalForm("ιx271(robot(x271) ∧ ∃x269(freecell(x269) ∧ ∃x270(card(x270) ∧ ιx229(agent(x271) ∧ destination(x229) ∧ foundation(x229) ∧ moved(x271, x270, x229, x269) ∧ patient(x270) ∧ source(x269)))))"));
			case FREE:
				return produceStatement(logicalForm("ιx271(robot(x271) ∧ ∃x270(card(x270) ∧ ∃x232(agent(x271) ∧ destination(x232) ∧ freecell(x232) ∧ moved(x271, x270, x232) ∧ patient(x270))))"));
			case UNFREE:
				return produceStatement(logicalForm("ιx271(robot(x271) ∧ ∃x270(card(x270) ∧ ιx235(ιx236(agent(x271) ∧ destination(x235) ∧ freecell(x236) ∧ moved(x271, x270, x235, x236) ∧ patient(x270) ∧ source(x236) ∧ tableau(x235)))))"));
			default:
				System.err.println("Unknown predicate: "+action.getPredicate());
				return null;
			}
		}
		
		Predicate predicate   = action.getPredicate();
		String    entity      = action.getEntity();
		String    destination = action.getDestination();
		String    source      = action.getSource();
		
		if (rand.nextInt(2) < 1) source = null;
		
		// X
		if (entity != null && (predicate != Predicate.MOVE || (predicate == Predicate.MOVE && destination == null && source == null))) {
			// "I moved X"
			if (rand.nextInt(2) < 1) {
				Expression I     = lexicon.getFirstSense("I", "NP");
				Expression moved = getPredicateEntry(action.getPredicate(), 1, true, false);
				Expression x     = getEntityExpression(entity);
				
				if (x != null) {
					Expression moved_x   = ((LambdaAbstraction) moved).applyTo(x);
					
					if (action.getPredicate() == Predicate.FINISH_FROM_CELL) {
						Expression freecell = getEntityExpression("the freecell");
						moved_x = ((LambdaAbstraction) moved_x).applyTo(freecell);
					}
					
					Expression I_moved_x = ((LambdaAbstraction) moved_x).applyTo(I);
					return produceStatement(logicalForm(I_moved_x));
				}
				
				return null;
			}
			
			// "X was moved"
			else {
				Expression moved = getPredicateEntry(action.getPredicate(), 1, true, true);
				Expression x     = getEntityExpression(entity);
				
				if (x != null) {
					if (action.getPredicate() == Predicate.FINISH_FROM_CELL) {
						Expression freecell = getEntityExpression("the freecell");
						moved = ((LambdaAbstraction) moved).applyTo(freecell);
					}
					
					Expression x_moved  = ((LambdaAbstraction) moved).applyTo(x);
					return produceStatement(logicalForm(x_moved));
				}
				
				return null;
			}
		}
		
		// X, Y
		else if (entity != null && destination != null && source == null) {
			// "I moved X to Y"
			if (rand.nextInt(2) < 1) {
				Expression I     = lexicon.getFirstSense("I", "NP");
				Expression moved = getPredicateEntry(action.getPredicate(), 2, true, false);
				Expression x     = getEntityExpression(entity);
				Expression y	 = getEntityExpression(destination);
				
				if (x != null && y != null) {
					Expression moved_x    = ((LambdaAbstraction) moved).applyTo(x);
					Expression moved_xy   = ((LambdaAbstraction) moved_x).applyTo(y);
					Expression I_moved_xy = ((LambdaAbstraction) moved_xy).applyTo(I);
					return produceStatement(logicalForm(I_moved_xy));
				}
				
				return null;
			}
			
			// "X was moved to Y"
			else {
				Expression moved = getPredicateEntry(action.getPredicate(), 2, true, true);
				Expression x     = getEntityExpression(entity);
				Expression y	 = getEntityExpression(destination);
				
				if (x != null && y != null) {
					Expression moved_y    = ((LambdaAbstraction) moved).applyTo(y);
					Expression x_moved_y  = ((LambdaAbstraction) moved_y).applyTo(x);
					return produceStatement(logicalForm(x_moved_y));
				}
				
				return null;
			}
		}
		
		// X, Y, Z
		else if (entity != null && destination != null && source != null) {
			// "I moved X from Z to Y"
			if (rand.nextInt(2) < 1) {
				Expression I     = lexicon.getFirstSense("I", "NP");
				Expression moved = getPredicateEntry(action.getPredicate(), 3, true, false);
				Expression x     = getEntityExpression(entity);
				Expression y	 = getEntityExpression(destination);
				Expression z	 = getEntityExpression(source);
				
				if (x != null && y != null && z != null) {
					Expression moved_x     = ((LambdaAbstraction) moved).applyTo(x);
					Expression moved_xz    = ((LambdaAbstraction) moved_x).applyTo(z);
					Expression moved_xzy   = ((LambdaAbstraction) moved_xz).applyTo(y);
					Expression I_moved_xyz = ((LambdaAbstraction) moved_xzy).applyTo(I);
					return produceStatement(logicalForm(I_moved_xyz));
				}
				
				return null;
			}
			
			// "X was moved from Z to Y"
			else {
				Expression moved = getPredicateEntry(action.getPredicate(), 3, true, true);
				Expression x     = getEntityExpression(entity);
				Expression y	 = getEntityExpression(destination);
				Expression z	 = getEntityExpression(source); 
				
				if (x != null && y != null && z != null) {
					Expression moved_z     = ((LambdaAbstraction) moved).applyTo(z);
					Expression moved_zy    = ((LambdaAbstraction) moved_z).applyTo(y);
					Expression x_moved_zy  = ((LambdaAbstraction) moved_zy).applyTo(x);
					return produceStatement(logicalForm(x_moved_zy));
				}
				
				return null;
			}
		}
		
		System.err.println("Invalid move performed.");
		return null;
	}
	
	@Override
	public String moveCommand(Action action) {
		// "Move a card"
		if (action.emptyArguments()) {
			switch (action.getPredicate()) {
			case MOVE:
				return logicalForm("∃x270(card(x270) ∧ (move(x270) ∧ patient(x270)))");
			case FINISH:
				return logicalForm("∃x270(card(x270) ∧ ιx177(destination(x177) ∧ foundation(x177) ∧ move(x270, x177) ∧ patient(x270)))");
			case FINISH_FROM_CELL:
				return logicalForm("ιx269(freecell(x269) ∧ ∃x270(card(x270) ∧ ιx180(destination(x180) ∧ foundation(x180) ∧ move(x270, x180, x269) ∧ patient(x270) ∧ source(x269))))");
			case FREE:
				return logicalForm("∃x270(card(x270) ∧ ∃x182(destination(x182) ∧ freecell(x182) ∧ move(x270, x182) ∧ patient(x270)))");
			case UNFREE:
				return logicalForm("∃x270(card(x270) ∧ ιx184(ιx185(destination(x184) ∧ freecell(x185) ∧ move(x270, x184, x185) ∧ patient(x270) ∧ source(x185) ∧ tableau(x184))))");
			default:
				System.err.println("Unknown predicate: "+action.getPredicate());
				return null;
			}
		}

		Predicate predicate   = action.getPredicate();
		String    entity      = action.getEntity();
		String    destination = action.getDestination();
		String    source      = action.getSource();
		
		if (rand.nextInt(2) < 1) source = null;
		
		// "Move X"
		if (entity != null && (predicate != Predicate.MOVE || (predicate == Predicate.MOVE && destination == null && source == null))) {
			Expression move = getPredicateEntry(action.getPredicate(), 1, false);
			Expression x    = getEntityExpression(entity);

			if (x != null) {
				Expression move_x   = ((LambdaAbstraction) move).applyTo(x);
				
				if (action.getPredicate() == Predicate.FINISH_FROM_CELL) {
					Expression freecell = getEntityExpression("the freecell");
					move_x = ((LambdaAbstraction) move_x).applyTo(freecell);
				}
				
				return logicalForm(move_x);
			}

			return null;
		}
		
		// "Move X to Y"
		if (entity != null && destination != null && source == null) {
			Expression move  = getPredicateEntry(action.getPredicate(), 2, false);
			Expression x     = getEntityExpression(entity);
			Expression y	 = getEntityExpression(destination);

			if (x != null && y != null) {
				Expression move_x     = ((LambdaAbstraction) move).applyTo(x);
				Expression move_xy    = ((LambdaAbstraction) move_x).applyTo(y);
				return logicalForm(move_xy);
			}

			return null;
		}
		
		// "Move X from Z to Y"
		if (entity != null && destination != null && source != null) {
			Expression move  = getPredicateEntry(action.getPredicate(), 3, false);
			Expression x     = getEntityExpression(entity);
			Expression y	 = getEntityExpression(destination);
			Expression z	 = getEntityExpression(source); 

			if (x != null && y != null && z != null) {
				Expression move_x     = ((LambdaAbstraction) move).applyTo(x);
				Expression move_xz    = ((LambdaAbstraction) move_x).applyTo(z);
				Expression move_xzy   = ((LambdaAbstraction) move_xz).applyTo(y);
				return logicalForm(move_xzy);
			}

			return null;
		}
		
		System.err.println("Invalid move performed.");
		return null;
	}
	
	@Override
	public String movePreference(Collection<Action> actions) {
		StringBuilder sb = new StringBuilder();
		
		int r = rand.nextInt(3);
		if (r < 1) 	 	sb.append("I don't know what to do next. ");
		else if (r < 2) sb.append("I'm not sure what to do now. ");
		else 			sb.append("I'm stuck. ");
		
		if (actions == null || actions.size() == 0 || actions.size() == 1) {
			if (rand.nextInt(2) < 1) sb.append("Please suggest a move.");
			else					 sb.append("What should I do now?");
		}
		else {
			Iterator<Action> it = actions.iterator();
			// return input move commands as suggestions
			if (actions.size() == 2) {
				String c1 = moveCommand(it.next()), c2 = moveCommand(it.next());
				if (c1 != null && c2 != null) sb.append(String.format("Should I %s or %s?", c1, c2));
				else return null;
			}
			else {
				sb.append("I could ");
				for (int i = 0; i < actions.size(); i++) {
					String c = moveCommand(it.next());
					
					if (c != null) {
						if (i == actions.size()-1) sb.append(String.format("or %s.", c));
						else sb.append(String.format("%s, ", c));
					}
					else return null;
				}
				if (rand.nextInt(2) < 1) sb.append(" What do you think?");
				else sb.append(" What do you say?");
			}
		}
		return sb.toString();
	}

	@Override
	public String badReward(Collection<Action> actions) {
		StringBuilder sb = new StringBuilder();
		
		int r = rand.nextInt(3);
		if (r < 1) 		sb.append("I don't think your suggested move was very good. ");
		else if (r < 2) sb.append("I don't think that direction is very fruitful. ");
		else 			sb.append("That's probably not a good idea. ");
		
		if (actions == null || actions.size() == 0) sb.append("What other ideas do you have?");
		else {
			Iterator<Action> it = actions.iterator();
			if (actions.size() == 1) {
				String c1 = moveCommand(it.next());
				if (c1 != null) sb.append(String.format("Instead, you might want to %s.", c1));
				else return null;
			}
			else if (actions.size() == 2) {
				String c1 = moveCommand(it.next()), c2 = moveCommand(it.next());
				if (c1 != null && c2 != null) sb.append(String.format("Instead, you might want to %s or %s.", c1, c2));
				else return null;
			}
			else {
				sb.append("It might be better to ");
				// return input move commands as suggestions
				for (int i = 0; i < actions.size(); i++) {
					String c = moveCommand(it.next());
					
					if (c != null) {
						if (i == actions.size()-1) sb.append(String.format("or %s.", c));
						else sb.append(String.format("%s, ", c));
					}
					else return null;
				}
			}
		}
		return sb.toString();
	}

	@Override
	public String subtask(Subtask subtask, String... parameters) {
		StringBuilder sb = new StringBuilder();
		int r = rand.nextInt(3);
		if (r < 1) 		sb.append("Currently, I am trying to ");
		else if (r < 2) sb.append("I am attempting to ");
		else			sb.append("My current goal is to ");
		
		switch (subtask) {
		case OUT_OF_THE_WAY:
			if (parameters.length >= 2) {
				Expression c1 = getEntityExpression(parameters[0]);
				Expression c2 = getEntityExpression(parameters[1]);

				if (c1 != null && c2 != null) {
					Expression blocking = lexicon.getFirstSense("blocking", "(NP\\NP)/NP");
					Expression blocking_c2 = ((LambdaAbstraction) blocking).applyTo(c2);
					Expression c1_blocking_c2 = ((LambdaAbstraction) blocking_c2).applyTo(c1);

					Expression move = null;
					if (rand.nextInt(2) < 1) move = getPredicateEntry(Predicate.MOVE, 1);
					else					 move = getPredicateEntry(Predicate.REMOVE, 1);

					Expression move_c1_blocking_c2 = ((LambdaAbstraction) move).applyTo(c1_blocking_c2);
					
					String rz = logicalForm(move_c1_blocking_c2);
					if (rz != null) {
						sb.append(String.format("%s.", logicalForm(move_c1_blocking_c2)));
						return sb.toString();
					}
					
					return null;
				}
			}
			
			System.err.println("Insufficient number of arguments to "+subtask);
			return null;
		case BOTTOM_OUT_OF_THE_WAY:
			if (parameters.length >= 1) {
				Expression x 	  = getEntityExpression(parameters[0]);
				Expression remove = getPredicateEntry(Predicate.REMOVE, 1);
				
				if (x != null) {
					Expression remove_x = ((LambdaAbstraction) remove).applyTo(x);
					
					String rz = logicalForm(remove_x);
					if (rz != null) {
						sb.append(String.format("%s.", logicalForm(remove_x)));
						return sb.toString();
					}
					
					return null;
				}
			}
			
			System.err.println("Insufficient number of arguments to "+subtask);
			return null;
		case DO_CARD:
			Action action = new Action(Predicate.FINISH, parameters);
			
			String c = moveCommand(action);
			if (c != null) {
				sb.append(String.format("%s.", moveCommand(action)));
				return sb.toString();
			}
			
			return null;
		default:
			System.err.println("Unknown subtask: "+subtask);
			return null;
		}
	}

	/**
	 * Returns a random realization of the input logical form.
	 */
	@Override
	public String logicalForm(String logicalForm) {
		try {
			Expression e = Expression.parseString(fixExpressionString(logicalForm));
			return logicalForm(e);
		} catch (Exception e) {
			System.err.println("Malformed logical form: "+logicalForm);
			return null;
		}
	}
	
	public String logicalForm(Expression logicalForm) {
		ArrayList<ChartEdge> result_edges = generator.generate(logicalForm);

		if (result_edges != null && !result_edges.isEmpty()) {
			int r = rand.nextInt((result_edges.size()));
			return result_edges.get(r).getSurfaceString().replace("-", " ");
		}

		System.err.println("Unable to generate from expression: "+logicalForm);
		return null;
	}
	
	/**
	 * Note: only returns Expressions for N or NP parses of the entity string 
	 * (i.e., entity must be a noun/noun phrase).
	 */
	public Collection<Expression> parseEntityString(String entity) {
		return this.parser.parseEntity(entity.trim().toLowerCase());
	}
	
	/**
	 * Attempts to parse the entity string and displays an error if unsuccessful.
	 * @param entity The entity string to be parsed
	 * @return The (first) Expression returned by the parser for the input string
	 */
	Expression getEntityExpression(String entity) {
		Collection<Expression> expressions = parseEntityString(entity);
		if (expressions == null || expressions.isEmpty()) {
			System.err.println("Unable to parse entity: "+entity);
			return null;
		}
		return expressions.iterator().next(); // FIXME: assuming the first entity Expression returned is correct!
	}
	
	/**
	 * Note: only returns Expressions for S parses of the sentence string
	 */
	public Collection<Expression> parseSentence(String sentence) {
		return this.parser.parseSentence(sentence);
	}
	
	/**
	 * Temporary fix for parentheses around quantified expression strings.
	 * @param e String to be adjusted
	 * @return fixed String representation
	 */
	public static String fixExpressionString(String e) {
		// fix open parentheses around quantifiers
		String es = e.replace("(ι", "ι").replace("(∃", "∃");
		StringBuilder sb = new StringBuilder(es);
		
		// balance the remaining parentheses
		int open = 0, index = 0;
		for (int i = 0; i < es.length(); i++) {
			if (es.charAt(i) == '(') open += 1;
			if (es.charAt(i) == ')' && open > 0) open -= 1;
			else if (es.charAt(i) == ')' && open == 0) {
				sb.deleteCharAt(index);
				index -= 1;
			}
			index += 1;
		}
		
		return sb.toString();
	}
	
	/**
	 * Capitalizes and punctuates the sentence.
	 * @param s The sentence to capitalize/punctuate
	 * @return A well-formed sentence
	 */
	public static String produceStatement(String s) {
		if (s == null) return null;
		s = s.substring(0,1).toUpperCase() + s.substring(1);
		return s.trim() + ".";
	}
	
	Expression getPredicateEntry(Predicate predicate, int numParams, boolean... flags) {
		switch (predicate) {
		case MOVE:
			if (flags.length > 0 && flags[0]) {
				switch (numParams) {
				case 1: if (!flags[1]) return lexicon.getFirstSense("moved", "(S\\NP)/NP");
									   return lexicon.getFirstSense("was-moved", "S\\NP");
				case 2: if (!flags[1]) return lexicon.getFirstSense("moved", "((S\\NP)/PP[to])/NP");
									   return lexicon.getFirstSense("was-moved", "(S\\NP)/PP[to]");
				case 3: if (!flags[1]) return lexicon.getFirstSense("moved", "(((S\\NP)/PP[to])/PP[from])/NP");
									   return lexicon.getFirstSense("was-moved", "((S\\NP)/PP[to])/PP[from]");
				default:
					return null;
				}
			}

			switch (numParams) {
			case 1:  return lexicon.getFirstSense("move", "S/NP");
			case 2:  return lexicon.getFirstSense("move", "(S/PP[to])/NP");
			case 3:  return lexicon.getFirstSense("move", "((S/PP[to])/PP[from])/NP");
			default: return null;
			}

		case FINISH:
			if (flags.length > 0 && flags[0]) {
				if (!flags[1]) return lexicon.getFirstSense("finished", "(S\\NP)/NP");
				else 		   return lexicon.getFirstSense("was-finished", "S\\NP");
			}
			
			return lexicon.getFirstSense("finish", "S/NP");

		case FINISH_FROM_CELL:
			if (flags.length > 0 && flags[0]) {
				if (!flags[1]) return lexicon.getFirstSense("finished", "((S\\NP)/PP[from])/NP");
				else 		   return lexicon.getFirstSense("was-finished", "(S\\NP)/PP[from]");
			}
			
			return lexicon.getFirstSense("finish", "(S/PP[from])/NP");
			
		case FREE:
			if (flags.length > 0 && flags[0]) {
				if (!flags[1]) return lexicon.getFirstSense("freed", "(S\\NP)/NP");
				else 		   return lexicon.getFirstSense("was-freed", "S\\NP");
			}
			
			return lexicon.getFirstSense("free", "S/NP");
			
		case UNFREE:
			if (flags.length > 0 && flags[0]) {
				if (!flags[1]) return lexicon.getFirstSense("unfreed", "(S\\NP)/NP");
				else 		   return lexicon.getFirstSense("was-unfreed", "S\\NP");
			}
			
			return lexicon.getFirstSense("unfree", "S/NP");
			
		case REMOVE:
			return lexicon.getFirstSense("remove", "S/NP");
			
		default:
			return null;
		}
	}
}
