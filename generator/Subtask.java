package generator;

/**
 * List of flags for FreeCell subtasks.
 * 
 * @author nrynchn2
 *
 */
public enum Subtask {
	
	OUT_OF_THE_WAY,
	
	BOTTOM_OUT_OF_THE_WAY,
	
	DO_CARD

}
