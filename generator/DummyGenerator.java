package generator;

import java.util.Collection;

/**
 * Returns canned responses to the initial set of communication actions 
 * (essentially a placeholder, but more functionality can be added).
 * 
 * @author ramusa2
 *
 */
public class DummyGenerator implements FreeCellGenerator {

	@Override
	public String actionApplied(String... actions) {
		if(actions.length == 0) {
			return "I performed an action.";
		}
		if(actions.length == 1) {
			return String.format("I performed the action: %s", actions[0]);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("I performed the following actions:");
		for(String action : actions) {
			sb.append(String.format("\n  %s", action));
		}
		return sb.toString();
	}

	@Override
	public String ambiguousTarget(String originalEntity, String... possibleMatches) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Which %s are you referring to? Is it one of:", originalEntity));
		for(String match : possibleMatches) {
			sb.append(String.format("\n  %s", match));
		}
		return sb.toString();
	}

	@Override
	public String invalidMove() {
		return "I apologize, but I don't believe that is a valid move.";
	}

	@Override
	public String movePerformed(Action action) {
		if (action == null) return "I don't know what I did.";
		
		String p = null;
		switch (action.getPredicate()) {
		case MOVE:
			p = "moved";
			break;
		case FINISH:
		case FINISH_FROM_CELL:
			p = "finished";
			break;
		case FREE:
			p = "freed";
			break;
		case UNFREE:
			p = "unfreed";
			break;
		default:
			p = null;
			break;
		}
		
		if(action.emptyArguments()) {
			if (p == null) p = "did";
			return "I don't know what I "+p+".";
		}
		
		if (p == null) p = "moved";
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("I %s %s", p, action.getEntity()));
		if(action.getDestination() != null) {
			sb.append(String.format(" from %s", action.getDestination()));
		}
		if(action.getSource() != null) {
			sb.append(String.format(" to %s", action.getSource()));
		}
		return sb.toString();
	}
	
	@Override
	public String moveCommand(Action action) {
		String p = null;
		switch (action.getPredicate()) {
		case MOVE:
			p = "Move";
			break;
		case FINISH:
		case FINISH_FROM_CELL:
			p = "Finish";
			break;
		case FREE:
			p = "Free";
			break;
		case UNFREE:
			p = "Unfree";
			break;
		default:
			p = null;
			break;
		}
		
		if(action.emptyArguments()) {
			if (p == null) p = "Do";
			return p+" something.";
		}
		
		if (p == null) p = "Move";
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s %s", p, action.getEntity()));
		if(action.getDestination() != null) {
			sb.append(String.format(" from %s", action.getDestination()));
		}
		if(action.getSource() != null) {
			sb.append(String.format(" to %s", action.getSource()));
		}
		return sb.toString();
	}

	@Override
	public String logicalForm(String logicalForm) {
		return String.format("Using dummy generator, can't generate a logical form from:\n    %s", logicalForm);
	}

	@Override
	public String movePreference(Collection<Action> actions) {
		StringBuilder sb = new StringBuilder();
		if (actions == null || actions.size() == 1) sb.append("I don't know what to do next. Please suggest a move.");
		else {
			sb.append("I don't know what to do next. Please suggest a move from the following:");
			for (Action action : actions) {
				String p = null;
				switch (action.getPredicate()) {
				case MOVE:
					p = "Move";
					break;
				case FINISH:
				case FINISH_FROM_CELL:
					p = "Finish";
					break;
				case FREE:
					p = "Free";
					break;
				case UNFREE:
					p = "Unfree";
					break;
				default:
					p = null;
					break;
				}
				
				sb.append(String.format("\n  %s %s", p, action.getEntity()));
				if(action.getDestination() != null) {
					sb.append(String.format(" from %s", action.getDestination()));
				}
				if(action.getSource() != null) {
					sb.append(String.format(" to %s", action.getSource()));
				}
			}
		}
		return sb.toString();
	}

	@Override
	public String badReward(Collection<Action> actions) {
		StringBuilder sb = new StringBuilder();
		if (actions == null || actions.size() == 0) sb.append("Your suggested move leads to a bad reward. Please suggest a different move.");
		else {
			sb.append("Your suggested move leads to a bad reward. These might be better:");
			for (Action action : actions) {
				String p = null;
				switch (action.getPredicate()) {
				case MOVE:
					p = "Move";
					break;
				case FINISH:
				case FINISH_FROM_CELL:
					p = "Finish";
					break;
				case FREE:
					p = "Free";
					break;
				case UNFREE:
					p = "Unfree";
					break;
				default:
					p = null;
					break;
				}
				
				sb.append(String.format("\n  %s %s", p, action.getEntity()));
				if(action.getDestination() != null) {
					sb.append(String.format(" from %s", action.getDestination()));
				}
				if(action.getSource() != null) {
					sb.append(String.format(" to %s", action.getSource()));
				}
			}
		}
		return sb.toString();
	}

	@Override
	public String subtask(Subtask s, String... parameters) {
		// TODO Auto-generated method stub
		return null;
	}

}
