package generator;

/**
 * List of flags for FreeCell predicates.
 * 
 * @author nrynchn2
 *
 */
public enum Predicate {
	
	MOVE,
	
	FINISH,
	
	FINISH_FROM_CELL,
	
	FREE,
	
	UNFREE,
	
	REMOVE
}
