package semanticsOLD.ccg;

/**
 * A ProducedCCGcat is used to wrap a CCGcat with information
 * about the combinator used to produce that CCGcat from its
 * child(ren) during parsing/decoding.
 * 
 * @author ramusa2
 *
 */
public class ProducedCCGcat {

	private Rule_Type combinatorUsed;
	
	private int arityOfCombinatorUsed;
	
	private CCGcat producedCategory;
	
	public ProducedCCGcat(Rule_Type combinator, int arity, CCGcat category) {
		this.combinatorUsed = combinator;
		this.arityOfCombinatorUsed = arity;
		this.producedCategory = category;
	}
	
	public Rule_Type getCombinatorUsed() {
		return this.combinatorUsed;
	}
	
	public int getArityOfCombinatorUsed() {
		return this.arityOfCombinatorUsed;
	}
	
	public CCGcat getProducedCCGcat() {
		return this.producedCategory;
	}
	
	public String catString() {
		return this.producedCategory.catString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		ProducedCCGcat other = (ProducedCCGcat) obj;
		if (arityOfCombinatorUsed != other.arityOfCombinatorUsed)
			return false;
		if (combinatorUsed != other.combinatorUsed)
			return false;
		if (producedCategory == null) {
			if (other.producedCategory.catString() != null)
				return false;
		} else if (!producedCategory.catString().equals(other.producedCategory.catString()))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arityOfCombinatorUsed;
		result = prime * result
				+ ((combinatorUsed == null) ? 0 : combinatorUsed.hashCode());
		result = prime
				* result
				+ ((producedCategory == null) ? 0 : producedCategory.catString().hashCode());
		return result;
	}
}
