package semanticsOLD.ccg;

import java.util.ArrayList;
import java.util.Collections;

import semanticsOLD.parsing.Sentence;

public class PargDepSet {
	
	protected Sentence sentence;
	
	protected ArrayList<PargDep> deps;
	
	public PargDepSet(Sentence sentence) {
		this.sentence = sentence;
		this.deps = new ArrayList<PargDep>();
	}
	
	public void addDep(int arg, int head, String category, int slot) {
		this.deps.add(new PargDep(arg, head, category, slot));
	}
	
	public void addDep(int arg, int head, String category, int slot, boolean extracted, boolean bounded) {
		this.deps.add(new PargDep(arg, head, category, slot, extracted, bounded));
	}
	
	public ArrayList<PargDep> getDeps() {
		return this.deps;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<s> ");
		sb.append(this.deps.size()+"\n");
		Collections.sort(this.deps);
		for(PargDep dep : deps) {
			sb.append(dep.getArg()+" \t ");
			sb.append(dep.getHead()+" \t ");
			sb.append(dep.getCategory()+" \t ");
			sb.append(dep.getArgSlot()+" \t ");
			sb.append(sentence.wordAt(dep.getArg())+" ");
			sb.append(sentence.wordAt(dep.getHead()));
			if(dep.isExtracted()) {
				if(dep.isBounded()) {
					sb.append(" <XB>");
				}
				else {
					sb.append(" <XU>");
				}
			}
			sb.append("\n");
		}
		sb.append("</s>");
		return sb.toString();
	}
	
	public int size() {
		return this.deps.size();
	}
	
	public int matchedLabeledDirected(PargDepSet o) {
		int matched = 0;
		for(PargDep dep : this.deps) {
			for(PargDep dep2 : o.deps) {
				if(dep.matchesLabeledDirected(dep2)) {
					matched++;
					continue;
				}
			}
		}
		return matched;
	}
	
	public int matchedUnlabeledUndirected(PargDepSet o) {
		int matched = 0;
		for(PargDep dep : this.deps) {
			for(PargDep dep2 : o.deps) {
				if(dep.matchesUnlabeledUndirected(dep2)) {
					matched++;
					continue;
				}
			}
		}
		return matched;
	}

}
