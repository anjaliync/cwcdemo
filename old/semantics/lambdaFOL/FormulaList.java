/**
 * 
 */
package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author juliahmr
 *
 */
public class FormulaList extends Formula {
	final protected Connective con;
	ArrayList<semanticsOLD.lambdaFOL.Expression> formulas;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;

	public static FormulaList createFormulaList(Connective c, ArrayList<semanticsOLD.lambdaFOL.Expression> f){
		if (c == Connective.AND || c == Connective.OR){
			ArrayList<semanticsOLD.lambdaFOL.Expression> fSorted = sortExpressionList(f);
			return new FormulaList(c,fSorted);
		}
		else return null;
	}

	protected FormulaList(Connective c, ArrayList<semanticsOLD.lambdaFOL.Expression> f){
		con = c;
		ArrayList<semanticsOLD.lambdaFOL.Expression> fSorted = sortExpressionList(f);
		formulas = fSorted;
		yield = initYield();
		yieldTypes = initYieldTypes();
		//formulas = f;
	}

	public Connective connective(){
		return con;
	}
	/**
	 * @param elist
	 * @return a list of expressions in which all atomic formulas appear first (sorted by their predicates), 
	 * followed by all complex formulas, followed by all quantified formulas, followed by all lambda abstractions, followed by all lambda applications, followed by all formula lists.
	 */
	protected static ArrayList<semanticsOLD.lambdaFOL.Expression> sortExpressionList(ArrayList<semanticsOLD.lambdaFOL.Expression> elist) {//TODO -- implement me
		ArrayList<semanticsOLD.lambdaFOL.Expression> sortedList = new ArrayList<semanticsOLD.lambdaFOL.Expression>();
		ArrayList<semanticsOLD.lambdaFOL.AtomicFormula> atomicFormulaList = new ArrayList<semanticsOLD.lambdaFOL.AtomicFormula>();
		ArrayList<semanticsOLD.lambdaFOL.ComplexFormula> complexFormulaList = new ArrayList<semanticsOLD.lambdaFOL.ComplexFormula>();
		ArrayList<FormulaList> formulaListList = new ArrayList<FormulaList>();
		ArrayList<semanticsOLD.lambdaFOL.QuantifiedFormula> quantifiedFormulaList = new ArrayList<semanticsOLD.lambdaFOL.QuantifiedFormula>();
		ArrayList<semanticsOLD.lambdaFOL.LambdaAbstraction> lambdaAbstractionList = new ArrayList<semanticsOLD.lambdaFOL.LambdaAbstraction>();
		ArrayList<semanticsOLD.lambdaFOL.LambdaApplication> lambdaApplicationList = new ArrayList<semanticsOLD.lambdaFOL.LambdaApplication>();
		ArrayList<FormulaVariable> formulaVarList = new ArrayList<FormulaVariable>();

		ArrayList<semanticsOLD.lambdaFOL.Constant> constantList = new ArrayList<semanticsOLD.lambdaFOL.Constant>();
		ArrayList<semanticsOLD.lambdaFOL.TermVariable> termVarList = new ArrayList<semanticsOLD.lambdaFOL.TermVariable>();

		for (semanticsOLD.lambdaFOL.Expression e: elist){
			if (e instanceof semanticsOLD.lambdaFOL.AtomicFormula){
				atomicFormulaList.add((semanticsOLD.lambdaFOL.AtomicFormula) e);
			}
			else if (e instanceof semanticsOLD.lambdaFOL.ComplexFormula){
				complexFormulaList.add((semanticsOLD.lambdaFOL.ComplexFormula) e);
			}
			else if (e instanceof FormulaList){
				formulaListList.add((FormulaList) e);
			}
			else if (e instanceof semanticsOLD.lambdaFOL.QuantifiedFormula){
				quantifiedFormulaList.add((semanticsOLD.lambdaFOL.QuantifiedFormula) e);
			}
			else if (e instanceof semanticsOLD.lambdaFOL.LambdaAbstraction){
				lambdaAbstractionList.add((semanticsOLD.lambdaFOL.LambdaAbstraction) e);
			}
			else if (e instanceof semanticsOLD.lambdaFOL.LambdaApplication){
				lambdaApplicationList.add((semanticsOLD.lambdaFOL.LambdaApplication) e);
			}
			else if (e instanceof FormulaVariable){
				formulaVarList.add((FormulaVariable)e);
			}	
			else if (e instanceof semanticsOLD.lambdaFOL.Constant){
				constantList.add((semanticsOLD.lambdaFOL.Constant)e);
			}
			else if (e instanceof semanticsOLD.lambdaFOL.TermVariable){
				termVarList.add((semanticsOLD.lambdaFOL.TermVariable)e);
			}

		}
		Collections.sort(atomicFormulaList);// NB: atomic formulas are the only ones that can be sorted (just by their predicate...which is wrong);
		sortedList.addAll(atomicFormulaList);
		sortedList.addAll(complexFormulaList);
		sortedList.addAll(formulaListList);
		sortedList.addAll(quantifiedFormulaList);
		sortedList.addAll(lambdaAbstractionList);
		sortedList.addAll(lambdaApplicationList);
		sortedList.addAll(formulaVarList);
		sortedList.addAll(constantList);
		sortedList.addAll(termVarList);
		return sortedList;
	}

	public boolean isDisjunction(){
		if (con == Connective.OR)
			return true;
		else return false;
	}

	public boolean isConjunction(){
		if (con == Connective.AND)
			return true;
		else return false;
	}

	public String toString(){
		String connective = "";
		switch (con){
		case AND: connective = " ∧ "; break;
		case OR: connective = " ∨ "; break;
		case IMPLIES: connective = " ⟶ "; break;
		}
		String out = "";
		String delim = "";
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			String f1 = (f instanceof semanticsOLD.lambdaFOL.AtomicFormula || f instanceof semanticsOLD.lambdaFOL.LambdaApplication)? f.toString(): new String("(" + f + ")");
			out = out + delim + f1;
			delim = connective;
		}
		return out;	
	}


	public FormulaList replace(semanticsOLD.lambdaFOL.Variable var, semanticsOLD.lambdaFOL.Expression e){
		ArrayList<semanticsOLD.lambdaFOL.Expression> newFormulas = new ArrayList<semanticsOLD.lambdaFOL.Expression>();
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			semanticsOLD.lambdaFOL.Expression f1 = f.replace(var, e);
			newFormulas.add(f1);
		}
		return new FormulaList(this.con, newFormulas);
	}


	public boolean equals(Object o){// strict equality (including order of terms), not equivalence 
		if (o != null && o instanceof FormulaList){
			FormulaList f = (FormulaList)o;

			if (this.con.equals(f.con) && this.formulas.size() == f.formulas.size()){
				for (int i = 0; i < this.formulas.size(); i++){
					semanticsOLD.lambdaFOL.Expression ei = this.formulas.get(i);
					semanticsOLD.lambdaFOL.Expression fi = f.formulas.get(i);
					if (!ei.equals(fi)){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	public int hashCode(){
		int seed = 31;
		switch (con){
		case AND: seed *=1; break;
		case OR: seed *= 3; break;
		default: seed *= 5; break;
		}
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			seed += f.hashCode();
			seed *= 31;
		}
		return seed;
	}

	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(semanticsOLD.lambdaFOL.Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat){
		if (e != null && e instanceof FormulaList){
			FormulaList f = (FormulaList)e;

			if (this.con.equals(f.con) && this.formulas.size() == f.formulas.size()){
				//boolean retval = true;
				for (int i = 0; i < this.formulas.size(); i++){
					semanticsOLD.lambdaFOL.Expression ei = this.formulas.get(i);
					semanticsOLD.lambdaFOL.Expression fi = f.formulas.get(i);
					if (ei.isEquivalentModuloVarRenaming(fi,varEquivalenceMapThis,varEquivalenceMapThat)){
						//retval = true;
					}
					else { 
						//retval = false;
						return false;
					}
				}
				return true;
			}
			else return false;
		}
		return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		String connective = "";
		switch (con){
		case AND: connective = "∧"; break;
		case OR: connective = "∨"; break;
		default: connective = ""; break;
		}
		String delim = null;
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			yield.addAll(f.yield());
			if (delim != null){
				yield.add(delim);
			}
			else {
				delim = connective;
			}
		}
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		TerminalType conType = null;
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			yieldTypes.addAll(f.yieldTypes());
			if (conType != null){
				yieldTypes.add(conType);
			}
			else {
				conType = TerminalType.CONNECTIVE;
			}
		}
		return yieldTypes;
	}

	public HashSet<semanticsOLD.lambdaFOL.Variable> getAllVariables() {
		HashSet<semanticsOLD.lambdaFOL.Variable> varSet = new HashSet<semanticsOLD.lambdaFOL.Variable>();
		for (semanticsOLD.lambdaFOL.Expression f: formulas){
			HashSet<semanticsOLD.lambdaFOL.Variable> fVars = f.getAllVariables();
			if (fVars != null){
				varSet.addAll(fVars);
			}
		}
		return varSet;		
	}

	protected semanticsOLD.lambdaFOL.Expression renameVar(semanticsOLD.lambdaFOL.Variable v, semanticsOLD.lambdaFOL.Variable v1) {
		ArrayList<semanticsOLD.lambdaFOL.Expression> newFormulas = new ArrayList<semanticsOLD.lambdaFOL.Expression>();
		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			semanticsOLD.lambdaFOL.Expression f1 = f.renameVar(v, v1);
			newFormulas.add(f1);
		}
		return new FormulaList(this.con, newFormulas);
	}


	public semanticsOLD.lambdaFOL.Expression flattenNestedFormulas(){
		//System.out.println("FormulaList.flattenNestedFormulas");
		ArrayList<semanticsOLD.lambdaFOL.Expression> fList = new ArrayList<semanticsOLD.lambdaFOL.Expression>();

		for (semanticsOLD.lambdaFOL.Expression f : formulas){
			//System.out.println("\t " + f);
			
			semanticsOLD.lambdaFOL.Expression f1 = f.flattenNestedFormulas();
			//System.out.println("\t =>" + f1);
			//System.out.println("Connectives match? " + this.connective() + " " + f1.connective());
			if (this.connective() == f1.connective()){
				//System.out.println("YES! " + f1.getClass());
				if (f1 instanceof semanticsOLD.lambdaFOL.ComplexFormula){
					fList.add(((semanticsOLD.lambdaFOL.ComplexFormula)f1).formula1);
					fList.add(((semanticsOLD.lambdaFOL.ComplexFormula)f1).formula2);
				}
				else if (f1 instanceof FormulaList){
					fList.addAll(((FormulaList)f1).formulas);
				}
			}
			else {
				fList.add(f1);
			}
		}

		return FormulaList.createFormulaList(this.connective(), fList);


	}
}
