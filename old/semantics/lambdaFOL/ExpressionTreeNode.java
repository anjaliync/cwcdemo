package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.LinkedList;

import semanticsOLD.lambdaFOL.Expression.Connective;
import semanticsOLD.lambdaFOL.Expression.Quantifier;
import semanticsOLD.lambdaFOL.Variable.Status;

public class ExpressionTreeNode extends semanticsOLD.lambdaFOL.ExpressionTree {
	ArrayList<semanticsOLD.lambdaFOL.ExpressionTree> children;
	boolean readLastChild = false;
	boolean readFirstChild = false;
	boolean requiresClosingParenthesis = false;
	ExpressionTreeNode(){
		parent = null;
		readLastChild = false;
		readFirstChild = false;
	}

	ExpressionTreeNode(String string){
		parent = null;
	}

	public ExpressionTreeNode(Type t, ExpressionTreeNode p) {
		type = t;
		parent = p;
		if (parent != null)
			parent.addChild(this);
	}

	public void addChild(semanticsOLD.lambdaFOL.ExpressionTree child) {
		if (children == null){
			children = new ArrayList<semanticsOLD.lambdaFOL.ExpressionTree>();
		}
		child.parent = this;
		children.add(child);
		this.readFirstChild = true;
	}


	// we have just read the last of the children for this node.
	// if we're at the root of the tree (parent == null), we're done. Else we pass the remainder of the list back up to the parent
	private semanticsOLD.lambdaFOL.ExpressionTree parsedLastChild (LinkedList<String> tokensRemainder){
		readFirstChild = true;
		readLastChild = true;
		// go up to the parent if there is a parent.
		debug("ENTER parseLastChild " + tokensRemainder + "\nRequires closing parenthesis?" + this.requiresClosingParenthesis);
		debug("This: " + this + "\n");
		if (parent != null){
			debug("parseLastChild: go up to parent");
			debug("PARENT: " + parent  + " " + parent.requiresClosingParenthesis);
			return parent.parse(tokensRemainder);
		}
		else if (tokensRemainder == null || tokensRemainder.isEmpty()){ 
			debug("parseLastChild: done, returning this tree: " + this);
			return this;
		}
		else {
			debug("parseLastChild: creating a new parent for " + this);
			ExpressionTreeNode parent = new ExpressionTreeNode();
			this.parent = parent;
			parent.addChild(this);
			debug("Checking: " + parent);
			return parent.parse(tokensRemainder);
		}
	}

	
	protected static ExpressionTreeLeaf parseLeaf(String token, ExpressionTreeNode parent){
		ExpressionTreeLeaf leaf = null;
		if (isTermVariable(token) ){
			leaf = new ExpressionTreeLeaf(Type.TERM_VARIABLE, token, parent);
		}
		// 2. formula variables: p/P, q/Q, r/R, p1, P1, , s,S....
		else if (isFormulaVariable(token)){
			leaf =  new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, token, parent);
		}	
		//3. constants: [A-Za-z][A-Za-z]+[0-9]*
		else if (isConstant(token)){
			leaf =  new ExpressionTreeLeaf(Type.CONSTANT, token, parent);
		}
		// 4. logical connectives
		else if (isConnective(token)){
			leaf = new ExpressionTreeLeaf(Type.CONNECTIVE, token, parent);
		}
		return leaf;
	}

	public static void debug(String s){
		//System.out.println(s);
	}
	protected semanticsOLD.lambdaFOL.ExpressionTree parse(LinkedList<String> tokens){
 // TODO: ADD: if the current token is negation, add an Expression Tree node of type NEG_FORMULA -- if the next token is a parenthesis, this requires a closing parenthesis, otherwise it doesn't.

		debug("\nENTER parse " + tokens + "\nThis: " + this + " " + this.requiresClosingParenthesis);
		//this.printWholeTree();
		debug("Does this required a closing parenthesis? " + this.requiresClosingParenthesis);

		if (tokens != null){
			//Base case: We've reached the end of the list: if this is the root (parent == null), exit here, else go up to the parent;
			if (tokens.isEmpty()){
				debug(" > done with input, calling parseLastChild " + this);
				return parsedLastChild(tokens);
			}	

			// Otherwise: get the first token.
			String firstToken = tokens.pop();		
			debug(" - first token: <" + firstToken +">");
			// Case 2: A closing parenthesis indicates we're done with this node.
			if (firstToken.equals(")")){
				debug(" > parseLastChild " + this + " read closing parenthesis -- does this require a closing parenthesis? " + this.requiresClosingParenthesis);
				if (this.requiresClosingParenthesis){
					debug(" >>  done with this level");
					return parsedLastChild(tokens);
				}
				else if (this.parent != null) {
					debug(" >> done with this level AND with the parent");
					return this.parent.parsedLastChild(tokens);
				}
				else {
					debug(" >> ERROR: dangling closed parenthesis: " + firstToken + " " + tokens + " " + this);
				}
			}

			String secondToken = tokens.peekFirst();
			String thirdToken = (tokens.size() >= 2)?tokens.get(1):null;
			debug(" - second token: <" + secondToken + ">");


			// START OF ATOMIC FORMULA
			if (isPredicate(firstToken) && secondToken != null && secondToken.equals("(")){
				if (this.readFirstChild || this.type != null){ // we've already read the first child.
					ExpressionTreeNode child = new ExpressionTreeNode(Type.ATOMIC_FORMULA, this);
					child.requiresClosingParenthesis = true;
					new ExpressionTreeLeaf(Type.PREDICATE, firstToken, child);
					debug(" > parsed to the predicate of an atomic formula (which is not the first child of its parent): " + child);
					tokens.pop();// no need to keep the opening parenthesis around	
					return child.parse(tokens);
				}
				else {// we haven't added any children yet, so this node is an atomic formula by itself
					this.type = Type.ATOMIC_FORMULA;
					this.requiresClosingParenthesis = true; 
					new ExpressionTreeLeaf(Type.PREDICATE, firstToken, this);
					debug(" > parsed to the predicate of an atomic formula (which is the fist child of its parent: " + this);
					tokens.pop();// no need to keep the opening parenthesis around
					return this.parse(tokens);
				}
			}
			// START OF COMPLEX FORMULA (not in parentheses)
			else if (isFormulaVariable(firstToken) && isConnective(secondToken)){
				if (this.readFirstChild){ 
					ExpressionTreeNode child = new ExpressionTreeNode(Type.COMPLEX_FORMULA, this);
					child.requiresClosingParenthesis = false;
					new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, firstToken, child);
					new ExpressionTreeLeaf(Type.CONNECTIVE, secondToken, child);
					tokens.pop();
					debug(" > parsed to complex formula: " + this);
					return child.parse(tokens);
				}
				else {
					this.type = Type.COMPLEX_FORMULA;
					this.requiresClosingParenthesis = false;
					new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, firstToken, this);
					new ExpressionTreeLeaf(Type.CONNECTIVE, secondToken, this);
					tokens.pop();
					debug(" > parsed to complex formula: " + this);
					return this.parse(tokens);
				}
			}
			// START OF QUANTIFIED FORMULA
			else if (isQuantifier(firstToken) && isTermVariable(secondToken)){
				//System.out.println("Reading in a quantifier  " + firstToken  + " " + this.readFirstChild + " ");
				//this.printWholeTree();
				ExpressionTreeNode node = this.readFirstChild?new ExpressionTreeNode(Type.QUANTIFIED_FORMULA, this): this;
				node.requiresClosingParenthesis = false;
				node.type = Type.QUANTIFIED_FORMULA;
				new ExpressionTreeLeaf(Type.QUANTIFIER, firstToken, node);
				new ExpressionTreeLeaf(Type.TERM_VARIABLE, secondToken, node, Status.QUANTIFIED);//TODO: QUANTIFIED VARIABLE (or perhaps just when changing quantified formula tree nodes to expressions?)
				tokens.pop();
				return node.parse(tokens);
			}
			// START OF LAMBDA ABSTRACTION
			else if (isLambda(firstToken) && isVariable(secondToken)){
				ExpressionTreeNode node = this.readFirstChild?new ExpressionTreeNode(Type.LAMBDA_ABSTRACTION, this): this;
				node.requiresClosingParenthesis = false;
				node.type = Type.LAMBDA_ABSTRACTION;
				//new ExpressionLeaf(Type.LAMBDA, firstToken, node); // no need to have the lambda appear in the parse tree once we know we're dealing with a lambda abstraction
				if (isFormulaVariable(secondToken)){
					new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, secondToken, node, Status.LAMBDABOUND);//TODO: LAMBDABOUND VARIABLE
				}
				else if (isTermVariable(secondToken)){
					new ExpressionTreeLeaf(Type.TERM_VARIABLE, secondToken, node,  Status.LAMBDABOUND);//TODO: LAMBDABOUND VARIABLE
				}
				else {
					debug("ERROR: reading in a lambda abstraction: what is this variable? " + secondToken);
				}
				tokens.pop();
				return node.parse(tokens);

			}
			// START OF LAMBDA APPLICATION
			else if (firstToken.equals("(") && isFormulaVariable(secondToken) && thirdToken != null && !isConnective(thirdToken)){
				debug("Reading in a lambda application " + this.type + " " + this.readFirstChild);
				ExpressionTreeNode node = (this.readFirstChild || this.type != null)?new ExpressionTreeNode(Type.LAMBDA_APPLICATION, this): this;
				node.requiresClosingParenthesis = true;
				node.type = Type.LAMBDA_APPLICATION;
				new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, secondToken, node);
				tokens.pop();
				return node.parse(tokens);
			}
			// START OF COMPLEX FORMULA OR LAMBDA APPLICATION
			else if (firstToken.equals("(")){
				debug("Reading in either a complex formula (implemented) or a lambda application whose first argument is not a formula variable");
				debug("Have we read the first child? " + this.readFirstChild);//TODO: verify that we always want to add a new child here...
				//	ExpressionTreeNode node = this.readFirstChild?new ExpressionTreeNode(Type.COMPLEX_FORMULA, this): this;
				ExpressionTreeNode node = new ExpressionTreeNode(Type.LAMBDA_APPLICATION, this);// to be changed to a COMPLEX_FORMULA when we read in the connective
				node.requiresClosingParenthesis = true;
				node.type = Type.LAMBDA_APPLICATION;// to be changed to a COMPLEX_FORMULA when we read in the connective
				debug("does this node require a closing parenthesis?: " + node.requiresClosingParenthesis);
				debug("> added child node to COMPLEX FORMULA");
				this.readFirstChild = true; // DEBUG? 
				return node.parse(tokens);
			}
			else if (isConnective(firstToken)){
				if (!this.readFirstChild){
					debug("ERROR: first child is connective: " + firstToken +  " " + this);
				}
				if (this.type == Type.LAMBDA_APPLICATION){
					this.type = Type.COMPLEX_FORMULA;
				}
				if (this.type != Type.COMPLEX_FORMULA && this.type != null){
					debug ("ERROR?? adding a connective to " + this.type);
				}
				if (this.type == null)
					this.type = Type.COMPLEX_FORMULA;
				new ExpressionTreeLeaf(Type.CONNECTIVE, firstToken, this);
				return parse(tokens);
			}
			else if (isTermVariable(firstToken)){
				new ExpressionTreeLeaf(Type.TERM_VARIABLE, firstToken, this);
				return parse(tokens);
			}
			else if (isFormulaVariable(firstToken)){
				new ExpressionTreeLeaf(Type.FORMULA_VARIABLE, firstToken, this);
				return parse(tokens);
			}
			else if (isConstant(firstToken) && this.type == Type.ATOMIC_FORMULA){
				new ExpressionTreeLeaf(Type.CONSTANT, firstToken, this);
				return parse(tokens);
			}
			else if (isNegation(firstToken)){			
				ExpressionTreeNode node = this.readFirstChild?new ExpressionTreeNode(Type.NEGFORMULA, this): this;
				node.type = Type.NEGFORMULA;
				return node.parse(tokens);
			}
			else {
				debug(" WHAT IS THIS?  " + firstToken + " " + secondToken);
				return null;
			}
		}
		return this;
	}



	public String toString(){
		String thisNode = new String(this.type + ":<");
		//String thisNode = new String("");
		if (this.children != null){
			for (semanticsOLD.lambdaFOL.ExpressionTree t : this.children){
				thisNode = thisNode + " " + t.toString();
			}
		}
		thisNode = thisNode + ">";
		return thisNode;	
	}

	public semanticsOLD.lambdaFOL.Formula toFormula(){
		semanticsOLD.lambdaFOL.Formula f = null;
		if (type == Type.ATOMIC_FORMULA){ 
			f = (semanticsOLD.lambdaFOL.AtomicFormula)this.toExpression();
		}
		else if (type == Type.COMPLEX_FORMULA){
			f = (semanticsOLD.lambdaFOL.ComplexFormula)this.toExpression();
		}
		return f;
	}
	public semanticsOLD.lambdaFOL.Expression toExpression(){
		semanticsOLD.lambdaFOL.Expression e = null;
		//System.out.println("Entering toExpression() " + this);
		switch (type){
		case LAMBDA_ABSTRACTION:
			if (this.children != null && this.children.size() == 2){
				semanticsOLD.lambdaFOL.Variable variable = children.get(0).toVariable();
				semanticsOLD.lambdaFOL.Expression body = children.get(1).toExpression();
				e = new semanticsOLD.lambdaFOL.LambdaAbstraction(variable, body);
				//System.out.println("LAMBDA ABSTRACTION: " + e);
			}
			else {
				System.out.println("ERROR: this is a lambdaExpression with more or less than two children: " + this );
			}
			break;
		case LAMBDA_APPLICATION: 
			if (this.children != null && this.children.size() == 2){
				semanticsOLD.lambdaFOL.ExpressionTree function = children.get(0);
				semanticsOLD.lambdaFOL.ExpressionTree argument = children.get(1);
				semanticsOLD.lambdaFOL.Expression f = function.toExpression();
				semanticsOLD.lambdaFOL.Expression a = argument.toExpression();
				e = new semanticsOLD.lambdaFOL.LambdaApplication(f, a);
			}
			else {
				System.out.println("ERROR: this is a lambda application with " + this.children.size() + " children: " + this );
			}
			break;
		case ATOMIC_FORMULA:
			if (this.children != null && this.children.size() > 1){
				semanticsOLD.lambdaFOL.ExpressionTree pred = children.get(0);
				if (pred.type != Type.PREDICATE){
					System.out.println("ERROR: this is an atomic formula where the first child is not a predicate: " + pred);
				}
				else {
					String predicate = pred.toString();
					semanticsOLD.lambdaFOL.Expression[] args = new semanticsOLD.lambdaFOL.Expression[children.size() -1];
					for (int i = 0; i < children.size() - 1; i++){
						args[i] = children.get(i+1).toExpression();
					}
					e = new semanticsOLD.lambdaFOL.AtomicFormula(predicate, args);
				}
			}
			else {
				System.out.println("ERROR: this is an atomic formula with more or less than one child: " + this );
			}
			break;
		case QUANTIFIED_FORMULA: 
			//System.out.println("ExpressionTreeNode.toExpression() TODO: quantified formula " + this + "Children: " + this.children.size());
			if (this.children != null && this.children.size() == 3){
				semanticsOLD.lambdaFOL.ExpressionTree quantifier = children.get(0);
				semanticsOLD.lambdaFOL.ExpressionTree variable = children.get(1);
				semanticsOLD.lambdaFOL.ExpressionTree body = children.get(2);
				if (quantifier.type == Type.QUANTIFIER && variable.type == Type.TERM_VARIABLE){
					Quantifier q = quantifier.toQuantifier();
					//System.out.println("Quantifier: " + q);
					//System.out.println("Variable: " + variable.toTermVariable());
					//System.out.println("Body: " + body.toExpression());
					e = new semanticsOLD.lambdaFOL.QuantifiedFormula(q, variable.toTermVariable(), body.toExpression());

				}
				else {
					System.out.println("ERRROR: QUANTIFIED FORMULA " + this);
				}
			}
			else {
				System.out.println("ERRROR: QUANTIFIED FORMULA " + this);
			}
			break;
		case COMPLEX_FORMULA:
			//System.out.println("ExpressionTreeNode.toExpression(): complex formula " + this);
			if (this.children != null && this.children.size() == 3){
				semanticsOLD.lambdaFOL.ExpressionTree c1 = children.get(0);
				semanticsOLD.lambdaFOL.Expression f1 = c1.toExpression();
				semanticsOLD.lambdaFOL.ExpressionTree connective = children.get(1);
				semanticsOLD.lambdaFOL.Expression.Connective c = connective.toConnective();
				semanticsOLD.lambdaFOL.ExpressionTree c2 = children.get(2);
				semanticsOLD.lambdaFOL.Expression f2 = c2.toExpression();
				e = new semanticsOLD.lambdaFOL.ComplexFormula(f1, c, f2);
			}
			else if (this.children != null && this.children.size() > 3 && this.children.size() % 2 == 1){
				semanticsOLD.lambdaFOL.ExpressionTree connective = children.get(1);
				semanticsOLD.lambdaFOL.Expression.Connective c = connective.toConnective();
				if (c == Connective.AND || c == Connective.OR){
					ArrayList<semanticsOLD.lambdaFOL.Expression> list = new ArrayList<semanticsOLD.lambdaFOL.Expression>();
					boolean allConnectivesSame = true;
					for (int i = 0; i < this.children.size(); i++){
						//System.out.println("reading child " + this.children.get(i));
						
						if (i % 2 == 0){
							semanticsOLD.lambdaFOL.Expression f = this.children.get(i).toExpression();
							//System.out.println(" -> new expression: " + f);
							list.add(f);
						}
						else {
							Connective ci = this.children.get(i).toConnective();
							//System.out.println(" -> new connective: " + ci);
							if (ci == null || ci != c){
								allConnectivesSame = false;
								break;
							}
						}
					}
					if (allConnectivesSame){
						e = semanticsOLD.lambdaFOL.FormulaList.createFormulaList(c,list);
						//System.out.println("NEW FORMULA LIST: " + e);
					}
				}
			}
			else {
				// this shouldn't happen.
				System.out.println("ERROR: this is a complex formula that doesn't have three children: " + this  + " " + this.children.size());
				for (semanticsOLD.lambdaFOL.ExpressionTree c: this.children){
					System.out.println("\t " + c);
				}
				System.exit(0);
			}
			break;
		case NEGFORMULA:
			int nChildren = (this.children != null)?this.children.size():0;
			//System.out.println("ExpressionTreeNode.toExpression() NEGATED FORMULA" + this + " " + nChildren); 
			if (this.children != null && this.children.size() == 1){
				semanticsOLD.lambdaFOL.Expression f = this.children.get(0).toExpression();
				if (f instanceof semanticsOLD.lambdaFOL.Formula || f instanceof semanticsOLD.lambdaFOL.LambdaApplication)
					e = new semanticsOLD.lambdaFOL.NegatedFormula(f);
				else {
					System.out.println("ERROR: negation of somethign other than a formula or lambda application: " + f.getClass() + ": " + f);
					System.exit(0);
				}
			}
			else {
				System.out.println("ERROR: a negated formula with " + nChildren + " children");
				for (semanticsOLD.lambdaFOL.ExpressionTree c: this.children){
					System.out.println("\t " + c);
				}
				System.exit(0);
			}
			break;
		default: System.out.println("ExpressionTreeNode.toExpression() ERROR: this should not be an internal node " + this); 
		break;
		}
		return e;
	}
}
