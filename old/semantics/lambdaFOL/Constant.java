package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author juliahmr
 * Constants consist of a (final) string. Equality and equivalence depend on string equality.
 */
public class Constant extends Term {

	protected final String constant;
	final public ArrayList<String> yield;
	final public ArrayList<Expression.TerminalType> yieldTypes;
	
	protected Constant(String c){
		constant = c;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public String toString(){
		return new String(constant);
	}

	public static Expression createConstant(String string) {
		if (string != null)
			return new Constant(string);
		else return null;
	}

	public Constant replace(Variable v, Expression e){
		return this;
	}

	public boolean equals(Object o){
		if (o != null && o instanceof Constant && this.constant.equals(((Constant) o).constant)){
			return true;
		}
		else return false;
	}

	public int hashCode(){
		return constant.hashCode();
	}

	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable,Variable> equivalenceMapThis, HashMap<Variable,Variable> equivalenceMapThat){
		//System.out.println("Constants:  " + this + " " + e  + " " + (e instanceof Constant) + " " +  this.equals(e));
		if (e instanceof Constant && this.equals(e)){
			return true;
		}
		else return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<Expression.TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(constant);
		return yield;
	}
	protected ArrayList<Expression.TerminalType> initYieldTypes(){
		ArrayList<Expression.TerminalType> yieldTypes = new ArrayList<Expression.TerminalType>();
		yieldTypes.add(Expression.TerminalType.CONSTANT);
		return yieldTypes;
	}
	
	public HashSet<Variable> getAllVariables() {
		return null;	
	}
	
	protected Expression renameVar(Variable v, Variable v1) {
		return this;
	}
}
