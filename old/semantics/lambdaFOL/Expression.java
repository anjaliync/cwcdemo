package semanticsOLD.lambdaFOL;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import semanticsOLD.generation.Configuration;

public class Expression implements Cloneable{
	public static SemLexicon lexicon;
	public static final String AND = "∧";//TODO: rename
	public static final String OR = "∨";//TODO: rename
	public static final String IMPLIES = "⟶";//TODO: rename
	public static final String EXISTS = "∃";//TODO: rename
	public static final String FORALL = "∀";//TODO: rename
	public static final String UNIQUE = "ι";//TODO: rename
	public static final String LAMBDA = "λ";
	public static final String NEG = "¬";

	enum Quantifier {FORALL, EXIST, UNIQUE};
	enum Connective {AND, OR, IMPLIES};
	enum VarType {EVENT, ENTITY, TIME, FORMULA, NIL} 




	/**
	 * @author juliahmr
	 * Used for computing coverage vectors
	 */
	public enum TerminalType {QUANTIFIER, CONNECTIVE, TERMVARIABLE, CONSTANT, PREDICATE, LAMBDA, FORMULAVARIABLE, NEGATION}

	public Expression replace(semanticsOLD.lambdaFOL.Variable var, Expression e){
		return null;
	}

	/* 
	 * since Java doesn't have multiple inheritance, this returns an Expression (which is either a TermVariable or a FormulaVariable)
	 */
	public static Expression createNewVariable(VarType t, semanticsOLD.lambdaFOL.Variable.Status s){
		switch (t){
		case EVENT:
		case ENTITY:
		case TIME:
			return new TermVariable(t,s);
		case FORMULA:
			return new FormulaVariable(s);
		case NIL:
			return null;
		}
		return null;
	}
	/* 
	 * since Java doesn't have multiple inheritance, this returns an Expression (which is either a TermVariable or a FormulaVariable)
	 */
	public static Expression createNewVariable(VarType t){	
		switch (t){
		case EVENT:
		case ENTITY:
		case TIME:
			return new TermVariable(t);
		case FORMULA:
			return new FormulaVariable();
		case NIL:
			return null;
		}
		return null;
	}
	public static String[] tokenizeString(String string){
		// 1. insert blanks around all parentheses, commas and connectives
		String tmp = string.replaceAll("\\(", " \\( ");
		tmp = tmp.replaceAll("\\)", " \\) ");
		tmp = tmp.replaceAll(",", " ");
		tmp = tmp.replaceAll("&", " " + AND +" ");
		tmp = tmp.replaceAll("->", " " + IMPLIES + " ");
		tmp = tmp.replaceAll(AND, " " + AND +" ");
		tmp = tmp.replaceAll(OR, " " + OR +" ");
		tmp = tmp.replaceAll(IMPLIES, " " + IMPLIES + " ");
		tmp = tmp.replaceAll(FORALL, " " + FORALL + " ");
		tmp = tmp.replaceAll(EXISTS, " " + EXISTS + " ");
		tmp = tmp.replaceAll(UNIQUE, " " + UNIQUE + " ");
		// remove full stops in lambda applications
		tmp = tmp.replaceAll("\\.", " ");
		tmp = tmp.replaceAll(LAMBDA, " " + LAMBDA + " ");
		tmp = tmp.replaceAll("\\^", " " + LAMBDA + " " );//lambda
		tmp = tmp.replaceAll(NEG, " " + NEG + " ");
		tmp = tmp.replaceAll(" NOT\\(", " " + NEG + "( ");
		String[] tokens = tmp.trim().split("\\s+");
		// DEBUGGING
		//		System.out.println(string + " SPLIT TO <" + tokens[0] + ">" );
		//		for (int i = 1; i < tokens.length; i++){
		//			System.out.print(", <" + tokens[i] + ">");
		//		}
		//		System.out.println();
		// END DEBUGGING
		return tokens;
	}

	/**
	 * @param e the expression whose variables need to be replaced in this expression
	 * @return a copy of this expression in which all variables that also occur in e have been renamed to new variables that don't occur in e
	 */
	public Expression replaceMyVars(Expression e){
		Expression newE = this;
		HashSet<semanticsOLD.lambdaFOL.Variable> eVars = e.getAllVariables();
		HashSet<semanticsOLD.lambdaFOL.Variable> myVars = this.getAllVariables();
		if (eVars != null && myVars != null){
			eVars.retainAll(myVars);
			if (!eVars.isEmpty()){
				newE = newE.renameVariables(eVars);
				//System.out.println("\nRename variables: " + eVars );
				//System.out.println("\tfrom expression: "  + e);
				//System.out.println("\tin the old expression: " + this);
				//System.out.println("\tto the new expression: "  + newE + "\n");
			}
		}
		return newE;
	}
	public static Expression parseString(String string){
		String[] tokens =  tokenizeString(string);
		//System.out.println("\n\nENTER parseString(" + string + ")");
		Expression e = parseTokens(tokens);
		//System.out.println("\n### EXIT parseString(" + string + "): return " + e.getClass() + " "  + e + "\n\n");
		resetStringToVarMaps();
		return e;
	}

	public static void resetStringToVarMaps(){
		TermVariable.resetStringToVarMap();
		FormulaVariable.resetStringToVarMap();

	}
	private static Expression parseTokens(String[] tokens) {
		if (tokens == null)
			return null;
		if (tokens.length == 1){
			return parseToken(tokens[0]);
		}
		else {
			ExpressionTree tree = ExpressionTree.parse(tokens);
			//System.out.println("After ExpressionTree.parse: " + tree);
			return tree.toExpression();
		}
	}

	// individual tokens are either variables or constants 
	public static Expression parseToken(String token){
		// 1. term variables
		if (ExpressionTree.isTermVariable(token) ){
			return TermVariable.createTermVariable(token);
		}
		// 2. formula variables:
		else if (ExpressionTree.isFormulaVariable(token)){
			return FormulaVariable.createFormulaVariable(token);
		}
		// 3. constants: [A-Za-z][A-Za-z]+[0-9]*
		else if (ExpressionTree.isConstant(token)){
			return semanticsOLD.lambdaFOL.Constant.createConstant(token);
		}
		return null;
	}

	public LambdaAbstraction typeRaise(){
		FormulaVariable T = FormulaVariable.createFormulaVariable("T");
		LambdaApplication Te = new LambdaApplication(T, this); 
		return new LambdaAbstraction(T, Te);
	}
	public static void testSuite(){
		parseString("¬P");
		parseString("¬sleep(mary)");
		
		
		parseString("¬(P & Q) ");
		LambdaAbstraction notPx = (LambdaAbstraction) parseString("^P.^x.¬(P x) ");
		LambdaAbstraction fx = (LambdaAbstraction) parseString("^x.sleep(x)");
		semanticsOLD.lambdaFOL.Constant maryC = (semanticsOLD.lambdaFOL.Constant) parseString("mary");
		LambdaAbstraction notfx = (LambdaAbstraction) notPx.applyTo(fx);
		System.out.println("not sleep: " + notfx);
		Expression marynotsleep = notfx.applyTo(maryC);
		System.out.println("mary not sleep: " + marynotsleep);
		
		parseString("(P (Q x) )");
		parseString("^x.(P (Q x) )");
		parseString("^R.R");
		parseString("^x.(((Q ^R.R) e) x)");

		Expression john = parseString("john");
		Expression mary = parseString("mary");
		Expression bill = parseString("bill");
		parseString("x");
		parseString("sleep(mary)");
		parseString("love(mary,john)");
		//		parseString("hate(mary,x)");
		//		parseString("hate(x,y)");
		//		parseString("hate(x,x)");
		parseString("love(mary,john) & hate(mary,bill)");
		//		//TODO: conjunction of three arguments
		parseString("(love(mary,john) & hate(mary,bill)) -> hate(john,bill)");
		//		parseString("(love(mary,x) & hate(mary,y)) -> hate(x,y)");
		LambdaAbstraction selfLove = (LambdaAbstraction)parseString("^x.love(x,x)");
		Expression johnloveshimself = selfLove.applyTo(john);
		System.out.println("Does john love himself? " + johnloveshimself);
		LambdaAbstraction hate = (LambdaAbstraction)parseString("^x.^y.hate(x,y)");
		LambdaAbstraction johnhatessomeone = (LambdaAbstraction)hate.applyTo(john);
		Expression johnhatesmary = johnhatessomeone.applyTo(mary);
		System.out.println("Who does john hate? " + johnhatessomeone);
		System.out.println("john hates mary: " + johnhatesmary);
		parseString("^x.^y.(hate(x,y)&love(y,x))");//complex formulas have to be enclosed in parentheses

		Expression lovetriangle = ((LambdaAbstraction)((LambdaAbstraction)parseString("^x.^y.((love(mary,x) & love(y,mary)) -> hate(x,y))")).applyTo(john)).applyTo(bill);
		System.out.println("Love triangle: " + lovetriangle);
		//System.exit(0);

		//parseString("^x.x");
		//	parseString("^P.P");
		//	parseString("^P.(P x)"); 
		LambdaAbstraction sleep = (LambdaAbstraction)parseString("^P.^e.(P ^x.sleep(e, x))");
		LambdaAbstraction eat = (LambdaAbstraction)parseString("^P.^Q.^e(Q ^x.(P ^y.eat(e,x,y)))");
		System.out.println("EAT: " + eat);
		LambdaAbstraction lunch = (LambdaAbstraction)parseString("^P.(lunch(x) & (P x))");
		System.out.println("LUNCH: " + lunch);
		LambdaAbstraction eatlunch = (LambdaAbstraction) eat.applyTo(lunch);
		System.out.println("eat lunch: " + eatlunch);
		LambdaAbstraction man = (LambdaAbstraction)parseString("^x.man(x)");
		LambdaAbstraction a = (LambdaAbstraction)parseString("^P.^Q.((P x) & (Q x))"); 

		Expression aman = a.applyTo(man);
		System.out.println("a man: " + aman);
		Expression amansleeps = sleep.applyTo(aman);
		System.out.println("a man sleeps: " + amansleeps);
		Expression amaneatslunch = eatlunch.applyTo(aman);
		System.out.println("a man eats lunch: " + amaneatslunch);

		LambdaAbstraction happily = (LambdaAbstraction) parseString("^Q.^P.^e.((P ^x.(((Q ^R.R) e) x)) & manner(e, happy))");
		System.out.println("HAPPILY:"  + happily);
		;//λQ.λP λe.[P λx.[((Q λR.R) e) x) ∧  manner(e, happy)]]	
		LambdaAbstraction tall = (LambdaAbstraction) parseString("^P.^x.(tall(x) & (P x))");
		Expression tallman = tall.applyTo(man);
		System.out.println("tall man: " +  tallman);
		LambdaAbstraction amanTR = aman.typeRaise();
		System.out.println("a man type-raised: " + amanTR);
		Expression amanTRsleeps = amanTR.applyTo(sleep);
		System.out.println("a man type-raised sleeps: " + amanTRsleeps);
		Expression amanTReatslunch = amanTR.applyTo(eatlunch);
		System.out.println("a man type-raised eats lunch: " + amanTReatslunch);
	}

	public static void readLexicon(String fileName){//todo: trim blanks from cats and words
		
		boolean debug = Configuration.DEBUG;
		
		Charset charset = Charset.forName("UTF-8");
		if(debug) {
			System.out.println("Reading lexicon " + fileName);
		}
		Path filePath = Paths.get(fileName);
		lexicon = new SemLexicon();
		try (BufferedReader reader = Files.newBufferedReader(filePath, charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {

				if (!line.startsWith("//") && !line.isEmpty()){
					if(debug) {
						System.out.println("\n" + line);
					}
					String[] tokens = line.split("\t");
					if (tokens != null && tokens.length == 3){
						String word = tokens[0].trim();
						String cat = tokens[1].trim();
						String sem = tokens[2].trim();

						lexicon.addEntry(word, cat, sem);
					}
					else {
						if(debug) {
							System.out.println("ERROR readlexicon: Can't read this line: " + tokens.length);
						}
					}
				}
			}
		} catch (IOException x) {
			System.err.format("readLexicon -- IOException: %s%n", x);
		}

	}

	public static void testLexicon(){
		LambdaAbstraction a  = (LambdaAbstraction)lexicon.getFirstSense("a", "NP/N");
		System.out.println("a: " + a);
		LambdaAbstraction the  = (LambdaAbstraction)lexicon.getFirstSense("the", "NP/N");
		System.out.println("the: " + the);
		LambdaAbstraction lunch = (LambdaAbstraction)lexicon.getFirstSense("lunch", "NP");	
		System.out.println("lunch: " + lunch);
		LambdaAbstraction food = (LambdaAbstraction)lexicon.getFirstSense("food", "N");	
		System.out.println("food: " + food);
		LambdaAbstraction greasy = (LambdaAbstraction)lexicon.getFirstSense("greasy", "N/N");	
		System.out.println("greasy: " + greasy);
		LambdaAbstraction bob = (LambdaAbstraction)lexicon.getFirstSense("Bob", "NP");
		System.out.println("bob: " + bob);
		LambdaAbstraction mary = (LambdaAbstraction)lexicon.getFirstSense("Mary", "NP");
		System.out.println("mary: " + mary);
		LambdaAbstraction eat = (LambdaAbstraction)lexicon.getFirstSense("eat", "(S\\NP)/NP");
		System.out.println("eat: " + eat);
		LambdaAbstraction at = (LambdaAbstraction)lexicon.getFirstSense("at", "(N\\N)/NP");		
		System.out.println("at: " + at);
		LambdaAbstraction happily = (LambdaAbstraction)lexicon.getFirstSense("happily", "(S\\NP)/(S\\NP)");		
		System.out.println("happily: " + happily);
		LambdaAbstraction say = (LambdaAbstraction)lexicon.getFirstSense("say", "(S\\NP)/S");		
		System.out.println("say: " + say);
		LambdaAbstraction today = (LambdaAbstraction)lexicon.getFirstSense("today", "S/S");		
		System.out.println("today: " + today);
		LambdaAbstraction IF = (LambdaAbstraction)lexicon.getFirstSense("if", "(S/S)/S");		
		System.out.println("if: " + IF);
		LambdaAbstraction be = (LambdaAbstraction)lexicon.getFirstSense("be", "(S\\NP)/(S\\NP)");
		System.out.println("be: " + be);
		LambdaAbstraction was = (LambdaAbstraction)lexicon.getFirstSense("was", "(S\\NP)/(S\\NP)");
		System.out.println("was: " + was);
		LambdaAbstraction happy = (LambdaAbstraction)lexicon.getFirstSense("happy", "S\\NP");
		System.out.println("happy: " + happy);
		LambdaAbstraction sleep = (LambdaAbstraction)lexicon.getFirstSense("sleep", "S\\NP");
		System.out.println("sleep: " + sleep);
		LambdaAbstraction dream = (LambdaAbstraction)lexicon.getFirstSense("dream", "S\\NP");
		System.out.println("dream: " + dream);
		LambdaAbstraction  andNP = (LambdaAbstraction)lexicon.getFirstSense("and", "(NP\\NP)/NP");
		System.out.println("and (NP conj): " + andNP);
		LambdaAbstraction  andVP = (LambdaAbstraction)lexicon.getFirstSense("and", "((S\\NP)\\(S\\NP))/(S\\NP)");
		System.out.println("and (VP conj): " + andVP);
		if (lunch != null && at != null && food != null && the != null && eat != null){
			LambdaAbstraction atlunch = (LambdaAbstraction)at.applyTo(lunch);
			System.out.println("at lunch: \n" + atlunch + "\n");
			LambdaAbstraction greasyfood = (LambdaAbstraction)greasy.applyTo(food);
			LambdaAbstraction greasyfoodatlunch = (LambdaAbstraction)atlunch.applyTo(greasyfood);
			System.out.println("greasy food at lunch: \n" + greasyfoodatlunch + "\n");
			LambdaAbstraction thefoodatlunch = (LambdaAbstraction)the.applyTo(greasyfoodatlunch);
			System.out.println("the greasy food at lunch: \n" + thefoodatlunch + "\n");
			LambdaAbstraction eatthefoodatlunch = (LambdaAbstraction)eat.applyTo(thefoodatlunch);
			System.out.println("ate the greasy food at lunch: \n" + eatthefoodatlunch + "\n");
			LambdaAbstraction happilyeatthefoodatlunch = (LambdaAbstraction)happily.applyTo(eatthefoodatlunch);
			System.out.println("happily ate the greasy food at lunch: \n"  + happilyeatthefoodatlunch + "\n");

			LambdaAbstraction bobeatthefoodatlunch = (LambdaAbstraction)happilyeatthefoodatlunch.applyTo(bob);		
			System.out.println("Bob happily ate the greasy food at lunch: \n" + bobeatthefoodatlunch + "\n");
			LambdaAbstraction bobeatthefoodatlunchtoday = (LambdaAbstraction)today.applyTo(bobeatthefoodatlunch);
			System.out.println("Bob happily ate the greasy food at lunch today: \n" + bobeatthefoodatlunchtoday + "\n");
			LambdaAbstraction saybobeatthefoodatlunch = (LambdaAbstraction)say.applyTo(bobeatthefoodatlunchtoday);
			System.out.println("says Bob happily ate the greasy food at lunch today: \n" + saybobeatthefoodatlunch + "\n");
			LambdaAbstraction marysaybobeatthefoodatlunch = (LambdaAbstraction)saybobeatthefoodatlunch.applyTo(mary);
			System.out.println("Mary says Bob happily ate the greasy food at lunch today: \n" + marysaybobeatthefoodatlunch + "\n");
			LambdaAbstraction behappy = (LambdaAbstraction)be.applyTo(happy);
			System.out.println("is happy \n" + behappy+ "\n");
			LambdaAbstraction bobishappy = (LambdaAbstraction)behappy.applyTo(bob);
			System.out.println("bob is happy \n" + bobishappy+ "\n");
			LambdaAbstraction washappy = (LambdaAbstraction)was.applyTo(happy);
			System.out.println("was happy \n" + washappy+ "\n");		
			LambdaAbstraction bobwashappy = (LambdaAbstraction)washappy.applyTo(bob);
			System.out.println("bob was happy \n" + bobwashappy+ "\n");
			LambdaAbstraction ifbobishappy = (LambdaAbstraction)IF.applyTo(bobishappy);
			System.out.println("if bob is happy \n" + ifbobishappy+ "\n");
			LambdaAbstraction bobsleeps = (LambdaAbstraction)sleep.applyTo(bob);
			System.out.println("bob sleeps \n" + bobsleeps+ "\n");
			LambdaAbstraction ifbobishappybobsleeps = (LambdaAbstraction)ifbobishappy.applyTo(bobsleeps);
			System.out.println("if bob is happy bob sleeps\n" + ifbobishappybobsleeps + "\n");// problem -- these are two different bobs!
			//		LambdaAbstraction happilyeats = (LambdaApplication)happily.composeWith(eat);
			//			System.out.println("testing composition: happily eats:\n" + happilyeats + "\n");
			LambdaAbstraction andMary= (LambdaAbstraction)andNP.applyTo(mary);
			System.out.println("and Mary \n" + andMary+ "\n");
			LambdaAbstraction bobandmary= (LambdaAbstraction)andMary.applyTo(bob);
			System.out.println("Bob and Mary \n" + bobandmary+ "\n");
			LambdaAbstraction bobandmaryeat = (LambdaAbstraction)sleep.applyTo(bobandmary);
			System.out.println("Bob and Mary sleep\n" + bobandmaryeat+ "\n");// potential problem: this only gives us the collective reading (one event in which both sleep)
			LambdaAbstraction bobandmaryTR = bobandmary.typeRaise();
			System.out.println("Bob and Mary (TR) \n" + bobandmaryTR + "\n");
			System.out.println("eat\n" + bobandmaryTR + "\n");

			LambdaAbstraction bobandmarysleepTR = (LambdaAbstraction)bobandmaryTR.applyTo(sleep);
			System.out.println("Bob and Mary sleep (TR)\n" + bobandmarysleepTR+ "\n");// 

			LambdaAbstraction anddream = (LambdaAbstraction)andVP.applyTo(dream);
			System.out.println("and dream\n" + anddream+ "\n");
			LambdaAbstraction sleepanddream = (LambdaAbstraction)anddream.applyTo(sleep);
			System.out.println("sleeps and dreams\n" + sleepanddream+ "\n");
			LambdaAbstraction bobsleepsanddreams = (LambdaAbstraction)sleepanddream.applyTo(bob);
			System.out.println("bob sleeps and dreams\n" + bobsleepsanddreams+ "\n");// potential problem: this gives us one event with two copies of Bob...
		}

	}

	public static boolean testEquivalence(){
		boolean retval = true;
		semanticsOLD.lambdaFOL.AtomicFormula f1 = (semanticsOLD.lambdaFOL.AtomicFormula)parseString("man(x,y)");
		semanticsOLD.lambdaFOL.AtomicFormula f2 = (semanticsOLD.lambdaFOL.AtomicFormula)parseString("man(x,y)");
		semanticsOLD.lambdaFOL.AtomicFormula g = (semanticsOLD.lambdaFOL.AtomicFormula)parseString("man(y,x)");
		semanticsOLD.lambdaFOL.AtomicFormula h = (semanticsOLD.lambdaFOL.AtomicFormula)parseString("man(x,x)");
		retval &= testEquivalence(f1, f2, true);
		retval &= testEquivalence(f1, g, true);
		retval &= testEquivalence(f1, h, false);
		//LambdaAbstraction l1 = (LambdaAbstraction)parseString("λx.man(x)");
		//LambdaAbstraction l2 = (LambdaAbstraction)parseString("λy.man(y)");
		//LambdaAbstraction l3 = (LambdaAbstraction)parseString("λx.(man(x) ∧ sleep(x))");
		//LambdaAbstraction l4 = (LambdaAbstraction)parseString("λy.(man(y) ∧ sleep(y))");
		//LambdaAbstraction l5 = (LambdaAbstraction)parseString("λy.(man(y) ∧ sleep(x))");
		QuantifiedFormula f3 = (QuantifiedFormula)parseString("∃x.(man(x) ∧ sleep(x))");
		QuantifiedFormula f4 = (QuantifiedFormula)parseString("∃y.(man(y) ∧ sleep(y))");
		QuantifiedFormula f5 = (QuantifiedFormula)parseString("∃y.(man(y) ∧ sleep(x))");
		QuantifiedFormula f6 = (QuantifiedFormula)parseString("∃y.(sleep(y) ∧ man(y))");
		retval &= testEquivalence(f3, f4, true);
		retval &= testEquivalence(f3, f5, false);
		retval &= testEquivalence(f3, f6, true);
		LambdaAbstraction bob = (LambdaAbstraction)parseString("λQ.(ιx.(Bob(x) ∧ (Q x)))");
		LambdaAbstraction bob2 = (LambdaAbstraction)parseString("λP.(ιy.((P y) ∧ Bob(y)))");
		retval &= testEquivalence(bob, bob2, true);
		return retval;
	}

	public static boolean testEquivalence(Expression e1, Expression e2, boolean retval){
		System.out.println("EQUIVALENCE TEST: " + e1 + " " + e2 + " desired outcome: " + retval);
		if (e1.isEquivalentTo(e2) == retval){
			if (retval == true) System.out.println("Equivalence check passed: " + e1 + " is (correctly) equivalent to " + e2 + "\n");
			else if (retval == false) System.out.println("Equivalence check passed: " + e1 + " is (correctly) not equivalent to " + e2+ "\n");
			return true;
		}
		else {
			if (retval == true) System.out.println("Equivalence check failed: " + e1 + " is (incorrectly) not equivalent to " + e2+ "\n");
			else if (retval == false) System.out.println("Equivalence check failed: " + e1 + " is (incorrectly) equivalent to " + e2+ "\n");
			return false;
		}
	}
	public boolean isEquivalentTo(Expression e){
		return isEquivalentModuloVarRenaming(e, new HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable>(), new HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable>());
	}

	/**
	 * @param e The expression that may or may not be equivalent to this.
	 * @param varEquivalenceMapThis A map from the variables in this expression to the variables in e.
	 * @param varEquivalenceMapThat A map from the variables in e to the variables in this expression.
	 * @return true iff e and this are equivalent modulo variable renaming (for now, no flattening or reordering of complex formulas, and no moving of quantifiers, or check of equivalent quantifier scopes; we also assume both this and e have been fully reduced).
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat) {
		// TODO Auto-generated method stub
		return false;
	}

	public ArrayList<String> yield(){
		return null;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return null;
	}

	public static boolean testComposition(){
		boolean retval = true;
		System.out.println("TESTING COMPOSITION");
		LambdaAbstraction bob = (LambdaAbstraction)lexicon.getFirstSense("Bob", "NP");
		System.out.println("\t bob: " + bob  + " Yield: " + bob.yield() + " " + bob.yieldTypes());
		LambdaAbstraction eat = (LambdaAbstraction)lexicon.getFirstSense("eat", "(S\\NP)/NP");
		System.out.println("\t eat: " + eat+ " Yield: " + eat.yield() + " " + eat.yieldTypes());
		LambdaAbstraction lunch = (LambdaAbstraction)lexicon.getFirstSense("lunch", "NP");	
		System.out.println("\t lunch: " + lunch + " Yield: " + lunch.yield() + " " + lunch.yieldTypes());
		LambdaAbstraction happily = (LambdaAbstraction)lexicon.getFirstSense("happily", "(S\\NP)/(S\\NP)");
		System.out.println("\t happily: " + happily + " Yield: " + happily.yield() + " " + happily.yieldTypes());
		LambdaAbstraction tall = (LambdaAbstraction)lexicon.getFirstSense("tall", "N/N");
		System.out.println("\t tall: " + tall + " Yield: " + tall.yield() + " " + tall.yieldTypes());
		LambdaAbstraction greasy = (LambdaAbstraction)lexicon.getFirstSense("greasy", "N/N");
		System.out.println("\t greasy: " + greasy + " Yield: " + greasy.yield() + " " + greasy.yieldTypes());
		LambdaAbstraction food = (LambdaAbstraction)lexicon.getFirstSense("food", "N");
		System.out.println("\t food: " + food + " Yield: " + food.yield() + " " + food.yieldTypes());

		System.out.println("1) Just application...");
		LambdaAbstraction greasyfood = (LambdaAbstraction)greasy.applyTo(food);
		System.out.println("\t greasy food: " + greasyfood + " Yield: " + greasyfood .yield() + " " + greasyfood.yieldTypes());
		LambdaAbstraction greasygreasyfood = (LambdaAbstraction)greasy.applyTo(greasyfood);
		System.out.println("\t greasy greash food: " + greasygreasyfood + " Yield: " + greasygreasyfood .yield() + " " + greasygreasyfood.yieldTypes());

		LambdaAbstraction eatlunch = (LambdaAbstraction)eat.applyTo(lunch);
		System.out.println("\t eat lunch: " + eatlunch + " Yield: " + eatlunch.yield() + " " + eatlunch.yieldTypes());
		LambdaAbstraction happilyeatlunch = (LambdaAbstraction)happily.applyTo(eatlunch);
		System.out.println("\t happily eat lunch: " + happilyeatlunch + " Yield: " + happilyeatlunch.yield() + " " + happilyeatlunch.yieldTypes());

		Expression bobeatlunch = eatlunch.applyTo(bob);
		System.out.println("\t bob eats lunch: " + bobeatlunch+ " Yield: " + bobeatlunch.yield() + " " + bobeatlunch.yieldTypes());
		LambdaAbstraction bobhappilyeatlunch = (LambdaAbstraction)happilyeatlunch.applyTo(bob);
		System.out.println("\t bob happily eats lunch: " + bobhappilyeatlunch + " Yield: " + bobhappilyeatlunch.yield() + " " + bobhappilyeatlunch.yieldTypes());
		System.out.println("2) Typeraising and composition..");
		LambdaAbstraction bobTR = bob.typeRaise();
		System.out.println("bob (TR): " + bobTR);
		LambdaAbstraction bobeat = bobTR.compose_b1(eat);
		System.out.println("bob eat: " + bobeat);
		Expression bobTReatlunch = bobeat.applyTo(lunch);
		System.out.println("bobTR eat lunch: " + bobTReatlunch);

		retval &=  bobeatlunch.isEquivalentTo(bobTReatlunch);
		System.out.println("composition test so far: " + retval);

		LambdaAbstraction happilyeat = happily.compose_b1(eat);
		LambdaAbstraction happilyeatlunchB1 = (LambdaAbstraction)happilyeat.applyTo(lunch);
		System.out.println("happily eat lunch (b1): " + happilyeatlunchB1);
		System.out.println("happily eat lunch:      " + happilyeatlunch);

		retval &=  happilyeatlunch.isEquivalentTo(happilyeatlunchB1);
		LambdaAbstraction greasygreasy = greasy.compose_b1(greasy);
		System.out.println("greasy greasy: " + greasygreasy);
		LambdaAbstraction greasygreasyfood2 = (LambdaAbstraction)greasygreasy.applyTo(food);
		System.out.println("greasy greasy food (1): " + greasygreasyfood);
		System.out.println("greasy greasy food (2): " + greasygreasyfood2);
		retval &=  greasygreasyfood.isEquivalentTo(greasygreasyfood2);
		System.out.println("composition test so far: " + retval);
		LambdaAbstraction today = (LambdaAbstraction)lexicon.getFirstSense("today", "S/S");
	//	LambdaAbstraction eat = (LambdaAbstraction)lexicon.getFirstSense("eat", "(S\\NP)/NP");
	//	LambdaAbstraction lunch = (LambdaAbstraction)lexicon.getFirstSense("lunch", "NP");
	//	LambdaAbstraction bob = (LambdaAbstraction)lexicon.getFirstSense("Bob", "NP");
		LambdaAbstraction todaybobeatlunch = (LambdaAbstraction)today.applyTo((LambdaAbstraction)((LambdaAbstraction)eat.applyTo(lunch)).applyTo(bob));
			
		System.out.println("today eat:        " + today + " " + eat);
		LambdaAbstraction todayeat = today.compose_b2(eat);
		System.out.println("todayeat:         " + todayeat);
		LambdaAbstraction todayeatlunch = (LambdaAbstraction)todayeat.applyTo(lunch);
		System.out.println("todayeatlunch:    " + todayeatlunch);
		LambdaAbstraction bobtodayeatlunch = (LambdaAbstraction)todayeatlunch.applyTo(bob);
		System.out.println("bobtodayeatlunch: " + bobtodayeatlunch);
		System.out.println("todaybobeatlunch: " + todaybobeatlunch);
		retval &= todaybobeatlunch.isEquivalentTo(bobtodayeatlunch);
	
		
		return retval;

	}

	public static boolean testCoverageVectors(){
		boolean retval = true;
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> out = null;
		System.out.println("\n------------------ TESTING COVERAGE VECTORS ------------------\n");
		LambdaAbstraction bob = (LambdaAbstraction)lexicon.getFirstSense("Bob", "NP");
		//System.out.println("\t bob: " + bob  + " Yield: " + bob.yield() + " " + bob.yieldTypes());
		LambdaAbstraction eat = (LambdaAbstraction)lexicon.getFirstSense("eat", "(S\\NP)/NP");
		//System.out.println("\t eat: " + eat+ " Yield: " + eat.yield() + " " + eat.yieldTypes());
		LambdaAbstraction lunch = (LambdaAbstraction)lexicon.getFirstSense("lunch", "NP");	
		//System.out.println("\t lunch: " + lunch + " Yield: " + lunch.yield() + " " + lunch.yieldTypes());
		LambdaAbstraction happily = (LambdaAbstraction)lexicon.getFirstSense("happily", "(S\\NP)/(S\\NP)");
		//System.out.println("\t happily: " + happily + " Yield: " + happily.yield() + " " + happily.yieldTypes());

		//System.out.println("1) Just application...");
		LambdaAbstraction eatlunch = (LambdaAbstraction)eat.applyTo(lunch);
		//System.out.println("\t eat lunch: " + eatlunch + " Yield: " + eatlunch.yield() + " " + eatlunch.yieldTypes());
		LambdaAbstraction happilyeatlunch = (LambdaAbstraction)happily.applyTo(eatlunch);
		//System.out.println("\t happily eat lunch: " + happilyeatlunch + " Yield: " + happilyeatlunch.yield() + " " + happilyeatlunch.yieldTypes());

		Expression bobeatlunch = eatlunch.applyTo(bob);
		//System.out.println("\t bob eats lunch: " + bobeatlunch+ " Yield: " + bobeatlunch.yield() + " " + bobeatlunch.yieldTypes());
		LambdaAbstraction bobhappilyeatlunch = (LambdaAbstraction)happilyeatlunch.applyTo(bob);
		//System.out.println("\t bob happily eats lunch: " + bobhappilyeatlunch + " Yield: " + bobhappilyeatlunch.yield() + " " + bobhappilyeatlunch.yieldTypes());



		GenerationTarget bobhappilyeatslunchTarget = new GenerationTarget(bobhappilyeatlunch);

		System.out.println("<Bob happily eats lunch> vs <Bob>");
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> bobItems = bobhappilyeatslunchTarget.computeInstantiations(bob);
		if (bobItems != null && bobItems.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		System.out.println("<Bob happily eats lunch> vs <eats lunch>");
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> eatLunchItems  = bobhappilyeatslunchTarget.computeInstantiations(eatlunch);
		if (eatLunchItems != null && eatLunchItems.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		System.out.println("Debugging application of chart items: <eats lunch> and <Bob> (target for both: <bob happily eats lunch>)");
		if (eatLunchItems != null && bobItems != null){
			for (semanticsOLD.lambdaFOL.ChartGenItem e: eatLunchItems){
				for (semanticsOLD.lambdaFOL.ChartGenItem b: bobItems){
					bobhappilyeatslunchTarget.debugApplication(e,b);
				}
			}
		}

		System.out.println("<Bob happily eats lunch> vs <Bob eats lunch>");
		out = bobhappilyeatslunchTarget.computeInstantiations(bobeatlunch);
		if (out != null && out.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		System.out.println("\n<Bob happily eats lunch> vs <Bob happily eats lunch>");
		out = bobhappilyeatslunchTarget.computeInstantiations(bobhappilyeatlunch);
		if (out != null && out.size() > 0){
			System.out.println("Success!\n");

		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		//LambdaAbstraction eatlunch1 = (LambdaAbstraction)eat.applyTo(lunch);	
		LambdaAbstraction luncheatlunch = (LambdaAbstraction)eatlunch.applyTo(lunch);
		GenerationTarget luncheatlunchTarget = new GenerationTarget(luncheatlunch);

		System.out.println("<Bob happily eats lunch> vs <Lunch eats lunch>");
		out = bobhappilyeatslunchTarget.computeInstantiations(luncheatlunch);
		if (out == null || out.size() == 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}
		System.out.println("<Lunch eats lunch> vs <Lunch eats lunch>");
		out = luncheatlunchTarget.computeInstantiations(luncheatlunch);
		if (out != null && out.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		System.out.println("<Lunch eats lunch> vs <lunch>");
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> lunchItems   = luncheatlunchTarget.computeInstantiations(lunch);
		if (lunchItems != null && lunchItems.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}
		System.out.println("<lunch eats lunch> vs <eats lunch>");
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> eatLunchItems2  = luncheatlunchTarget.computeInstantiations(eatlunch);
		if (eatLunchItems2 != null && eatLunchItems2.size() > 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}


		LambdaAbstraction eatBob = (LambdaAbstraction)eat.applyTo(bob);
		LambdaAbstraction luncheatBob = (LambdaAbstraction)eatBob.applyTo(lunch);

		System.out.println("<Lunch eats lunch> vs <lunch eats Bob>");
		out = bobhappilyeatslunchTarget.computeInstantiations(luncheatBob);
		if (out == null || out.size() == 0){
			System.out.println("Success!\n");
		}
		else {
			System.out.println("Failure!\n");
			retval = false;
		}

		System.out.println("---- Part 2: combining ChartGenItems ----- ");
		System.out.println("Debugging application of chart items: <eats lunch> and <lunch> (target for both: <lunch eats lunch>)");
		if (eatLunchItems2 != null && lunchItems != null){
			System.out.println("eatLunch analyses (should be 1): " + eatLunchItems2.size());
			System.out.println("lunch analyses (should be 2): " + lunchItems.size());

			for (semanticsOLD.lambdaFOL.ChartGenItem e: eatLunchItems2){
				for (semanticsOLD.lambdaFOL.ChartGenItem b: lunchItems){
					luncheatlunchTarget.debugApplication(e,b);
				}
			}
		}


		LambdaAbstraction bobTR = bob.typeRaise();
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> bobTRitems = bobhappilyeatslunchTarget.computeInstantiations(bobTR);
		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> eatItems = bobhappilyeatslunchTarget.computeInstantiations(eat);
		System.out.println("Debugging composition of chart items: <bob> (type-raised) and <eat> (target for both: <bob eats lunch>)");
		if (bobTRitems != null && eatItems != null){
			System.out.println("bob analyses (should be 1): " + bobTRitems.size());
			System.out.println("eat analyses (should be 1): " + eatItems.size());

			for (semanticsOLD.lambdaFOL.ChartGenItem b: bobTRitems){
				for (semanticsOLD.lambdaFOL.ChartGenItem e: eatItems){
					bobhappilyeatslunchTarget.debugComposition(b,e);
				}
			}
		}

		return retval;

	}

	public HashSet<semanticsOLD.lambdaFOL.Variable> getAllVariables() {
		return null;
	}


	public static void main(String[] args){
		if (args != null && args.length > 0){

			String lexFile = args[0];
			readLexicon(lexFile);
			
			
			
			/*LambdaAbstraction list  = (LambdaAbstraction)lexicon.getFirstSense("list", "N");
			LambdaAbstraction list2  = (LambdaAbstraction)lexicon.getFirstSense("list2", "N");

			System.out.println("list 2: " + list2);
			LambdaAbstraction list2flat = (LambdaAbstraction) list2.flattenNestedFormulas();
			System.out.println("list 2 flat: " + list2flat);
			LambdaAbstraction list3  = (LambdaAbstraction)lexicon.getFirstSense("list3", "N");
			System.out.println("list 3: " + list3);
			LambdaAbstraction list3flat = (LambdaAbstraction) list3.flattenNestedFormulas();
			System.out.println("list 3 flat: " + list3flat);

			System.out.println("list:        " + list);
			System.out.println("list 2 flat: " + list2flat);
			System.out.println("list 3 flat: " + list3flat);
			System.out.println("These flat lists should all be equivalent: " + list.isEquivalentTo(list2flat)  + " " + list.isEquivalentTo(list3flat) + " " + list2flat.isEquivalentTo(list3flat)  );
			System.exit(0);*/
			boolean testCoverage = testCoverageVectors();// Change DEBUG_COVERAGE in GenerationTarget.computeInstantiations(Expression e) to true to see what this does.
			if (testCoverage){
				System.out.println("SUCCESS! Coverage test succeeded.");
				//System.exit(0);
			}
			else {
				System.out.println("ERROR! Coverage test failed.");
				System.exit(0);
			}

			boolean testComposition = testComposition();
			if (testComposition){
				System.out.println("SUCCESS! Composition test succeeded.");
				//System.exit(0);
			}
			else {
				System.out.println("ERROR! Composition test failed.");
				System.exit(0);
			}

			boolean equivalenceTest = testEquivalence();
			if (equivalenceTest){
				System.out.println("SUCCESS! Equivalence test succeeded.");
				//System.exit(0);
			}
			else {
				System.out.println("ERROR! Equivalence test failed.");
				System.exit(0);
			}

			//testLexicon();
		}
		// TODO: equivalence check: variable renaming is okay, but no flattening of complex formulas, for now. We also assume that both expressions have been fully reduced before equivalence is checked.
		// TODO: enter the entire lexicon (quantifiers and coordination are still missing here, I think)
		// TODO: implement B2
		// TODO: implement B3
		// TODO: implement coordination of atomic categories
		// TODO: implement coordination of unary functions X|Y with X and Y atomic -- can Y be complex? (try to coordinate two type-raised NPs)
		// TODO: implement coordination of binary functions X|Y|Z with X, Y, Z atomic
	}

	public Expression renameVariables(HashSet<semanticsOLD.lambdaFOL.Variable> argVars) {
		Expression e = this;
		for (semanticsOLD.lambdaFOL.Variable v : argVars){
			e = e.renameVar(v);
		}
		return e;
	}

	protected Expression renameVar(semanticsOLD.lambdaFOL.Variable v){
		semanticsOLD.lambdaFOL.Variable v1 = v.getNewVariable();
		return this.renameVar(v, v1);
	}

	protected Expression renameVar(semanticsOLD.lambdaFOL.Variable v, semanticsOLD.lambdaFOL.Variable v1) {
		// TODO Auto-generated method stub
		return this;
	}

	public Expression renameTermVariables(HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> replaceVar) {// NB: Assumption: only term variables are free or quantified, and need to be renamed. Formula variables are always lambda-bound
		if (replaceVar == null || replaceVar.isEmpty())
			return this;
		Expression newE = this;
		//System.out.println("renaming term variables...");
		for (Map.Entry<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> entry : replaceVar.entrySet()){
			//String key = removeVarType(entry.getKey());
			//String val = removeVarType(entry.getValue());
			semanticsOLD.lambdaFOL.Variable oldVar = entry.getKey();
			semanticsOLD.lambdaFOL.Variable newVar = entry.getValue();
			//System.out.println("... " + oldVar + " -> " + newVar);

			//	TermVariable newVar = null;//this.lookupTargetVariable(val);//TODO: this seems buggy -- I want to look up the existing variable
			//	System.out.println("=   " + oldVar + " -> " + newVar);
			newE = newE.renameVar(oldVar, newVar);
		}
		return newE;
	}


	static String removeVarType(String var){
		String v = var.replace("(Q)", "");
		v = v.replace("(F)", "");
		v = v.replace("(B)", "");
		return v;
	}

	public HashMap<String, semanticsOLD.lambdaFOL.Variable> computeVariableMap() {
		HashMap<String, semanticsOLD.lambdaFOL.Variable> varMap = new HashMap<String, semanticsOLD.lambdaFOL.Variable>();
		HashSet<semanticsOLD.lambdaFOL.Variable> allVars = getAllVariables();
		for (semanticsOLD.lambdaFOL.Variable v: allVars){
			String s = v.toString();
			//System.out.println("VarMap Entry: " + s + " -> " + v);
			varMap.put(s, v);
		}
		return varMap;
	}

	public Expression flattenNestedFormulas(){
		return this;
	}

	public Connective connective(){
		return null;
	}

	//	@Override
	//	public int compareTo(Object o) {
	//		// TODO Auto-generated method stub
	//		return 0;
	//	}


}
