package semanticsOLD.lambdaFOL;

public class ExpressionLeaf extends ExpressionTree {
	String string;

	ExpressionLeaf(String s, semanticsOLD.lambdaFOL.ExpressionTreeNode p){
		if (isTermVariable(s)){
			expression = semanticsOLD.lambdaFOL.TermVariable.createTermVariable(s);
			type = Type.TERM_VARIABLE;
		}
		else if (isFormulaVariable(s)){
			expression = semanticsOLD.lambdaFOL.FormulaVariable.createFormulaVariable(s);
			type = Type.FORMULA_VARIABLE;
		}
		else if (isConstant(s)){
			expression = semanticsOLD.lambdaFOL.Constant.createConstant(s);
			type = Type.CONSTANT;
		}
		else if (isLambda(s)){
			type = Type.LAMBDA;
			expression = semanticsOLD.lambdaFOL.Expression.parseToken(string);// TODO: LAMBDA ABSTRACTIONS ARE NOW SINGLE TOKENS  " λ x f ( x ) "
		}
		else if (isQuantifier(s)){
			type = Type.QUANTIFIER;
			expression  = semanticsOLD.lambdaFOL.Expression.parseToken(string);// TODO
		}
		//string = (type == Type.LAMBDA)?"λ"+ expression.toString():expression.toString();	
		parent = p;
		if (parent != null)
			p.addChild(this);
	}
	ExpressionLeaf(Type t, String s, semanticsOLD.lambdaFOL.ExpressionTreeNode p){
		type = t;// this assumes the type matches the expression
		expression = semanticsOLD.lambdaFOL.Expression.parseToken(s);// either a variable or a constant -- or a predicate?? TODO
		string = (expression != null)?expression.toString():s;
		parent = p;
		if (parent != null)
			p.addChild(this);
	}

	public String toString(){
		return new String(string);
	}
	
	public semanticsOLD.lambdaFOL.Variable toVariable() {
		semanticsOLD.lambdaFOL.Variable var = null;
		switch (type){
		case TERM_VARIABLE:
			var = (semanticsOLD.lambdaFOL.TermVariable)expression;
			break;
		case FORMULA_VARIABLE:
			var = (semanticsOLD.lambdaFOL.FormulaVariable)expression;
			break;
		case LAMBDA:		
			var = (semanticsOLD.lambdaFOL.Variable)expression;
			//System.out.println("ExpressionLeaf.toVariable(): lambda " + string + " -> " + var);
			break;
		default:
				System.out.println("ExpressionLeaf.toVariable() ERROR: this is not a variable " + this); 
		}
		return var;
	}
	
	public semanticsOLD.lambdaFOL.TermVariable toTermVariable(){
		semanticsOLD.lambdaFOL.TermVariable var = null;
		if (this.type == Type.TERM_VARIABLE){
			var = (semanticsOLD.lambdaFOL.TermVariable)expression;
		}
		return var;
	}
	public semanticsOLD.lambdaFOL.Formula toFormula(){
		semanticsOLD.lambdaFOL.Formula f = null;
		if (type == Type.FORMULA_VARIABLE){
			f = semanticsOLD.lambdaFOL.FormulaVariable.createFormulaVariable(string);//TODO: debug me!
		}
		return f;
	}
	public semanticsOLD.lambdaFOL.Expression.Connective toConnective(){
		if (type == Type.CONNECTIVE){
			if (string.equals(semanticsOLD.lambdaFOL.Expression.AND)){
				return semanticsOLD.lambdaFOL.Expression.Connective.AND;
			}
			else if (string.equals(semanticsOLD.lambdaFOL.Expression.OR)){
				return semanticsOLD.lambdaFOL.Expression.Connective.OR;
			}
			else if (string.equals(semanticsOLD.lambdaFOL.Expression.IMPLIES)){
				return semanticsOLD.lambdaFOL.Expression.Connective.IMPLIES;
			}
		}
		return null;
	}
	
	
	public semanticsOLD.lambdaFOL.Expression toExpression(){
		semanticsOLD.lambdaFOL.Expression e = null;
		switch (type){
		case CONSTANT:
			e = new semanticsOLD.lambdaFOL.Constant(string);
			break;
		case TERM_VARIABLE:
			e = expression;
			//System.out.println("CHECK " + e + " " + var2);
			break;
		case FORMULA_VARIABLE:
			e = semanticsOLD.lambdaFOL.FormulaVariable.createFormulaVariable(string);
			break;
		case PREDICATE:
			System.out.println("ExpressionLeaf.toExpression() TODO: predicate " + string);
			break;
		case LAMBDA:
			System.out.println("ExpressionLeaf.toExpression() TODO: lambda " + string);
			break;
		case CONNECTIVE:
			System.out.println("ExpressionLeaf.toExpression() TODO: connective " + string);
			break;
		default:
			System.out.println("ExpressionLeaf.toExpression() ERROR: this should not be a leaf " + this); 
		}
		return e;
	}
}
