package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class LambdaApplication extends semanticsOLD.lambdaFOL.Expression {
	protected final semanticsOLD.lambdaFOL.Expression functor;
	protected final semanticsOLD.lambdaFOL.Expression argument;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	public LambdaApplication(semanticsOLD.lambdaFOL.Expression f, semanticsOLD.lambdaFOL.Expression a){
		functor  = f;
		argument = a;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	public semanticsOLD.lambdaFOL.Expression getFunctor(){
		return functor;
	}
	public semanticsOLD.lambdaFOL.Expression getArgument(){
		return argument;
	}
	public String toString(){
		return "(" + functor + " " + argument + ")";
	}
	
	public semanticsOLD.lambdaFOL.Expression replace(semanticsOLD.lambdaFOL.Variable var, semanticsOLD.lambdaFOL.Expression e){
		//System.out.println("Replacing the variable " + var + " with " + e + " in " + this);
		semanticsOLD.lambdaFOL.Expression f2 = functor.replace(var, e);
		//System.out.println(" - Functor: from " + this.functor + " to " + f2);
		semanticsOLD.lambdaFOL.Expression a2 = argument.replace(var, e);
		//System.out.println(" - Argument: from " + this.argument + " to " + a2);
		if (f2 instanceof LambdaAbstraction){
			return ((LambdaAbstraction) f2).applyTo(a2);
		}
		else return new LambdaApplication(f2, a2);
	}
	
	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(semanticsOLD.lambdaFOL.Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat) {
	 if (e != null && e instanceof LambdaApplication){
		 LambdaApplication f = (LambdaApplication)e;
		 if (this.functor.isEquivalentModuloVarRenaming(f.functor, varEquivalenceMapThis, varEquivalenceMapThat)
				 && this.argument.isEquivalentModuloVarRenaming(f.argument, varEquivalenceMapThis, varEquivalenceMapThat)){
			 return true;
		 }
		 else return false;
	 }
	 return false;
	}
	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		 ArrayList<String> yield = new ArrayList<String>();
		 yield.addAll(functor.yield());
		 yield.addAll(argument.yield());
		 return yield;	 
		}
	
	protected ArrayList<TerminalType> initYieldTypes(){
		 ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		 yieldTypes.addAll(functor.yieldTypes());
		 yieldTypes.addAll(argument.yieldTypes());
		 return yieldTypes;	 
		}
	
	public HashSet<semanticsOLD.lambdaFOL.Variable> getAllVariables() {
		HashSet<semanticsOLD.lambdaFOL.Variable> varSet = new HashSet<semanticsOLD.lambdaFOL.Variable>();
		HashSet<semanticsOLD.lambdaFOL.Variable> functorVars = functor.getAllVariables();
		if (functorVars != null){
			varSet.addAll(functorVars);
		}
		HashSet<semanticsOLD.lambdaFOL.Variable> argumentVars = argument.getAllVariables();
		if (argumentVars != null){
			varSet.addAll(argumentVars);
		}
		return varSet;		
	}
	
	protected semanticsOLD.lambdaFOL.Expression renameVar(semanticsOLD.lambdaFOL.Variable v, semanticsOLD.lambdaFOL.Variable v1) {
		semanticsOLD.lambdaFOL.Expression functor1 = functor.renameVar(v, v1);
		semanticsOLD.lambdaFOL.Expression arg1 = argument.renameVar(v,v1);
		return new LambdaApplication(functor1, arg1);
	}
	
	public semanticsOLD.lambdaFOL.Expression flattenNestedFormulas(){
		semanticsOLD.lambdaFOL.Expression functor1 = functor.flattenNestedFormulas();
		semanticsOLD.lambdaFOL.Expression arg1 = argument.flattenNestedFormulas();
		return new LambdaApplication(functor1, arg1);
	}
}
