package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author juliahmr
 *
 */
public class TermVariable extends Term implements semanticsOLD.lambdaFOL.Variable {
	final VarType type;
	final int index;
	final String string;
	final Status status;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	static int ecounter = 0;
	static int xcounter = 0;
	static int tcounter = 0;
	static HashMap<String, TermVariable> stringToVar = new HashMap<String, TermVariable>();

	public static void resetCounters(){
		ecounter = 0;
		xcounter = 0;
		tcounter = 0;
		stringToVar = new HashMap<String, TermVariable>();
	}

	public TermVariable(VarType t){
		status = Status.FREE;
		type = t;
		String statusString = "";//"(F)";
		switch(t){
		case EVENT: 
			index = ++ecounter;
			string = "e" + index + statusString;
			break;
		case ENTITY:
			index = ++xcounter;
			string = "x" + index + statusString;
			break;
		case TIME:
			index = ++tcounter;
			string = "t" + index + statusString;
			break;
		default:
			index = -1;
			string = "v" + index + statusString;
			break;
		}
		yield = initYield();
		yieldTypes = initYieldTypes();
		
	}

	protected static String statusString(Status s){
		String statusString = null;		
		if (s == Status.QUANTIFIED){
			statusString = "(Q)";
		}
		else if (s == Status.LAMBDABOUND){
			statusString = "(B)";
		}
		else if (s == Status.FREE){
			statusString = "(F)";
		}
		return statusString;
	}
	
	public TermVariable(VarType t, Status s){
		status = s;
		type = t;
		String statusString = "";//statusString(status);
		switch(t){
		case EVENT: 
			index = ++ecounter;
			string = "e" + index + statusString;
			break;
		case ENTITY:
			index = ++xcounter;
			string = "x" + index + statusString;
			break;
		case TIME:
			index = ++tcounter;
			string = "t" + index + statusString;
			break;
		default:
			index = -1;
			string = "v" + index + statusString;
			break;
		}	
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public TermVariable(TermVariable var){
		this.index = var.index;
		this.type  = var.type;
		this.string = var.string;
		this.status = var.status;
		yield = var.yield();//TODO: copy
		yieldTypes = var.yieldTypes();//TODO: copy
	}

	public TermVariable(TermVariable var, Status status){
		this.index = var.index;
		this.type  = var.type;
		this.status = status;
		String statusString = "";//statusString(status);
		switch(this.type){
		case EVENT: 
			string = "e" + index + statusString;
			break;
		case ENTITY:
			string = "x" + index + statusString;
			break;
		case TIME:
			string = "t" + index + statusString;
			break;
		default:
			string = "v" + index + statusString;
			break;
		}	
		yield = var.yield();//TODO: copy
		yieldTypes = var.yieldTypes();//TODO: copy
	}
	
	
	public boolean equals(Object o){
		if (o instanceof TermVariable){
			TermVariable v = (TermVariable)o;
			if (v.index == this.index
					&& v.string.equals(v.string)
					&& v.type == this.type
					&& v.status == this.status){ 
				return true;
			}
			else return false;
		}
		else return false;
	}

	public int hashCode(){
		int seed = 31 * string.hashCode();
		switch (type){
		case EVENT:
			seed += 1;
			break;
		case ENTITY:
			seed += 3;
			break;
		case TIME:
			seed += 5;
			break;
		default:
			seed += 7;
			break;	
		}
		seed *= 31;
		seed += index;
		seed *= 31;
		switch (status){
		case FREE:
			seed += 1;
			break;
		case QUANTIFIED:
			seed += 3;
			break;
		case LAMBDABOUND:
			seed +=5;
			break;
		default:
			seed += 7;
			break;
		}
		return seed;
	}



	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(semanticsOLD.lambdaFOL.Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat) {
		//System.out.println("\t TermVariable equivalence check: comparing "  + this + " and " + e + " " + varEquivalenceMapThis.keySet() + " " + varEquivalenceMapThat.keySet());
		if (e != null && e instanceof TermVariable){
			TermVariable v = (TermVariable)e;
			//for (Variable key : varEquivalenceMapThat.keySet()){
			//System.out.println("key " + key + " and variable v " + v + " equal? " + v.equals(key) + " " + key.equals(v) + " contains? " + varEquivalenceMapThat.containsKey(v) + " HashCodes: " + key.hashCode() + " " + v.hashCode() + " " + ((Variable)v).hashCode());
			//}
			//System.out.println("\t ... cast " + e + " to a TermVariable " + v + " " + varEquivalenceMapThat.containsKey(v) + " " + varEquivalenceMapThat.containsKey((Variable)v));
			semanticsOLD.lambdaFOL.Variable x = varEquivalenceMapThis.get((semanticsOLD.lambdaFOL.Variable)this);
			semanticsOLD.lambdaFOL.Variable y = varEquivalenceMapThat.get((semanticsOLD.lambdaFOL.Variable)v);
			//System.out.println("\t\t " + this + " corresponds to " + x + " and " + v + " corresponds to " + y);
			if (x == null && y == null){
				varEquivalenceMapThis.put((semanticsOLD.lambdaFOL.Variable)this, (semanticsOLD.lambdaFOL.Variable)v);
				varEquivalenceMapThat.put((semanticsOLD.lambdaFOL.Variable)v, (semanticsOLD.lambdaFOL.Variable)this);
				//System.out.println("\t\t both are new variables, and have been added to the equivalence maps");
				return true;
			}
			else if (x != null && y != null && ((semanticsOLD.lambdaFOL.Variable)this).equals((semanticsOLD.lambdaFOL.Variable)y) && ((semanticsOLD.lambdaFOL.Variable)v).equals((semanticsOLD.lambdaFOL.Variable)x)){
				return true;
			}
			else return false;
		}
		return false;
	}

	public TermVariable clone(){
		return new TermVariable(this);
	}


	public semanticsOLD.lambdaFOL.Expression replace(semanticsOLD.lambdaFOL.Variable var, semanticsOLD.lambdaFOL.Expression e){
		//System.out.println("CHECK replace variable " + var + " with " + e + " in " + this);
		if (this.equals(var)){
			return e;
		}
		else {
			return this;
		}
	}

	public static void printStringToVar(){
		Set<Entry<String, TermVariable>> entries = stringToVar.entrySet();
		for (Entry<String,TermVariable> e: entries){
			System.out.println("\t " + e.getKey() + " -> " + e.getValue());
		}
	}
	public static TermVariable lookupTermVariable(String string){
	
		return stringToVar.get(string);
		
	}
	public static TermVariable createTermVariable(String string) {
		TermVariable var = stringToVar.get(string);
		//System.out.println("\tDEBUG Term.createTermVariable: Input string: " + string + " var: " + var );
		if (var != null){
			TermVariable copy =  new TermVariable(var);
			//System.out.println("\tCREATING A COPY OF AN EXISTING TERM VARIABLE: " + var + "  -> " + copy + " equals? " + var.equals(copy));
			return copy;
		}
		else {

			if (ExpressionTree.isEntityVariable(string)){
				var = new TermVariable(VarType.ENTITY);
			}
			else if (ExpressionTree.isEventVariable(string)){
				var = new TermVariable(VarType.EVENT);
			}
			else if (ExpressionTree.isTemporalVariable(string)){
				var = new TermVariable(VarType.TIME);
			}
			else var = new TermVariable(VarType.NIL);
			stringToVar.put(string, var);
			//System.out.println("\tDEBUG Term.createTermVariable: created a new variable: " + var + "  var.string: " + var.string + " input string: " + string  );
			return var;
		}
	}

	public static TermVariable createTermVariable(String string, Status status) {
		TermVariable var = stringToVar.get(string);
		if (var != null){
			TermVariable copy =  new TermVariable(var);
			//System.out.println("CREATING A COPY OF AN EXISTING TERM VARIABLE: " + var + "  -> " + copy + " equals? " + var.equals(copy));
			return copy;
		}
		else {
			if (ExpressionTree.isEntityVariable(string)){
				var = new TermVariable(VarType.ENTITY, status);
			}
			else if (ExpressionTree.isEventVariable(string)){
				var = new TermVariable(VarType.EVENT, status);
			}
			else if (ExpressionTree.isTemporalVariable(string)){
				var = new TermVariable(VarType.TIME, status);
			}
			else var = new TermVariable(VarType.NIL, status);
			stringToVar.put(string, var);
			return var;
		}
	}

	public String toString(){
		return string;
	}

	@Override
	public String name(){
		return string;
	}

	@Override
	public VarType type() {
		return type;
	}

	public static void resetStringToVarMap() {
		stringToVar = new HashMap<String,TermVariable>();

	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(this.toString());
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.TERMVARIABLE);
		return yieldTypes;
	}

	public HashSet<semanticsOLD.lambdaFOL.Variable> getAllVariables() {
		HashSet<semanticsOLD.lambdaFOL.Variable> varSet = new HashSet<semanticsOLD.lambdaFOL.Variable>();
		varSet.add(this);
		return varSet;		
	}

	/**
	 * @return a new variable of the same type and status as this variable
	 */
	public semanticsOLD.lambdaFOL.Variable getNewVariable(){
		return new TermVariable(this.type, this.status);
	}

	protected semanticsOLD.lambdaFOL.Expression renameVar(semanticsOLD.lambdaFOL.Variable v, semanticsOLD.lambdaFOL.Variable v1) {
		if (this.equals(v) && v1 instanceof TermVariable)
			return new TermVariable((TermVariable)v1);
		else return this;
	}

	public Status status(){
		return status;
	}

}

