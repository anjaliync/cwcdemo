package semanticsOLD.lambdaFOL;

public interface Variable {
	enum Status {FREE, QUANTIFIED, LAMBDABOUND};
	
	public Status status();
	public String name();
	public Expression.VarType type();
	public Variable getNewVariable();
}
