package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import semanticsOLD.lambdaFOL.Expression.TerminalType;
import semanticsOLD.lambdaFOL.Variable.Status;

/**
 * @author juliahmr
 * The generator should have one (static?) instance of a generation target, consisting of an expression, its terminal yield (and yield shapes) and its length (for efficient creating of coverage vectors)
 * 
 */
public class GenerationTarget {
	protected final semanticsOLD.lambdaFOL.Expression targetExpression;
	protected final ArrayList<String> targetYield;
	protected final ArrayList<TerminalType> targetYieldTypes;
	protected final int targetYieldLength;
	protected final HashMap<String, Integer> multipleOccurrencePredicates;
	protected final HashMap<String, Integer> arityMap;
	protected final HashMap<String, Variable> targetVariableMap;

	public GenerationTarget(semanticsOLD.lambdaFOL.Expression target){
		targetExpression = target;
		targetYield = target.yield();
		targetYieldTypes = target.yieldTypes();//NB: we don't check that yield and yieldTypes have the same length, but rely on the implementation.
		targetYieldLength = targetYield.size();
		multipleOccurrencePredicates = computeMultiplePredicateOccurrencesInTarget();
		arityMap = computePredicateArityMap();
		targetVariableMap = target.computeVariableMap();
	}



	private HashMap<String, Integer> computePredicateArityMap() {
		HashMap<String, Integer> arityMap = new HashMap<String, Integer>();
		for (int i = 0; i < targetYieldLength; i++){
			if (targetYieldTypes.get(i) == semanticsOLD.lambdaFOL.Expression.TerminalType.PREDICATE){
				String pred_i = targetYield.get(i);
				int arity = 0;
				Integer arityInt = arityMap.get(pred_i);
				if (arityInt == null){
					String[] predArity = pred_i.split("N="); // see AtomicFormula#yield(): predicates "pred" of arity n are turned into "pred_N=n" by the yield() function
					arity = new Integer(predArity[1]).intValue();
					arityMap.put(pred_i, arity);
				}
				else {
					arity = arityInt.intValue();
				}

			}
		}
		return arityMap;
	}



	/**
	 * Goes through the target, and checks for predicates that occur multiple times.
	 * @return HashMap of predicates with multiple occurrences, paired with their number of occurrences in the target
	 */
	private HashMap<String, Integer> computeMultiplePredicateOccurrencesInTarget() {
		HashMap<String, Integer> multiplePredicateOccurrences = new HashMap<String, Integer>();
		for (int i = 0; i < targetYieldLength; i++){
			if (targetYieldTypes.get(i) == semanticsOLD.lambdaFOL.Expression.TerminalType.PREDICATE){
				String pred_i = targetYield.get(i);
				Integer nOccurrences = multiplePredicateOccurrences.get(pred_i);
				if (nOccurrences == null){
					multiplePredicateOccurrences.put(pred_i, new Integer(1));
				}
				else {
					multiplePredicateOccurrences.put(pred_i, nOccurrences + 1);
				}
			}
		}
		//TODO: change to Java 8...
		Iterator<Map.Entry<String, Integer>> iterator = multiplePredicateOccurrences.entrySet().iterator() ;
		while(iterator.hasNext()){
			Map.Entry<String, Integer> predOccurrence = iterator.next();
			if (predOccurrence.getValue() == 1)
				iterator.remove();
		}
		if (multiplePredicateOccurrences.isEmpty()){
			return null;
		}
		else return multiplePredicateOccurrences;	
	}


	private HashMap<String, ArrayList<Integer>> getPredicatePositions(ArrayList<String> expressionYield, ArrayList<TerminalType> expressionYieldTypes){
		if (expressionYield == null || expressionYieldTypes == null)
			return null;
		HashMap<String, ArrayList<Integer>> predicatePositions = new HashMap<String, ArrayList<Integer>>();
		for (int i = 0; i < expressionYield.size(); i++){
			if (expressionYieldTypes.get(i) == semanticsOLD.lambdaFOL.Expression.TerminalType.PREDICATE){
				String pred_i = expressionYield.get(i);
				ArrayList<Integer> pos_i = predicatePositions.get(pred_i);
				if (pos_i == null){
					pos_i = new ArrayList<Integer>();
				}
				pos_i.add(i);
				predicatePositions.put(pred_i, pos_i);
			}
		}
		return predicatePositions;
	}

	public semanticsOLD.lambdaFOL.Expression getTargetExpression(){
		return targetExpression;
	}
	public ArrayList<String> getTargetYield(){
		return targetYield;
	}

	public ArrayList<TerminalType> getTargetYieldTypes(){
		return targetYieldTypes;
	}

	/**
	 * @return the length (number of tokens) of the yield
	 */
	public int getTargetYieldLength(){
		return targetYieldLength;
	}

	/**
	 * @param e the expression corresponding to the generated string.
	 * @return true if e is equivalent (modulo variable renaming etc.) to the target expression
	 * @see semanticsOLD.lambdaFOL.Expression#isEquivalentTo(semanticsOLD.lambdaFOL.Expression e)
	 */
	public boolean isEquivalentToTarget(semanticsOLD.lambdaFOL.Expression e){
		return targetExpression.isEquivalentTo(e);
	}

	public boolean hasPredicatesWithMultipleOccurrences(){
		if (multipleOccurrencePredicates != null)
			return true;
		else return false;
	}

	/**
	 * @param e The expression whose coverage against the target we wish to compute
	 * @return an ArrayList of boolean vectors for each possible coverage
	 */
	public ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> computeInstantiations(semanticsOLD.lambdaFOL.Expression e) {


		ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> chartItems  = null;
		e = e.replaceMyVars(this.targetExpression);

		boolean DEBUG_COVERAGE = false;
		/*if (DEBUG_COVERAGE){
			System.out.println("\n====================================================================================");
			System.out.println("ENTERING COMPUTE INSTANTIATIONS");
			System.out.println("  target:     " + this.targetExpression);
			System.out.println("  expression: " + e);
			System.out.println("====================================================================================");
		}*/
		ArrayList<String> eYield = e.yield();
		ArrayList<TerminalType> eYieldTypes = e.yieldTypes();
		int eYieldLength = eYield.size();

		boolean foundAllMatches = true;

		// 1) Does the target contain all the predicates, constants, quantifiers and connectives that the expression e contains? 
		// (if not, e contains additional information, which shouldn't be the case, so we return null)
		boolean targetContainsE = checkContainmentOfE(e);
		if (!targetContainsE){
			//if (DEBUG_COVERAGE) System.out.println("GenerationTarget.computeCoverage fails because the target does not contain the expression e: "
			//		+ targetExpression + " != " + e);
			foundAllMatches = false;
		}

		if (foundAllMatches){
			HashMap<String, ArrayList<Integer>> predicatesTarget = getPredicatePositions(targetYield, targetYieldTypes);
			HashMap<String, ArrayList<Integer>> predicatesExpression = getPredicatePositions(eYield, eYieldTypes);
			Stack<Integer> hasMatch = new Stack<Integer>();
			boolean[][] matches = new boolean[eYieldLength][targetYieldLength];

			for (String pred : predicatesExpression.keySet()){
				ArrayList<Integer> predPositionsE = predicatesExpression.get(pred);
				ArrayList<Integer> predPositionsT = predicatesTarget.get(pred);
				int arity = arityMap.get(pred);
				//System.out.println("arity of predicate " + pred + " " + arity);
				for (int i : predPositionsE){
					boolean foundMatch = false;
					for (int j : predPositionsT){
						boolean allConstantArgumentsAgree = true;// is false if there are constant arguments that don't match -- or if the expression contains a constant argument that is not contained in the target
						for (int l = 1; l <= arity; l++){
							int il = i + l;
							int jl = j + l;
							TerminalType eType_l = eYieldTypes.get(il);
							TerminalType targetType_l = targetYieldTypes.get(jl);
							if (eType_l == semanticsOLD.lambdaFOL.Expression.TerminalType.CONSTANT
									&& targetType_l == semanticsOLD.lambdaFOL.Expression.TerminalType.CONSTANT){
								String e_il = eYield.get(il);
								String target_jl = targetYield.get(jl);
								if  (!e_il.equals(target_jl)){
									if (DEBUG_COVERAGE) System.out.println("GenerationTarget.computeInstantiations: Mismatch between constant arguments of predicate " + pred + ": target argument " + target_jl  + " != " + e_il + "\n" + "Hence " + targetExpression + " != " + e);
									allConstantArgumentsAgree = false;
								}}
							else if (targetType_l != semanticsOLD.lambdaFOL.Expression.TerminalType.CONSTANT
									&& eType_l == semanticsOLD.lambdaFOL.Expression.TerminalType.CONSTANT){
								String e_il = eYield.get(il);
								String target_jl = targetYield.get(jl);
								if (DEBUG_COVERAGE) System.out.println("GenerationTarget.computeInstantiations: Mismatch between arguments of predicate " + pred + ": target argument is not a constant " + target_jl  + ", but e's argument is: " + e_il + "\n" + "Hence " + targetExpression + " != " + e);
								allConstantArgumentsAgree = false;
							}
						}
						if (allConstantArgumentsAgree){
							matches[i][j] = true;
							foundMatch = true;
						}			
					}
					if (!foundMatch){// there is a predicate in e that cannot be matched to
						foundAllMatches = false;
						break;
					}
					else {hasMatch.add(i);}
				}
			}
			if (foundAllMatches){
				ArrayList<boolean[][]> coverageMatrices = getAllMatches(hasMatch, matches);//TODO: no, this has to be boolean[][] arrays (where only one element per row/column can be set to true), so that we can then compute coverage vectors from them and instantiate the variables accordingly
				chartItems = new ArrayList<semanticsOLD.lambdaFOL.ChartGenItem>();
				for (boolean[][] c: coverageMatrices){
					semanticsOLD.lambdaFOL.ChartGenItem newItem = getChartGenItem(c, e, eYield, eYieldTypes);
					if (newItem != null){
						chartItems.add(newItem);
					}
				}
			}
		}
		if (DEBUG_COVERAGE){
			System.out.println("\n====================================================================================");
			System.out.println("EXITING COMPUTE INSTANTIATIONS");
			System.out.println("  target:     " + this.targetExpression);
			System.out.println("  expression: " + e);
			if (chartItems == null || chartItems.isEmpty()){
				System.out.println("  -> failure: expression can't instantiate parts of the target");
			}
			else {
				for (semanticsOLD.lambdaFOL.ChartGenItem item: chartItems){
					System.out.println("             " + item);
				}
			}
			System.out.println("====================================================================================\n");

		}	

		return chartItems;
	}



	/**
	 * @param eYieldTypes 
	 * @param c: a matrix of size expression yield x target yield, c[i][j] is true if the predicate at position i in eYield instantiates the predicate at position j in the targetYield
	 * @param e: the (partial) expression that is added to the chart
	 * @param eYield: the yield of the expression
	 * @return a ChartGenItem that consists of a coverage vector and the Expression e, assuming the variables in e can be mapped to the variables in the target under the mapping defined in c
	 */
	private semanticsOLD.lambdaFOL.ChartGenItem getChartGenItem(boolean[][] c, semanticsOLD.lambdaFOL.Expression e, ArrayList<String> eYield, ArrayList<TerminalType> eYieldTypes) {
		boolean[] coverageVector = new boolean[targetYieldLength];
		HashMap<String,String> t_to_e_varMap = new HashMap<String,String>();
		HashMap<String,String> e_to_t_varMap = new HashMap<String,String>();
		//System.out.println("Entering getChartGenItem");
		//System.out.println("t: " + this.targetExpression);
		//System.out.println("e: " + e + "\n");
		for (int i = 0; i < c.length; i++){
			for (int j = 0; j < c[i].length; j++){
				if (c[i][j]){
					if (coverageVector[j])
						return null; // assumption: for each j, there is only one i with c[i][j] true.
					coverageVector[j] = true;
					String pred = targetYield.get(j);
					int arity = arityMap.get(pred);
					//System.out.println("Matching predicate " + pred + " with arity " + arity);
					for (int k = 1; k <= arity; k++){
						int jk = j+k;
						int ik = i+k;
						String t_k = targetYield.get(jk);
						String e_k = eYield.get(ik);
						TerminalType t_type = targetYieldTypes.get(jk);
						TerminalType e_type = eYieldTypes.get(ik);
						if (t_type == e_type && (t_type == TerminalType.TERMVARIABLE || t_type == TerminalType.FORMULAVARIABLE)){
							//System.out.println("-- matching argument " + k + " target: " + t_k + " -- " + e_k  + " " + t_type + " " + e_type);
							String t_k_match = t_to_e_varMap.get(t_k);
							String e_k_match = e_to_t_varMap.get(e_k);
							if ((t_k_match == null || t_k_match.equals(e_k)) && (e_k_match == null || e_k_match.equals(t_k))){
								t_to_e_varMap.put(t_k, e_k);
								e_to_t_varMap.put(e_k, t_k);
								coverageVector[jk] = true;
							}
							else return null;// the variable arguments of the matched predicate don't actually match
						}
						if (t_type == e_type && t_type == TerminalType.CONSTANT && t_k.equals(e_k)){
							coverageVector[jk] = true;
						}
					}
				}
			}
		}
		HashMap<String,Variable> eVarMap = e.computeVariableMap();
		HashMap<String,Variable> tVarMap = targetExpression.computeVariableMap();
		HashMap<Variable,Variable> replaceVar = new HashMap<Variable,Variable>();//replace variables in e (key) with variables from t (value)
		for (Map.Entry<String, String> entry : t_to_e_varMap.entrySet()){// replace each variable (name) in e with an actual variable from t
			String tVarString = entry.getKey();
			Variable tVar = tVarMap.get(tVarString);
			Status status = tVar.status();
			if (status == Status.FREE || status == Status.QUANTIFIED){ // we only instantiate free or quantified variables...
				//Variable tVar = tVarMap.get(tVarString);
				String eVarString = entry.getValue();
				if (!tVarString.equals(eVarString)){ 
					Variable eVar = eVarMap.get(eVarString);
					if (tVar.status() == eVar.status()){ // .. and we only replace free variables with free variables, and quantified variables with quantified variables
						// .. although the coverage vector will still match all the arguments if they are otherwise compatible
						//System.out.println("Target variable " + tVar + " should replace " + eVar );
						//System.out.println("\t target:     " + targetExpression);
						//System.out.println("\t expression: " + e);
						replaceVar.put(eVar,tVar);
					}
				}
			}
		}
		semanticsOLD.lambdaFOL.Expression eNew = e.renameTermVariables(replaceVar);
		//System.out.println("New expression: " + eNew + "\n");
		return new semanticsOLD.lambdaFOL.ChartGenItem(this, coverageVector, eNew);
	}


	private ArrayList<boolean[][]> getAllMatches(Stack<Integer> hasMatch, boolean[][] matches) {
		//LinkedHashSet<Integer> tMatches = new LinkedHashSet<Integer>();
		boolean[] tMatches = new boolean[targetYieldLength];
		boolean[][] currentMatch = new boolean[matches.length][targetYieldLength];
		return getAllMatchesRec(hasMatch, matches, tMatches, currentMatch);
	}

	private ArrayList<boolean[][]> getAllMatchesRec(Stack<Integer> hasMatch, boolean[][] matches,
			boolean[] tMatches, boolean[][] currentMatch) {
		// hasMatch: the items in e that remain to be matched
		// matches[i][j] is true if item i in e is matched to item j in t;
		ArrayList<boolean[][]> results = new ArrayList<boolean[][]>();
		if (hasMatch.isEmpty()){
			results.add(currentMatch);// success, we've matched everything, so we can return a list with a single match
			return results;
		}
		else {
			Integer firstItem = hasMatch.pop();// pop the first element off of the stack.
			for (int j = 0; j < matches[firstItem].length; j++){

				if (matches[firstItem][j] && !tMatches[j]){ // for any match that is still available, recurse further down the stack
					//System.out.println("Looking to match item " + firstItem + " with item " + j); 
					boolean[] tMatches_ij = Arrays.copyOf(tMatches, tMatches.length);
					tMatches_ij[j] = true;
					boolean[][] newMatch = new boolean[currentMatch.length][targetYieldLength];
					for (int k = 0; k < currentMatch.length; k++){
						newMatch[k] = Arrays.copyOf(currentMatch[k], currentMatch[k].length);
					}
					newMatch[firstItem][j] = true;
					//System.out.println("\tcurrentMatch: " + Arrays.toString(currentMatch[firstItem]));
					//System.out.println("\tnewMatch:     " + Arrays.toString(newMatch[firstItem]));
					@SuppressWarnings("unchecked")
					Stack<Integer> clone = (Stack<Integer>)hasMatch.clone();
					ArrayList<boolean[][]> getMatches_ij = getAllMatchesRec(clone, matches, tMatches_ij, newMatch);
					results.addAll(getMatches_ij);
				}
			}
			return results;
		}
	}



	/**
	 * This is a very minimal containment check.
	 * @param e
	 * @return true iff all occurrences of predicates, constants, connectives, quantifiers and negation symbolsin e occur in the target as well (regardless of their order, scope, etc.)
	 */
	private boolean checkContainmentOfE(semanticsOLD.lambdaFOL.Expression e) {
		boolean retval = true;
		ArrayList<String> eYield = e.yield();
		ArrayList<TerminalType> eYieldTypes = e.yieldTypes();
		int eYieldLength = eYield.size();
		boolean[] covered = new boolean[targetYieldLength];

		for (int i = 0; i < eYieldLength; i++){
			TerminalType eType_i = eYieldTypes.get(i);
			if (eType_i == semanticsOLD.lambdaFOL.Expression.TerminalType.CONNECTIVE || eType_i == semanticsOLD.lambdaFOL.Expression.TerminalType.CONSTANT || eType_i == semanticsOLD.lambdaFOL.Expression.TerminalType.QUANTIFIER || eType_i == semanticsOLD.lambdaFOL.Expression.TerminalType.PREDICATE || eType_i == semanticsOLD.lambdaFOL.Expression.TerminalType.NEGATION){
				boolean is_covered_i = false;
				String eYield_i = eYield.get(i);
				for (int j = 0; j < targetYieldLength; j++){
					if (!covered[j] 
							&& eType_i == targetYieldTypes.get(j) 
							&& eYield_i.equals(targetYield.get(j))){
						covered[j] = true;
						is_covered_i = true;
						break;
					}
				}
				if (!is_covered_i){
					retval = false;
					return retval;
				}
			}
		}

		return retval;
	}

	public boolean debugTypeRaising(semanticsOLD.lambdaFOL.ChartGenItem child){
		System.out.println("Entering debug application");
		System.out.println("Target: " + this.targetExpression);
		boolean retval = false;
		semanticsOLD.lambdaFOL.LambdaAbstraction parent = child.expression.typeRaise();
		semanticsOLD.lambdaFOL.ChartGenItem parentItem = new semanticsOLD.lambdaFOL.ChartGenItem(this, child.coverage.clone(), parent);
		return retval;
	}
	public boolean debugApplication(semanticsOLD.lambdaFOL.ChartGenItem functorChild, semanticsOLD.lambdaFOL.ChartGenItem argChild){
		System.out.println("Entering debug application");
		System.out.println("Target: " + this.targetExpression);
		int matches = 0;
		boolean retval = false;
		boolean[] jointCoverage = semanticsOLD.lambdaFOL.ChartGenItem.jointCoverage(functorChild,argChild);
		if (jointCoverage != null && functorChild.expression instanceof semanticsOLD.lambdaFOL.LambdaAbstraction){
			semanticsOLD.lambdaFOL.Expression result = ((semanticsOLD.lambdaFOL.LambdaAbstraction)functorChild.expression).applyTo(argChild.expression);
			ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> resItems = computeInstantiations(result);// in an actual application, this is what would have to be returned
			for (semanticsOLD.lambdaFOL.ChartGenItem res : resItems){
				if (res.coverageEquals(jointCoverage)){
					System.out.println("Application success: ");
					System.out.println("Functor: " + functorChild);
					System.out.println("Argument:" + argChild);
					System.out.println("Result:  " + res);		
					matches++;
					retval = true;
				}
			}
		}
		if (matches == 0){
			System.out.println("failure: these two children cannot combine");
		}
		return retval;	

	}

	public boolean debugComposition(semanticsOLD.lambdaFOL.ChartGenItem functorChild, semanticsOLD.lambdaFOL.ChartGenItem argChild){
		System.out.println("Entering debug composition");
		System.out.println("Target: " + this.targetExpression);
		System.out.println("Functor 1: " + functorChild.expression);
		System.out.println("Functor 2: " + argChild.expression);
		int matches = 0;
		boolean retval = false;
		boolean[] jointCoverage = semanticsOLD.lambdaFOL.ChartGenItem.jointCoverage(functorChild,argChild);
		if (jointCoverage != null && functorChild.expression instanceof semanticsOLD.lambdaFOL.LambdaAbstraction && argChild.expression instanceof semanticsOLD.lambdaFOL.LambdaAbstraction){
			semanticsOLD.lambdaFOL.Expression result = ((semanticsOLD.lambdaFOL.LambdaAbstraction)functorChild.expression).compose_b1((semanticsOLD.lambdaFOL.LambdaAbstraction)argChild.expression);
			System.out.println("Result of composition: " + result);
			ArrayList<semanticsOLD.lambdaFOL.ChartGenItem> resItems = computeInstantiations(result);
			for (semanticsOLD.lambdaFOL.ChartGenItem res : resItems){
				if (res.coverageEquals(jointCoverage)){
					System.out.println("Composition success: ");
					System.out.println("Functor: " + functorChild);
					System.out.println("Argument:" + argChild);
					System.out.println("Result:  " + res);		
					matches++;
					retval = true;
				}
			}
		}
		if (matches == 0){
			System.out.println("failure: these two children cannot combine");
		}
		return retval;	

	}
}