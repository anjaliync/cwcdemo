/**
 * 
 */
package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author juliahmr
 * TODO: This needs to be adjusted to include syntactic CCG categories as well... 
 */
public class ChartGenItem {
	
	protected final Expression expression;
	protected final boolean[] coverage;
	// TODO: add CCGcat, add yield, etc.
	

	public ChartGenItem(GenerationTarget target, boolean[] c, Expression e){
		expression = e;
		coverage = c;		
	}
	
	public Expression expression() { return expression; }
	public boolean[]  coverage()   { return coverage;   }
	
	/**
	 * @param leftChild
	 * @param rightChild
	 * @return a list of all possible ChartGenItems that result from the combination of the two items, or of null if the two children either should not combine (because their coverage overlaps), or because they cannot combine. 
	 */
	public static ArrayList<ChartGenItem> combineTwoItems(ChartGenItem leftChild, ChartGenItem rightChild){
		boolean[] jointCoverage = jointCoverage(leftChild,rightChild);// could be made more efficient by having this function return null
		if (jointCoverage != null){
			ArrayList<ChartGenItem> resultItems = new ArrayList<ChartGenItem>();
			// TODO: all possible combinations of these two items whose coverage vector is equal to jointCoverage
			// TODO: is this really a list, or should we just return a single item? 
			return resultItems;
		}
		return null;	
		
	}
	
	
	
	/**
	 * @param leftChild
	 * @param rightChild
	 * @return null if one of the coverage vectors is null, or if their lengths are not the same, or if there is a bit that both cover already. Otherwise, return a bit vector that the element-wise xor of both coverage vectors
	 */
	public static boolean[] jointCoverage(ChartGenItem leftChild, ChartGenItem rightChild){
		if (leftChild.coverage != null && rightChild.coverage != null
				&& leftChild.coverage.length == rightChild.coverage.length){
			boolean[] jointCoverage = new boolean[leftChild.coverage.length];
			for (int i = 0; i < leftChild.coverage.length; i++){
				boolean l = leftChild.coverage[i];
				boolean r = rightChild.coverage[i];
				// TT -> fail (both cover this bit)
				if (l && r){
					return null;
				}
				// TF -> T (left covers this bit)
				// FT -> T (right covers this bit)
				else if (l || r){
					jointCoverage[i] = true;
				}
				// FF -> F (neither covers this bit)
				else {
					jointCoverage[i] = false;// already set to false by initialization.
				}		
			}
			return jointCoverage;
		}
		else return null;
	}
	
	
	/** Not really necessary, subsumed by jointCoverage (which returns null if the coverage is not disjoint)
	 * @param leftChild
	 * @param rightChild
	 * @return true if both coverage vectors have the same length and there is no bit where both are true
	 */
	public static boolean isCoverageDisjoint(ChartGenItem leftChild, ChartGenItem rightChild){
		if (leftChild.coverage != null && rightChild.coverage != null
				&& leftChild.coverage.length == rightChild.coverage.length){
			for (int i = 0; i < leftChild.coverage.length; i++){
				boolean l = leftChild.coverage[i];
				boolean r = rightChild.coverage[i];
				if (l == true && r == true){
						return false;
				}
				// TF -> T (left covers this bit)
				// FT -> T (right covers this bit)
				// FF -> T (neither covers this bit)
			}
		}
		return true;
	}
	
	public String toString(){
		return new String("<" + expression + "\t " + Arrays.toString(coverage) + ">");
	}
	public int hashCode(){
		int hash = 1;
		if (expression != null) {
			hash += 17 * expression.hashCode();
		}
		if (coverage != null){
			hash += 31 * coverage.hashCode();
		}
		return hash;
	}

	public boolean equals(Object o){
		if (o instanceof ChartGenItem){
			ChartGenItem c = (ChartGenItem)o;
//			return (this.coverageEquals(c.coverage) && this.expression.equals(c.expression));
			return (this.coverageEquals(c.coverage) && this.expression.isEquivalentTo(c.expression));
		}
		else return false;
	}
	public boolean coverageEquals(boolean[] c) {
		return Arrays.equals(this.coverage, c);
	}
}
