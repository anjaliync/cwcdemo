package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import semanticsOLD.generation.Configuration;

public class SemLexicon {

	protected HashMap<String, HashMap<String, ArrayList<Expression>>> lexicon;

	SemLexicon(){
		lexicon = new HashMap<String, HashMap<String, ArrayList<Expression>>>();
	}

	public void addEntry(String word, String cat, String sem){
		//System.out.println("adding category " + cat + " with word \""+ word +  "\" and sense " + sem);
		boolean debug = Configuration.DEBUG;
		Expression e = Expression.parseString(sem);
		if (debug) System.out.println("Adding lexicon entry <" + word + ", " + cat + ", " + e + ">");
		if (e != null){
			if (!lexicon.containsKey(word)){
				lexicon.put(word, new HashMap<String, ArrayList<Expression>>());
			}
			HashMap<String, ArrayList<Expression>> wordEntries = lexicon.get(word);
			if (!wordEntries.containsKey(cat)){
				wordEntries.put(cat, new ArrayList<Expression>());
			}
			ArrayList<Expression> senses = wordEntries.get(cat);
			senses.add(e);
		}
	}

	public ArrayList<Expression> getEntry(String word, String cat){
		if (lexicon.containsKey(word)){
			HashMap<String, ArrayList<Expression>> wordEntries  = lexicon.get(word);
			if (!wordEntries.containsKey(cat)){
				System.out.println("ERROR: lexicon does not contain word " + word + " with category " + cat + " " + cat.hashCode()); 
			}
			return wordEntries.get(cat);
		}
		else {System.out.println("ERROR: lexicon does not contain word " + word + " " + word.hashCode()); 
			Set<String> allWords = lexicon.keySet();
			for (String w : allWords){
				System.out.println(" lexicon contains word " + w  + " " + w.hashCode());
			}
		}
		return null;
	}

	public Expression getFirstSense(String word, String cat) {
		ArrayList<Expression> allSenses = getEntry(word, cat);
		if (allSenses != null)
			return allSenses.get(0);
		return null;
	}
	public HashMap<String, ArrayList<Expression>> getAllEntries(String word) {
		return lexicon.get(word);
	}
	
	public HashMap<String, Expression> getAllFirstEntries(String word) {
		HashMap<String, ArrayList<Expression>> allEntries = getAllEntries(word);
		if (allEntries != null){
			HashMap<String, Expression> firstEntries = new HashMap<String, Expression>();
			for (String c: allEntries.keySet()){
				firstEntries.put(c, getFirstSense(word,c));
			}
			return firstEntries;
		}
		return null;
	}
	
	/**
	 * Added by Anjali for use in lexical lookup.
	 * @return the lexicon HashMap
	 */
	public Set<String> getWordsInLexicon() {
		return lexicon.keySet();
	}
}
