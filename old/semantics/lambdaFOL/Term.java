package semanticsOLD.lambdaFOL;

import java.util.HashMap;

/* 
 * Terms refer to entities.
 * We assume different sorts of entities: actual entities, times, events (plural entities?, time intervals?) 
 */
public class Term extends Expression {

	public String name(){
		return toString();
	}
	
	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat) {
	 return false;
	}
}
