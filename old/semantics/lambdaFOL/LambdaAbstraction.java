package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import semanticsOLD.lambdaFOL.Variable.Status;

public class LambdaAbstraction extends semanticsOLD.lambdaFOL.Expression {
	protected final Variable var;
	protected final semanticsOLD.lambdaFOL.Expression body;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;

	public LambdaAbstraction(Variable v, semanticsOLD.lambdaFOL.Expression b){
		var = v;
		body = b;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public String toString(){		
		if (var == null){
			System.out.println("LambdaApplication.toString() error: var is null");
		}
		if (body == null){
			System.out.println("LambdaApplication.toString() error: body is null");
		}
		String b = (body instanceof ComplexFormula || body instanceof QuantifiedFormula)?"(" + body.toString() + ")":body.toString();
		return LAMBDA + var.name() + "." + b;
	}


	public LambdaAbstraction replace(Variable v, semanticsOLD.lambdaFOL.Expression e){
		if (this.var.equals(v))
			return this;
		else {
			semanticsOLD.lambdaFOL.Expression b2 = body.replace(v, e);
			return new LambdaAbstraction(this.var, b2);
		}
	}

	public semanticsOLD.lambdaFOL.Expression applyTo(semanticsOLD.lambdaFOL.Expression argument) {
		semanticsOLD.lambdaFOL.Expression argumentNew = argument.replaceMyVars(body);
		semanticsOLD.lambdaFOL.Expression bodyNew = body.replace(var,  argumentNew);
		return bodyNew;
	}



	public LambdaAbstraction compose_b1(LambdaAbstraction gy){
		LambdaAbstraction gyNew = (LambdaAbstraction)gy.replaceMyVars(body);
		VarType type = gyNew.var.type();
		semanticsOLD.lambdaFOL.Expression z = createNewVariable(type, Status.LAMBDABOUND);
		semanticsOLD.lambdaFOL.Expression gz = gyNew.applyTo(z);
		semanticsOLD.lambdaFOL.Expression fg = this.applyTo(gz);
		LambdaAbstraction fgz = new LambdaAbstraction((Variable)z, fg);
		//System.out.println("Composed " + this + " with " + gy + " -> " + fgz);
		return fgz;
	}

	public LambdaAbstraction compose_b2(LambdaAbstraction gy1y2){// ^P.(f P) * ^y2.^y1.g(y1,y2) = ^z2.^z1.(f (g (z1 z2)
		if (gy1y2 != null && gy1y2.body instanceof LambdaAbstraction){
			//System.out.println("COMPOSE B2");
			//System.out.println("\t f: " + this);

			//1. replace all the common variables.
			LambdaAbstraction gy1y2New = (LambdaAbstraction)gy1y2.replaceMyVars(body);
			//System.out.println("\t g: " + gy1y2New);
			// 2. get the in
			LambdaAbstraction gy1New = (LambdaAbstraction)gy1y2New.body;
			VarType type1 = gy1New.var.type();
			semanticsOLD.lambdaFOL.Expression z1 = createNewVariable(type1, Status.LAMBDABOUND);

			VarType type2 = gy1y2New.var.type();
			semanticsOLD.lambdaFOL.Expression z2 = createNewVariable(type2, Status.LAMBDABOUND);
			LambdaAbstraction gz2 = (LambdaAbstraction)gy1y2New.applyTo(z2);
			//System.out.println("\t... first step: gy1y2 applied to z2: " + gz2 + " " + gz2.getClass());
			semanticsOLD.lambdaFOL.Expression gz1z2 = gz2.applyTo(z1);
			//System.out.println("\t... second step: gz2 applied to z1: " + gz1z2 + " " + gz1z2.getClass());
			semanticsOLD.lambdaFOL.Expression fg = this.applyTo(gz1z2);
			//System.out.println("\t... third step: f applied to gz1z2: " + fg + " " + fg.getClass());
			LambdaAbstraction z1_fg = new LambdaAbstraction ((Variable)z1,fg);
			//System.out.println("\t... fourth step: abstract over z1: " + z1_fg);
			LambdaAbstraction z2_z1_fg = new LambdaAbstraction ((Variable)z2,z1_fg);
			//System.out.println("Composed " + this + " with " + gy1y2 + " -> " + z2_z1_fg);
			return z2_z1_fg;
		}
		else return null;
	}

	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(semanticsOLD.lambdaFOL.Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat) {
		if (e != null && e instanceof LambdaAbstraction){
			LambdaAbstraction f = (LambdaAbstraction)e;
			Variable x = varEquivalenceMapThis.get(this.var);
			Variable y = varEquivalenceMapThat.get(f.var);
			if (x == null && y == null){// this means that neither this.var nor f.var have appeared in the parts of this expression and e that we have looked at so far.
				varEquivalenceMapThis.put(this.var, f.var);
				varEquivalenceMapThat.put(f.var, this.var);
				return this.body.isEquivalentModuloVarRenaming(f.body, varEquivalenceMapThis, varEquivalenceMapThat);
			}
			// TODO: I'm not sure we want this clause (see also QuantifiedFormula)
			else if (x != null && y != null && this.equals(y) && f.var.equals(y)){
				return this.body.isEquivalentModuloVarRenaming(f.body, varEquivalenceMapThis, varEquivalenceMapThat);
			}
			else return false;

		}
		return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(LAMBDA);
		yield.add(var.toString());
		yield.addAll(body.yield());
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.LAMBDA);
		VarType t = var.type();
		switch (t){
		case EVENT:
		case ENTITY: 
		case TIME:
			yieldTypes.add(TerminalType.TERMVARIABLE);
			break;
		case FORMULA:
			yieldTypes.add(TerminalType.FORMULAVARIABLE);
			break;	 
		case NIL:
			yieldTypes.add(null);
			break;	
		}
		yieldTypes.addAll(body.yieldTypes());
		return yieldTypes;
	}

	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();
		varSet.add(var);
		HashSet<Variable> bodyVars = body.getAllVariables();
		if (bodyVars != null){
			varSet.addAll(bodyVars);
		}
		return varSet;		
	}

	protected semanticsOLD.lambdaFOL.Expression renameVar(Variable v, Variable v1) {
		Variable v2 = (this.var.equals(v))?v1:this.var;
		semanticsOLD.lambdaFOL.Expression bodyNew = body.renameVar(v, v1);
		return new LambdaAbstraction(v2, bodyNew);
	}

	public semanticsOLD.lambdaFOL.Expression flattenNestedFormulas(){
		semanticsOLD.lambdaFOL.Expression bodyFlat = body.flattenNestedFormulas();
		return new LambdaAbstraction(var, bodyFlat);
	}

}
