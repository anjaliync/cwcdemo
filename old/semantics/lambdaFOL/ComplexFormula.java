package semanticsOLD.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ComplexFormula extends Formula {
	final protected Connective con;
	protected semanticsOLD.lambdaFOL.Expression formula1;// a formula or a lambda application that will eventually yield a formula...
	protected semanticsOLD.lambdaFOL.Expression formula2;// a formula or a lambda application that will eventually yield a formula...
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	public ComplexFormula(semanticsOLD.lambdaFOL.Expression f1, Connective c, semanticsOLD.lambdaFOL.Expression f2){
		formula1 = f1;
		con = c;
		formula2 = f2;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public Connective connective(){
		return con;
	}
	public String toString(){
		String connective = "";
		switch (con){
		case AND: connective = " ∧ "; break;
		case OR: connective = " ∨ "; break;
		case IMPLIES: connective = " ⟶ "; break;
		}
		String f1 = (formula1 instanceof semanticsOLD.lambdaFOL.AtomicFormula || formula1 instanceof LambdaApplication)? formula1.toString(): new String("(" + formula1 + ")");
		String f2 = (formula2 instanceof semanticsOLD.lambdaFOL.AtomicFormula || formula2 instanceof LambdaApplication)? formula2.toString(): new String("(" + formula2 + ")");
		return new String(f1 +  connective + f2);
	}


	public ComplexFormula replace(semanticsOLD.lambdaFOL.Variable var, semanticsOLD.lambdaFOL.Expression e){
		semanticsOLD.lambdaFOL.Expression f1 = formula1.replace(var, e);
		semanticsOLD.lambdaFOL.Expression f2 = formula2.replace(var, e);
		return new ComplexFormula(f1, this.con, f2);
	}

	public boolean equals(Object o){// strict equality, not equivalence
		if (o != null && o instanceof ComplexFormula){
			ComplexFormula f = (ComplexFormula)o;
			if (this.con.equals(f.con) && this.formula1.equals(f.formula1) && this.formula2.equals(f.formula2)){
				return true;
			}
		}
		return false;
	}

	public int hashCode(){
		int seed = 31;
		switch (con){
		case AND: seed *=1; break;
		case OR: seed *= 3; break;
		case IMPLIES: seed *= 5; break;
		}
		seed += formula1.hashCode();
		seed *= 31;
		seed += formula2.hashCode();
		return seed;
	}

	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(semanticsOLD.lambdaFOL.Expression e, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThis, HashMap<semanticsOLD.lambdaFOL.Variable, semanticsOLD.lambdaFOL.Variable> varEquivalenceMapThat){
		if (e != null && e instanceof ComplexFormula){
			ComplexFormula f = (ComplexFormula)e;

			//System.out.println("COMPLEX FORMULA EQUIVALENCE CHECK: " + con + " " + f.con + " " + this.formula1  + " " + f.formula1 + " " + this.formula2 + " " + f.formula2);
			//System.out.println("This map: " + varEquivalenceMapThis.keySet());
			//for (Variable v : varEquivalenceMapThis.keySet()){
			//	System.out.println(v + " -> " + varEquivalenceMapThis.get(v));
			//}
			//System.out.println("That map: " + varEquivalenceMapThat.keySet());
			//for (Variable v : varEquivalenceMapThat.keySet()){
			//	System.out.println(v + " -> " + varEquivalenceMapThat.get(v));
			//}
			//					
			//			//System.out.println("Equality checks: " + this.con.equals(f.con) + " " + this.con.equals(Connective.AND)  + " " + f.con.equals(Connective.AND));
			if (this.con.equals(f.con) && this.formula1.isEquivalentModuloVarRenaming(f.formula1,varEquivalenceMapThis,varEquivalenceMapThat) && this.formula2.isEquivalentModuloVarRenaming(f.formula2,varEquivalenceMapThis,varEquivalenceMapThat)){
				//System.out.println("True -- case 1");
				return true;
			}


			if (this.con.equals(Connective.AND) && f.con.equals(Connective.AND) && this.formula1.isEquivalentModuloVarRenaming(f.formula2,varEquivalenceMapThis,varEquivalenceMapThat) && this.formula2.isEquivalentModuloVarRenaming(f.formula1,varEquivalenceMapThis,varEquivalenceMapThat)){
				//System.out.println("True -- case 2");
				return true;
			}


			// TODO: this is not a full equivalence check, just a very basic (not comprehensive) check for commutativity: p v q == q v p (this breaks down if p or q are complex formulas themselves)
			if (this.con.equals(Connective.OR) && f.con.equals(Connective.OR) && this.formula1.isEquivalentModuloVarRenaming(f.formula2,varEquivalenceMapThis,varEquivalenceMapThat) && this.formula1.isEquivalentModuloVarRenaming(f.formula2,varEquivalenceMapThis,varEquivalenceMapThat)){
				//System.out.println("True -- case 3");	
				return true;
			}
			//System.out.println("False:  " + this + " not equivalent to " + f + " " + this.con.equals(f.con) + " " + this.formula1.isEquivalentModuloVarRenaming(f.formula1,varEquivalenceMapThis,varEquivalenceMapThat) + " " + this.formula2.isEquivalentModuloVarRenaming(f.formula2,varEquivalenceMapThis,varEquivalenceMapThat));

			// TODO: X -> Y == not X v Y
		}
		return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.addAll(formula1.yield());
		yield.add(con.toString());
		yield.addAll(formula2.yield());
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.addAll(formula1.yieldTypes());
		yieldTypes.add(TerminalType.CONNECTIVE);
		yieldTypes.addAll(formula2.yieldTypes());
		return yieldTypes;
	}

	public HashSet<semanticsOLD.lambdaFOL.Variable> getAllVariables() {
		HashSet<semanticsOLD.lambdaFOL.Variable> varSet = new HashSet<semanticsOLD.lambdaFOL.Variable>();
		HashSet<semanticsOLD.lambdaFOL.Variable> f1Vars = formula1.getAllVariables();
		if (f1Vars != null){
			varSet.addAll(f1Vars);
		}
		HashSet<semanticsOLD.lambdaFOL.Variable> f2Vars = formula2.getAllVariables();
		if (f2Vars != null){
			varSet.addAll(f2Vars);
		}
		return varSet;		
	}

	protected semanticsOLD.lambdaFOL.Expression renameVar(semanticsOLD.lambdaFOL.Variable v, semanticsOLD.lambdaFOL.Variable v1) {
		semanticsOLD.lambdaFOL.Expression f1 = formula1.renameVar(v,v1);
		semanticsOLD.lambdaFOL.Expression f2 = formula2.renameVar(v,v1);
		return new ComplexFormula(f1, this.con, f2);
	}

	public semanticsOLD.lambdaFOL.Expression flattenNestedFormulas(){
		semanticsOLD.lambdaFOL.Expression f1 = formula1.flattenNestedFormulas();
		semanticsOLD.lambdaFOL.Expression f2 = formula2.flattenNestedFormulas();
		if ((this.connective() == Connective.AND || this.connective() == Connective.OR)
				&& (this.connective() == f1.connective() || this.connective() == f2.connective()) ){
			
			ArrayList<semanticsOLD.lambdaFOL.Expression> fList = new ArrayList<semanticsOLD.lambdaFOL.Expression>();
			if (f1.connective() == this.connective()){
				if (f1 instanceof ComplexFormula){
					fList.add(((ComplexFormula)f1).formula1);
					fList.add(((ComplexFormula)f1).formula2);			
				}
				else if (f1 instanceof FormulaList){
					fList.addAll(((FormulaList)f1).formulas);
				}
				else {
					fList.add(f1);// this shouldn't happen -- only complex formulas and formula lists have non-null connectives!
				}
			}
			else {
				fList.add(f1);
			}
			if (f2.connective() == this.connective()){
				if (f2 instanceof ComplexFormula){
					fList.add(((ComplexFormula)f2).formula1);
					fList.add(((ComplexFormula)f2).formula2);			
				}
				else if (f2 instanceof FormulaList){
					fList.addAll(((FormulaList)f2).formulas);
				}
				else {
					fList.add(f2);// this shouldn't happen -- only complex formulas and formula lists have non-null connectives!
				}
			}
			else {
				fList.add(f2);
			}
			return FormulaList.createFormulaList(this.connective(), fList);
		}
		else {
			return new ComplexFormula(f1, this.con, f2);
		}
		
	}

}
