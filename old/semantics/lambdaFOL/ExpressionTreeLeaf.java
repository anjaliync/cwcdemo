package semanticsOLD.lambdaFOL;

import semanticsOLD.lambdaFOL.Variable.Status;

public class ExpressionTreeLeaf extends ExpressionTree {
	String string;

	ExpressionTreeLeaf(String s, ExpressionTreeNode p){
		if (isTermVariable(s)){
			expression = TermVariable.createTermVariable(s);
			type = Type.TERM_VARIABLE;
		}
		else if (isFormulaVariable(s)){
			expression = FormulaVariable.createFormulaVariable(s);
			type = Type.FORMULA_VARIABLE;
		}
		else if (isConstant(s)){
			expression = semanticsOLD.lambdaFOL.Constant.createConstant(s);
			type = Type.CONSTANT;
		}
		else if (isLambda(s)){
			type = Type.LAMBDA;
			expression = Expression.parseToken(string);
		}
		else if (isQuantifier(s)){
			type = Type.QUANTIFIER;
			expression  = Expression.parseToken(string);
		}
		parent = p;
		if (parent != null)
			p.addChild(this);
		string = (expression != null)?expression.toString():s;
	}
	
	
	ExpressionTreeLeaf(String s, ExpressionTreeNode p, Status status){
		if (isTermVariable(s)){
			expression = TermVariable.createTermVariable(s,status);
			type = Type.TERM_VARIABLE;
		}
		else if (isFormulaVariable(s)){
			expression = FormulaVariable.createFormulaVariable(s,status);
			type = Type.FORMULA_VARIABLE;
		}
		else if (isConstant(s)){
			expression = semanticsOLD.lambdaFOL.Constant.createConstant(s);
			type = Type.CONSTANT;
		}
		else if (isLambda(s)){
			type = Type.LAMBDA;
			expression = Expression.parseToken(string);
		}
		else if (isQuantifier(s)){
			type = Type.QUANTIFIER;
			expression  = Expression.parseToken(string);
		}
		//string = (type == Type.LAMBDA)?"λ"+ expression.toString():expression.toString();	
		parent = p;
		if (parent != null)
			p.addChild(this);
		string = (expression != null)?expression.toString():s;
	}
	
	public ExpressionTreeLeaf(Type t, String s, ExpressionTreeNode p){
		type = t;// this assumes the type matches the expression
		expression = Expression.parseToken(s);// either a variable or a constant -- or a predicate?? 
		string = (expression != null)?expression.toString():s;
		parent = p;
		if (parent != null)
			p.addChild(this);
	}

	public ExpressionTreeLeaf(Type t, String s, ExpressionTreeNode p, Status status) {
		type = t;
		//System.out.println("NEW CONSTRUCTOR FOR ExpressionTreeLeaf: Type " + t + " string " + s + " parent: " + p + " status: " + status);
		if (t == Type.TERM_VARIABLE){
			expression = TermVariable.createTermVariable(s,status);
			type = Type.TERM_VARIABLE;
		}
		else if (t == Type.FORMULA_VARIABLE){
			expression = FormulaVariable.createFormulaVariable(s,status);
		}
		else {
			System.out.println("THIS CONSTRUCTOR SHOULD ONLY BE CALLED ON VARIABLES...");
		}
		parent = p;
		if (parent != null)
			p.addChild(this);
		string = (expression != null)?expression.toString():s;
	}


	public String toString(){
		return new String(string);
	}
	
	public semanticsOLD.lambdaFOL.Variable toVariable() {
		semanticsOLD.lambdaFOL.Variable var = null;
		switch (type){
		case TERM_VARIABLE:
			var = (TermVariable)expression;
			break;
		case FORMULA_VARIABLE:
			var = (FormulaVariable)expression;
			break;
		case LAMBDA:		
			var = (semanticsOLD.lambdaFOL.Variable)expression;//
			//System.out.println("ExpressionLeaf.toVariable(): lambda " + string + " -> " + var);
			break;
		default:
				System.out.println("ExpressionLeaf.toVariable() ERROR: this is not a variable " + this); 
		}
		return var;
	}
	
	public semanticsOLD.lambdaFOL.Variable toVariable(Status status) {
		semanticsOLD.lambdaFOL.Variable var = null;
		semanticsOLD.lambdaFOL.Variable retVar = null;
		switch (type){
		case TERM_VARIABLE:
			var = (TermVariable)expression;
			retVar = new TermVariable((TermVariable)var, status);
			break;
		case FORMULA_VARIABLE:
			var = (FormulaVariable)expression;
			retVar = new FormulaVariable((FormulaVariable)var, status);
			break;
		case LAMBDA:		
			var = (semanticsOLD.lambdaFOL.Variable)expression;
			//retVar = new Variable(var, status);
			System.out.println("CHECK ME -- ExpressionLeaf.toVariable(): lambda " + string + " -> " + var);
			break;
		default:
				System.out.println("ExpressionLeaf.toVariable() ERROR: this is not a variable " + this); 
		}
		return retVar;
	}
	
	public TermVariable toTermVariable(){
		TermVariable var = null;
		if (this.type == Type.TERM_VARIABLE){
			var = (TermVariable)expression;
		}
		return var;
	}
	public TermVariable toTermVariable(Status status) {
		TermVariable retVar = null;
		if (this.type == Type.TERM_VARIABLE){
			TermVariable var = (TermVariable)expression;
			retVar = new TermVariable(var, status);
		}
		return retVar;
	}
	
	public Formula toFormula(){
		Formula f = null;
		if (type == Type.FORMULA_VARIABLE){
			System.out.println("Bug? Calling createFormulaVariable inside ExpressionTreeLeaf.toFormula()");
			f = FormulaVariable.createFormulaVariable(string);//TODO: debug me!
		}
		return f;
	}
	public Expression.Connective toConnective(){
		if (type == Type.CONNECTIVE){
			if (string.equals(Expression.AND)){
				return Expression.Connective.AND;
			}
			else if (string.equals(Expression.OR)){
				return Expression.Connective.OR;
			}
			else if (string.equals(Expression.IMPLIES)){
				return Expression.Connective.IMPLIES;
			}
		}
		return null;
	}
	
	
	public Expression toExpression(){
		Expression e = null;
		switch (type){
		case CONSTANT:
			e = new semanticsOLD.lambdaFOL.Constant(string);
			break;
		case TERM_VARIABLE:
			e = expression;
			//System.out.println("CHECK " + e + " " + var2);
			break;
		case FORMULA_VARIABLE:
			e = expression;
			break;
		case PREDICATE:
			System.out.println("ExpressionLeaf.toExpression() TODO: predicate " + string);
			break;
		case LAMBDA:
			System.out.println("ExpressionLeaf.toExpression() TODO: lambda " + string);
			break;
		case CONNECTIVE:
			System.out.println("ExpressionLeaf.toExpression() TODO: connective " + string);
			break;
		default:
			System.out.println("ExpressionLeaf.toExpression() ERROR: this should not be a leaf " + this); 
		}
		return e;
	}
}
