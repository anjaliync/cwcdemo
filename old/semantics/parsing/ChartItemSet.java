package semanticsOLD.parsing;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;


public class ChartItemSet<C extends Category, E extends EquivalenceClass<C>> 
extends HashSet<ChartItem<C, E>> implements Externalizable {
	
	protected HashMap<E, ChartItem<C, E>> items;
	
	public ChartItemSet() {
		this.items = new HashMap<E, ChartItem<C, E>>();
	}
	
	/**
	 * If an equivalent item already exists in the collection, 
	 * return it; otherwise add newItem to the collection and return it.
	 */
	public ChartItem<C, E> addItem(ChartItem<C, E> newItem) {
		E eq = newItem.getEquivalenceClass();
		ChartItem<C, E> existing = this.items.get(eq);
		if(existing == null) {
			this.items.put(eq, newItem);
			return newItem;
		}
		else {
			return existing;
		}
	}
	
	public ChartItem<C, E> getItem(E equivalenceClass) {
		return this.items.get(equivalenceClass);
	}
	
	public Collection<ChartItem<C, E>> items() {
		return this.items.values();
	}

    @Override
	public Iterator<ChartItem<C, E>> iterator() {
	    return this.items.values().iterator();
	}

	@Override
	public int size() {
		return this.items.size();
	}

	@Override
    public boolean contains(Object o) {
		if(o instanceof ChartItem<?, ?>) {
			ChartItem<C, E> item = (ChartItem<C, E>) o;
			ChartItem<C, E> existing = this.getItem(item.getEquivalenceClass());
			if(existing != null) {
				return existing.equals(item);
			}
		}
		return false;
    }
    
    @Override
    public boolean isEmpty() {
        return this.items.isEmpty();
    }
    
    @Override
	public boolean add(ChartItem<C, E> item) {
		return this.addItem(item) == item;
	}

	@Override
    public boolean remove(Object o) {
    	boolean present = this.items.containsKey(o);
    	if(present) {
    		this.items.remove(o);
    	}
    	return present;
    }

    /**
     * Removes all of the elements from this set.
     * The set will be empty after this call returns.
     */
    @Override
    public void clear() {
        this.items.clear();
    }

    /**
     * Returns a shallow copy of this instance: the elements
     * themselves are not cloned.
     *
     * @return a shallow copy of this set
     */
    @SuppressWarnings("unchecked")
	@Override
    public ChartItemSet<C, E> clone() {
    	ChartItemSet<C, E> newSet = (ChartItemSet<C, E>) super.clone();
        newSet.items = (HashMap<E, ChartItem<C, E>>) this.items.clone();
        return newSet;
    }

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(out);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.items = (HashMap<E, ChartItem<C, E>>) in.readObject();
	}

	public Set<E> equivalenceClasses() {
		return this.items.keySet();
	}
	
	public Set<Entry<E, ChartItem<C, E>>> mapEntries() {
		return this.items.entrySet();
	}

	public void addOrUpdateBackpointers(ChartItem<C, E> item) {
		// addItem adds the item if no equivalent one already exists...
		ChartItem<C, E> added = this.addItem(item);
		if(added != item) {
			// ... but if one does, then we should update the backpointers
			added.addBackpointersFromEquivalentItem(item);
		}
	}
}
