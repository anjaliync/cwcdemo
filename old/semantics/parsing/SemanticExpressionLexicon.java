package semanticsOLD.parsing;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import semanticsOLD.ccg.CCGcat;
import semanticsOLD.ccg.ProducedCCGcat;
import semanticsOLD.ccg.Rule_Type;


public class SemanticExpressionLexicon extends Lexicon<SemanticExpressionCategory> {

	private semanticsOLD.lambdaFOL.SemLexicon lexicon;

	public SemanticExpressionLexicon(String lexiconFile) {
		semanticsOLD.lambdaFOL.Expression.readLexicon(lexiconFile);
		this.lexicon = semanticsOLD.lambdaFOL.Expression.lexicon;
	}
	public SemanticExpressionLexicon(semanticsOLD.lambdaFOL.SemLexicon lexicon) {
		this.lexicon = lexicon;
	}

	@Override
	public void load(File directory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(File directory) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object[] getParametersForFutureInstantiation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<LexicalCategoryCandidate<SemanticExpressionCategory>> getLexicalCategories(Sentence sentence, int i) {
		ArrayList<LexicalCategoryCandidate<SemanticExpressionCategory>> categories = 
				new ArrayList<LexicalCategoryCandidate<SemanticExpressionCategory>>();
		String word = sentence.wordAt(i);
		HashMap<String, ArrayList<semanticsOLD.lambdaFOL.Expression>> entries = lexicon.getAllEntries(word);
		if (entries != null) {
			for(String cat : entries.keySet()) {
				ProducedCCGcat ccgCat = new ProducedCCGcat(Rule_Type.LEXICAL_CATEGORY, -1,
						CCGcat.lexCat(word, cat, sentence.posTagAt(i), i));
				for (semanticsOLD.lambdaFOL.Expression exp : entries.get(cat)) {
					categories.add(
							new LexicalCategoryCandidate<SemanticExpressionCategory>(
									new SemanticExpressionCategory(ccgCat, exp), i));
				}
			}
		}
		return categories;
	}

	@Override
	public Collection<CategoryCandidate<SemanticExpressionCategory>> getInitialCategories(Sentence sentence) {
		Collection<CategoryCandidate<SemanticExpressionCategory>> itemsToAdd = 
				new ArrayList<CategoryCandidate<SemanticExpressionCategory>>();
		for(int i=0; i<sentence.length(); i++) {
			itemsToAdd.addAll(this.getLexicalCategories(sentence, i));
		}
		return itemsToAdd;
	}

}
