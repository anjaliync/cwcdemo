package semanticsOLD.parsing;

import java.io.File;


public class ProfligateChartItemFactory<C extends Category, E extends EquivalenceClass<C>> 
									extends ChartItemFactory<C, E>  {

	@Override
	public ChartItem<C, E> internalItem(E ec, int spanStartIndex, int spanEndIndex) {
		return new ProfligateChartItem<C, E>(ec, spanStartIndex, spanEndIndex);
	}

	@Override
	public void load(File directory) {
		// Nothing to load
	}

	@Override
	public void save(File directory) {
		// Nothing to save
	}

	@Override
	public Object[] getParametersForFutureInstantiation() {
		return new Object[]{};
	}
}
