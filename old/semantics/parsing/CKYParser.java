package semanticsOLD.parsing;

import java.io.File;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Scanner;

public abstract class CKYParser<C extends Category, 
E extends EquivalenceClass<C>> 
extends Parser<C, E> 
implements Loadable<Parser<C, E>> {

	protected static final String LEXICON_DIR = "lexicon";

	protected static final String MODEL_DIR = "model";

	protected Lexicon<C> lexicon;

	protected Model<C, E> model;

	public CKYParser() {}

	public CKYParser(Lexicon<C> lexicon, Model<C, E> model) {
		this.lexicon = lexicon;
		this.model = model;
	}

	public ParsedSentence<C, E> parse(Sentence sentence) {
		CKYChart<C, E> chart = this.chartParseCKY(sentence);
		return this.buildParseResult(chart);
	}

	public CKYChart<C, E> chartParseCKY(Sentence sentence) {
		// Initialize cache
		this.model.initializeCache(sentence);
		// Initialize chart
		CKYChart<C, E> chart = new CKYChart<C, E>(sentence);
		// Fill diagonal (lexical) cells -- in future, this may need to support multi-word expressions 
		this.addLexicalEntries(chart);
		// Fill internal cells
		this.fillInternalCells(chart);
		// Check for root
		this.assignRootCategory(chart);
		// Clear cache and return chart
		this.model.clearCache();
		return chart;
	}

	protected void fillInternalCells(CKYChart<C, E> chart) {
		int length = chart.sentence().length();
		for(int span=1; span<length; span++) {
			for(int i=0; i<length-span; i++) {
				int j = i+span;
				Cell<C, E> parentCell = chart.getCell(i, j);
				// Combine items for sub-spans
				for(int k=i; k<j; k++) {
					Cell<C, E> leftCell = chart.getCell(i, k);
					Cell<C, E> rightCell = chart.getCell(k+1, j);
					this.addBinaryParentItems(chart, parentCell, leftCell, rightCell);
				}
				// Add unary parents
				this.addUnaryParentItems(chart, parentCell);
			}
		}
	}

	protected void addLexicalEntries(CKYChart <C, E> chart) {
		// Add lexical categories
		Sentence sentence = chart.sentence();
		for(int i=0; i<sentence.length(); i++) {
			Cell<C, E> cell = chart.getCell(i, i);
			for(LexicalCategoryCandidate<C> lexCat : this.lexicon.getLexicalCategories(sentence, i)) {
				chart.addItemToChart(this.model.createLexicalItem(lexCat, sentence), cell);
			}
		}
		// Add unary parents
		for(int i=0; i<sentence.length(); i++) {
			Cell<C, E> cell = chart.getCell(i, i);
			this.addUnaryParentItems(chart, cell);
		}
	}

	protected void assignRootCategory(CKYChart <C, E> chart) {
		Collection<ChartItem<C, E>> roots = this.model.checkForRootItem(chart);
		if(roots.size() > 1) {
			System.err.println(	"WARNING: Multiple items were assigned the root category."
					+	"\n       Only one root item was stored; parse forest may have been damaged.");
		}
		if(roots.size() > 0) {
			chart.assignRoot(roots.iterator().next());
		}
	}

	protected void addUnaryParentItems(CKYChart<C, E> chart, Cell<C, E> cell) {
		for(ChartItem<C, E> parentItem : this.model.createUnaryParentItems(cell)) {
			chart.addItemToChart(parentItem, cell);
		}
	}

	protected void addBinaryParentItems(CKYChart<C, E> chart, Cell<C, E> parentCell, 
			Cell<C, E> leftCell, Cell<C, E> rightCell) {
		for(ChartItem<C, E> parentItem : this.model.createBinaryParentItems(leftCell, rightCell)) {
			chart.addItemToChart(parentItem, parentCell);
		}
	}

	public abstract ParsedSentence<C, E> buildParseResult(Chart<C, E> chart);

	public Lexicon<C> getLexicon() {
		return this.lexicon;
	}

	public Model<C, E> getModel() {
		return this.model;
	}

	/*
	public Grammar<C, E> getGrammar() {
		if(this.model instanceof SyntacticParsingModel) {
			return ((SyntacticParsingModel<C, E>) model).getGrammar();
		}
		return null;
	}
	*/

	@Override
	public void load(File directory) {
		try {
			String dirPath = directory.getPath()+File.separator;
			this.lexicon = Loader.load(dirPath+LEXICON_DIR);
			this.model = Loader.load(dirPath+MODEL_DIR);
		}
		catch(Exception e) {
			System.err.println("Failed to load parser from: "+directory.getPath());
			e.printStackTrace();
		}
	}

	@Override
	public void save(File directory) {
		try {
			String dirPath = directory.getPath()+File.separator;
			Loader.save(this.lexicon, dirPath+LEXICON_DIR);
			Loader.save(this.model, dirPath+MODEL_DIR);
		}
		catch(Exception e) {
			System.err.println("Failed to save parser to: "+directory.getPath());
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersForFutureInstantiation() {
		return new Object[]{};
	}

	/*
	public CKYChart<C, E> convertAutoParseTree(Sentence sentence, 
			boolean ignoreNormalForm, boolean initializeCache) {
		if(this.model instanceof SyntacticParsingModel<?, ?>) {
			return ((SyntacticParsingModel<C,E>)this.model).convertAutoParseTree(sentence, 
					ignoreNormalForm, initializeCache);
		}
		return new CKYChart<C, E>(sentence);
	}
	*/

	public void encode(CKYChart<C, E> chart, PrintWriter pw) {
		if(chart.root() == null) {
			pw.println(0);
			pw.close();
			return;
		}
		IntegerMapping<ChartItem<C, E>> items = new IntegerMapping<ChartItem<C, E>>("Chart items"); 
		this.readUsedItems(chart.root(), items);
		pw.println(items.size());
		pw.println(items.getIDAndAddIfAbsent(chart.root()));
		for(ChartItem<C, E> item : items.items()) {
			if(item instanceof LexicalCategoryChartItem) {
				// Write chart item encoding
				pw.println(this.encodeLexicalCategoryChartItem((LexicalCategoryChartItem<C, E>) item));
			}
			else {
				// Write chart item encoding
				pw.println(this.encodeInternalChartItem(item));				
				// Write backpointer encodings
				Collection<Backpointer<C, E>> bps = item.backpointers();
				pw.println(bps.size());
				for(Backpointer<C, E> bp : bps) {
					pw.println(this.encodeBackpointer(bp, items));
				}
			}
		}
		pw.close();
	}

	private void readUsedItems(ChartItem<C, E> item, IntegerMapping<ChartItem<C, E>> items) {
		if(items.contains(item) || item == null) {
			return;
		}
		if(!(item instanceof LexicalCategoryChartItem)) {
			Collection<Backpointer<C, E>> bps = item.backpointers();
			for(Backpointer<C, E> bp : bps) {
				if(bp instanceof UnaryBackpointer) {
					this.readUsedItems(((UnaryBackpointer<C, E>) bp).child(), items);
				}
				else if(bp instanceof BinaryBackpointer) {
					this.readUsedItems(((BinaryBackpointer<C, E>) bp).leftChild(), items);
					this.readUsedItems(((BinaryBackpointer<C, E>) bp).rightChild(), items);
				}
			}
		}
		items.getIDAndAddIfAbsent(item);
	}

	protected String encodeLexicalCategoryChartItem(LexicalCategoryChartItem<C, E> item) {
		return this.model.encodeLexicalCategoryChartItem(item);
	}

	protected String encodeInternalChartItem(ChartItem<C, E> item) {
		return this.model.encodeInternalChartItem(item);
	}

	protected String encodeBackpointer(Backpointer<C, E> bp, IntegerMapping<ChartItem<C, E>> items) {
		return this.model.encodeBackpointer(bp, items); 
	}

	public CKYChart<C, E> decode(Sentence sentence, Scanner sc) {
		CKYChart<C, E> chart = new CKYChart<C, E>(sentence);
		int numItems = Integer.parseInt(sc.nextLine());
		if(numItems > 0) {
			int rootID = Integer.parseInt(sc.nextLine());
			@SuppressWarnings("unchecked")
			ChartItem<C, E>[] decodedItems = new ChartItem[numItems];
			this.decodeRecurse(decodedItems, sc);
			ChartItem<C, E> root = decodedItems[rootID];
			chart.assignRoot(root);
			for(ChartItem<C, E> item : decodedItems) {
				if(item != null) {
					chart.getCell(item.spanStartIndex(), item.spanEndIndex()).addItem(item);
				}
			}
		}
		return chart;
	}

	protected void decodeRecurse(ChartItem<C, E>[] decodedItems, Scanner sc) {
		int processed = 0;
		while(sc.hasNextLine()) {
			String line = sc.nextLine();
			if(line.isEmpty()) {
				continue;
			}
			ChartItem<C, E> item = this.decodeChartItem(line);
			decodedItems[processed++] = item;
			if(!(item instanceof LexicalCategoryChartItem)) {
				int numBackpointers = Integer.parseInt(sc.nextLine());
				for(int b=0; b<numBackpointers; b++) {
					item.addBackpointer(this.decodeBackpointer(sc.nextLine(), decodedItems));
				}
			}
			if(processed == decodedItems.length) {
				break;
			}
		}
	}

	protected ChartItem<C, E> decodeChartItem(String encoding) {
		return this.model.decodeChartItem(encoding);
	}
	protected Backpointer<C, E> decodeBackpointer(String encoding, ChartItem<C, E>[] decodedItems) {
		return this.model.decodeBackpointer(encoding, decodedItems);
	}
	/*

	public void initializeTrainingDatastructures() {
		this.model.initializeTrainingDatastructures();
	}

	public void clearTrainingDatastructures() {
		this.model.clearTrainingDatastructures();
	}

	public void saveParametersDuringTraining(File dir) {
		this.model.saveParametersDuringTraining(dir);
	}

	public void trainOnViterbi(CKYChart chart, CKYChart gold, LearningRateAgenda learningRate,
			UpdateSchedule updateSchedule) {
		this.model.trainOnViterbi(chart, gold, learningRate, updateSchedule);
	}

	public void updateParameters() {
		this.model.updateParameters();
	}
	*/
}
