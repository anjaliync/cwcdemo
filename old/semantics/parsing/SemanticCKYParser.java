package semanticsOLD.parsing;

import java.util.Collection;

import semanticsOLD.ccg.Normal_Form;
import semanticsOLD.lambdaFOL.Expression;

public class SemanticCKYParser extends CKYParser<SemanticExpressionCategory, 
SemanticNormalFormEquivalenceClass> {
	
	// Empty constructor for loading from disk
	public SemanticCKYParser() {}

	// Constructor for creating a new semantic parser with a uniform scoring function
	public SemanticCKYParser(SemanticExpressionLexicon lexicon) {
		super(lexicon, new SymbolicSemanticParsingModel(Normal_Form.Full));
	}
	// Constructor for creating a new semantic parser with a uniform scoring function
	public SemanticCKYParser(String lexiconFile) {
		super(new SemanticExpressionLexicon(lexiconFile), 
				new SymbolicSemanticParsingModel(Normal_Form.Full));
	}
	

	@Override
	public ParsedSentence<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> buildParseResult(
			Chart<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> chart) {
		// TODO Ryan: I need to see what kind of ParsedSentence result makes sense here...
		return null;
	}
	
	protected Collection<Expression> parse(Sentence sentence, String atomicAnalysisPrefix) {
		// Initialize cache
		this.model.initializeCache(sentence);
		// Initialize chart
		CKYChart chart = new CKYChart(sentence);
		// Fill diagonal (lexical) cells -- in future, this may need to support multi-word expressions 
		this.addLexicalEntries(chart);
		// Fill internal cells
		this.fillInternalCells(chart);
		// Check for root
		this.assignRootCategory(chart, atomicAnalysisPrefix);
		// Clear cache and return chart
		this.model.clearCache();
		return ((semanticsOLD.parsing.SemanticParsingModel)this.model).getParsedExpressions(chart);
	}
	
	protected void assignRootCategory(CKYChart<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass>  chart, String atomicAnalysisPrefix) {
		Collection<ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass>> roots = 
			((semanticsOLD.parsing.SemanticParsingModel)this.model).checkForRootItem(chart, atomicAnalysisPrefix);
		if(roots.size() > 1) {
			System.err.println(	"WARNING: Multiple items were assigned the root category."
					+	"\n       Only one root item was stored; parse forest may have been damaged.");
		}
		if(roots.size() > 0) {
			chart.assignRoot(roots.iterator().next());
		}
	}
	
	/**
	 * Returns all Expressions regardless of parse tree root.
	 */
	public Collection<Expression> parseAny(String toParse) {
		return this.parse(new Sentence(toParse), null);
	}
	
	/**
	 * Returns all Expressions regardless of parse tree root.
	 */
	public Collection<Expression> parseAny(Sentence toParse) {
		return this.parse(toParse, null);
	}
	
	/**
	 * Only returns Expressions for parses that are sentences (a parse
	 * is a sentence here if its root category is atomic and begins with 'S')
	 */
	public Collection<Expression> parseSentence(String sentence) {
		return this.parse(new Sentence(sentence), "S");
	}
	/**
	 * Only returns Expressions for parses that are sentences (a parse
	 * is a sentence here if its root category is atomic and begins with 'S')
	 */
	public Collection<Expression> parseSentence(Sentence sentence) {
		return this.parse(sentence, "S");
	}

	
	/**
	 * Only returns Expressions for parses that are nouns/noun phrases
	 * (i.e., the root category of the parse is atomic and begins with 'N')
	 */	
	public Collection<Expression> parseEntity(String entity) {
		return this.parse(new Sentence(entity), "N");
	}	
	
	/**
	 * Only returns Expressions for parses that are nouns/noun phrases
	 * (i.e., the root category of the parse is atomic and begins with 'N')
	 */	
	public Collection<Expression> parseEntity(Sentence entity) {
		return this.parse(entity, "N");
	}	

}
