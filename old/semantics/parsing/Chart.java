package semanticsOLD.parsing;

public abstract class Chart<C extends Category, E extends EquivalenceClass<C>> {
	
	protected Sentence sentence;
	
	protected ChartItem<C, E> root;
	
	public Chart(Sentence sentenceToParse) {
		this.sentence = sentenceToParse;
		this.root = null;
	}
	
	public ChartItem<C, E> assignRoot(ChartItem<C, E> newRoot) {
		this.root = newRoot;
		return this.root();
	}
	
	public ChartItem<C, E> root() {
		return this.root;
	}
	
	public Sentence sentence() {
		return this.sentence;
	}

}
