package semanticsOLD.parsing;

public class CategoryCandidate<C extends Category> {
	
	protected C category;
	
	protected CategoryCandidate(C category) {
		this.category = category;
	}
	
	public C category() {
		return this.category;
	}

}
