package semanticsOLD.parsing;

public class LexicalCategoryCandidate<C extends Category> extends CategoryCandidate<C> {
	
	protected int sentencePosition;
	
	public LexicalCategoryCandidate(C category, int sentencePosition) {
		super(category);
		this.sentencePosition = sentencePosition;
	}
	
	public int sentencePosition() {
		return this.sentencePosition;
	}
	
}
