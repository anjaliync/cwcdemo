package semanticsOLD.parsing;

public class Supertag implements Comparable<Supertag> {
	
	protected String category;
	
	protected double prob;
	
	protected volatile int hashCode;
	
	public Supertag(String category) {
		this(category, 1.0);
	}
	
	public Supertag(String category, double prob) {
		this.category = category;
		this.prob = prob;
	}

	public String category() {
		return this.category;
	}
	
	public double prob() {
		return this.prob;
	}
	
	public double logprob() {
		return Math.log(this.prob);
	}
	
	public int hashCode() {
		int result = this.hashCode;
		if(result == 0) {
			result = 17;
			result = 31 * result + this.category.hashCode();
			this.hashCode = result;
		}
		return result;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof Supertag)) {
			return false;
		}
		Supertag c = (Supertag) o;
		return this == c
				|| (this.category != null ? this.category.equals(c.category) : c.category == null);
	}

	@Override
	public int compareTo(Supertag o) {
		return (int) Math.signum(o.prob - this.prob);
	}

	public static Supertag[] toSupertags(String[] supertags) {
		Supertag[] supertag = new Supertag[supertags.length];
		for(int i=0; i<supertags.length; i++) {
			supertag[i] = new Supertag(supertags[i]);
		}
		return supertag;
	}

	public void normalizeProb(double Z) {
		this.prob /= Z;
	}

}
