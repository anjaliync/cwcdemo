package semanticsOLD.parsing;

import java.io.File;

public interface Loadable<T> {
	
	public void load(File directory);
	
	public void save(File directory);

	public Object[] getParametersForFutureInstantiation();
	
}
