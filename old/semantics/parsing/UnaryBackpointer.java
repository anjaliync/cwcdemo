package semanticsOLD.parsing;


public class UnaryBackpointer<C extends Category, E extends EquivalenceClass<C>> 
										extends Backpointer<C, E> {

	protected ChartItem<C, E> child;
	
	public UnaryBackpointer(ChartItem<C, E> child) {
		super();
		this.child = child;
	}

	public UnaryBackpointer(ChartItem<C, E> child, double score) {
		super(score);
		this.child = child;
	}
	
	public ChartItem<C, E> child() {
		return this.child;
	}
	
	
}
