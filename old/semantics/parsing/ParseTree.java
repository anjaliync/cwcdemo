package semanticsOLD.parsing;

import semanticsOLD.ccg.DepList;
import semanticsOLD.ccg.DepRel;
import semanticsOLD.ccg.SemanticTuple;

public class ParseTree<C extends Category, E extends EquivalenceClass<C>> {
	
	protected Sentence yield;
	
	protected ParseTreeNode<C, E> root;
	
	public ParseTree(Sentence yield, ParseTreeNode<C, E> root) {
		this.yield = yield;
		this.root = root;
	}
	
	public Sentence yield() {
		return yield;
	}
	
	public ParseTreeNode<C, E> root() {
		return this.root;
	}
	
	public semanticsOLD.ccg.PargDepSet getPARGDeps() {
		semanticsOLD.ccg.PargDepSet depSet = new semanticsOLD.ccg.PargDepSet(yield);
		try {
			if(this.root != null) {
				DepRel[][] deps = buildCCGDeps(this.root);
				addDeps(depSet, deps);
			}
			else {
				System.out.println(root);
			}
		}
		catch(Exception e) {
			System.err.println("ERROR: Failed to build feature structure.");
			e.printStackTrace();
		}
		return depSet;		
	}

	private void addDeps(semanticsOLD.ccg.PargDepSet depSet, DepRel[][] deps) {
		if (deps != null) {
			for (int i = 0; i < deps.length; i++) {
				for (int j = 0; j < deps.length; j++) {
					if (deps[i][j] != null) {
						if (j == deps.length - 1) {} else {
							DepRel dep = deps[i][j];
							depSet.addDep(i, j, dep.category(), dep.slot(), 
									dep.extracted(), dep.bounded());
						}
					}
				}
			}
		}
	}

	private DepRel[][] buildCCGDeps(ParseTreeNode<C, E> node) {
		DepRel[][] deps = new DepRel[this.yield.length()][this.yield.length()];
		if (node.isUnary()) {
			SemanticTuple.copyDepRel(deps, buildCCGDeps(node.child()));
		} 
		else if (node.isBinary()) {
			// Left Traverse
			SemanticTuple.copyDepRel(deps, buildCCGDeps(node.leftChild()));
			// Right Traverse
			SemanticTuple.copyDepRel(deps, buildCCGDeps(node.rightChild()));
		}

		if (node.ccgCat() != null) {
			DepList filled = node.ccgcat.filled_ccgDependencies();
			if (filled != null) {
				while (filled != null) {
					SemanticTuple.addDependency(deps, filled);
					filled = filled.next();
				}
			}
		}
		return deps;
	}
	
	public String toString() {
		return this.toStringRecurse(this.root, "");
	}

	private String toStringRecurse(ParseTreeNode<C, E> node, String tab) {
		if(node == null) {
			return tab+"null";
		}
		if(node.isLeaf()) {
			return tab+ node.category()+" "+yield.wordAt(node.spanEnd());
		}
		if(node.isUnary()) {
			return tab+node.category()+"\n"+toStringRecurse(node.child(), tab+"  ");
		}
		if(node.isBinary()) {
			return tab+node.category()
			+"\n"+toStringRecurse(node.leftChild(), tab+"  ")
			+"\n"+toStringRecurse(node.rightChild(), tab+"  ");
		}
		return tab+"null";
	}

	public String getParseStringSingleLine() {
		StringBuilder sb = new StringBuilder();
		this.toStringSingleLineRecurse(this.root, sb);
		return sb.toString();
	}
	
	protected void toStringSingleLineRecurse(ParseTreeNode<C, E> node, StringBuilder sb) {
		sb.append("{ ");
		sb.append(node.category());
		if(node.isLeaf()) {
			sb.append(" "+this.yield.wordAt(node.spanEnd));
		}
		else if(node.isUnary()) {
			toStringSingleLineRecurse(node.child(), sb);
		}
		else if(node.isBinary()) {
			toStringSingleLineRecurse(node.leftChild(), sb);
			toStringSingleLineRecurse(node.rightChild(), sb);
		}
		sb.append(" }");
	}
}
