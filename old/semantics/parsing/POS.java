package semanticsOLD.parsing;

public class POS extends Tag {
	
	protected String baseTag;
	
	protected volatile int hashCode;
	
	public POS(String posTag) {
		this.baseTag = posTag;
	}
	
	@Override
	public String tag() {
		return this.baseTag;
	}
	
	public int hashCode() {
		int result = this.hashCode;
		if(result == 0) {
			result = 17;
			result = 31 * result + this.baseTag.hashCode();
			this.hashCode = result;
		}
		return result;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof POS)) {
			return false;
		}
		POS p = (POS) o;
		return this == p
				|| (this.baseTag != null ? this.baseTag.equals(p.baseTag) : p.baseTag == null);
	}
	
	public String toString() {
		return this.baseTag;
	}

	public static POS[] toPOS(String[] posTags) {
		POS[] pos = new POS[posTags.length];
		for(int i=0; i<posTags.length; i++) {
			pos[i] = new POS(posTags[i]);
		}
		return pos;
	}

}
