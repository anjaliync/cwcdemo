package semanticsOLD.generation;

import java.io.File;
import java.util.*;

import semanticsOLD.ccg.Normal_Form;
import semanticsOLD.parsing.Sentence;

public class GeneratorTest {
	public static void main(String[] args) {
		String lexFile = null, testFile = null, expFile = null;
		if (args != null && args.length > 0) {
			for (String arg : args) {
				String s[] = arg.split("=", 2);
				if (s.length == 2) {
					switch(s[0]) {
					case "lex":
						lexFile  = s[1];
						break;
					case "sen":
						testFile = s[1];
						break;
					case "exp":
						expFile  = s[1];
						break;
					}
				}
			}

			if (lexFile == null) {
				System.out.println("Error: Please specify a lexicon file.");
				System.exit(0);
			}

			if (testFile == null && expFile == null) {
				System.out.println("Error: Please specify either a file of test sentences or a file of test expressions.");
				System.exit(0);
			}
			
			Configuration config = new Configuration();
			config.NF    = Normal_Form.Full;
			config.DEBUG = false;

			semanticsOLD.lambdaFOL.Expression.readLexicon(lexFile);
			//			Expression.testLexicon();

			semanticsOLD.lambdaFOL.SemLexicon lexicon = semanticsOLD.lambdaFOL.Expression.lexicon;

			// Ryan: this is Anjali's original semantic parser constructor
			//CYKParser parser = new CYKParser(lexicon);

			// Ryan: this is my new semantic parser constructor/class
			semanticsOLD.parsing.SemanticCKYParser parser = new semanticsOLD.parsing.SemanticCKYParser(lexFile);

			ChartGenerator generator = new ChartGenerator(lexicon);

			System.out.println("\n\nLexicon and generator initialized.\n");
			
			Collection<Sentence> sentences = readSentences(testFile);
			for (Sentence sen : sentences) {
				try {
					System.out.println("\n==========================================================");
					System.out.println("\nAttempting to parse sentence: "+sen);
					
					config.DEBUG = false;
					
					// This is Anjali's 
					Collection<semanticsOLD.lambdaFOL.Expression> result_expressions = parser.parseSentence(sen);
					result_expressions.addAll(parser.parseEntity(sen));
					
					System.out.println("Parser parsed "+result_expressions.size()+" expressions.\nParsed expressions:");
					for (semanticsOLD.lambdaFOL.Expression exp : result_expressions) System.out.println("\t"+exp);
					System.out.println();
					
					for (semanticsOLD.lambdaFOL.Expression exp : result_expressions) {
						System.out.println("Generating from result expression: "+exp);
						Collection<ChartEdge> result_edges = generator.generate(exp);
						System.out.println(result_edges.size()+" results generated:");
						
						int i = 1;
						for (ChartEdge result_edge : result_edges) {
							System.out.println("\t"+i+". "+result_edge.getSurfaceString().replace("-", " "));
							// config.DEBUG = true;
							if (Configuration.DEBUG) {
								result_edge.printHistory();
								System.out.println();
							}
							config.DEBUG = false;
							i += 1;
						}
						
						System.out.println("\n");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (expFile != null) {
				System.out.println("\n==========================================================");
				System.out.println("Test expression file found. Reading expressions ...\n");
				try {
					Scanner exp = new Scanner(new File(expFile));
					while (exp.hasNextLine()) {
						String l = exp.nextLine().trim();
						if (!l.isEmpty() && !l.startsWith("//")) {
							semanticsOLD.lambdaFOL.Expression e = semanticsOLD.lambdaFOL.Expression.parseString(l);
							System.out.println("Read expression: "+e);

							Collection<ChartEdge> result_edges = generator.generate(e);
							System.out.println(result_edges.size()+" results generated:");
							for (ChartEdge result_edge : result_edges) System.out.println(result_edge);
							
							int i = 1;
							for (ChartEdge result_edge : result_edges) {
								System.out.println("\t"+i+". "+result_edge.getSurfaceString());
								// config.DEBUG = true;
								if (Configuration.DEBUG) {
									result_edge.printHistory();
									System.out.println();
								}
								config.DEBUG = false;
								i += 1;
							}
							
							System.out.println("\n");
							System.out.println("----------------------------------------------------------");
						}
					}
					exp.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				System.out.println("==========================================================");
			}
		}
	}

	private static Collection<Sentence> readSentences(String testFile) {
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		try {
			Scanner sc = new Scanner(new File(testFile));
			while(sc.hasNextLine()) {
				String line = sc.nextLine();
				if(!line.isEmpty()) {
					sentences.add(new Sentence(line));
				}
			}
			sc.close();
		}
		catch(Exception e) {
			System.err.println("Unable to read sentences from: "+testFile);
			e.printStackTrace();
		}
		return sentences;
	}
}
