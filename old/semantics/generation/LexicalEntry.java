package semanticsOLD.generation;

import semanticsOLD.lambdaFOL.Expression;

public class LexicalEntry {
	private String word;
	private String category;
	private Expression expression;
	
	public LexicalEntry(String w, String c, Expression e) {
		word       = w;
		category   = c;
		expression = e;
	}
	
	public String getWord() 		  { return word; }
	public String getCategory()       { return category; }
	public Expression getExpression() { return expression; }
	
	public boolean equals(Object o) {
		if (o instanceof LexicalEntry) {
			LexicalEntry obj = (LexicalEntry) o;
			return (word.equals(obj.word) && category.equals(obj.category) && expression.isEquivalentTo(obj.expression));
		}
		return false;
	}
	
	public int hashCode() {
		return 17 * word.hashCode() * 31 * category.hashCode() * 17 * expression.hashCode(); // TODO: idk?
	}
	
	public String toString() {
		return word + " " + category + " " + expression;
	}
}
