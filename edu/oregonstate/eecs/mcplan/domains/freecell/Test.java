package edu.oregonstate.eecs.mcplan.domains.freecell;

import edu.illinois.cs.cogcomp.ccg2.parser.TrainedSemanticParser;
import java.io.File;
import java.io.IOException;

public class Test {

    public static void main(String[] argv) {
        TrainedSemanticParser parser;
        try {
            parser = new TrainedSemanticParser(new File("cardsModel"), new File("cards.types"), new File("cards.ont"));
        } catch (IOException e) {
            System.out.println("Unable to open model file");
            return;
        }

        //col 2 col move
        System.out.println(parser.parse("move the queen of hearts to the king of hearts"));
        System.out.println(parser.parseSimplified("move the queen of hearts to the king of hearts"));
        System.out.println(parser.parseForPlanner("move the nine of spades to the king of hearts"));
        System.out.println(parser.parseForPlanner("move the ten of diamonds to the jack of spades"));

        //move the XX cards from col a to col b
        System.out.println("stack moves");
        System.out.println(parser.parse("move the stack below the queen of hearts to the king of hearts"));
        System.out.println(parser.parseSimplified("move the stack below the queen of hearts to the king of hearts"));
        System.out.println(parser.parseForPlanner("move the stack below the queen of hearts to the king of hearts"));

        //foundation move
        System.out.println("");
        System.out.println(parser.parse("move the queen to a foundation"));
        System.out.println(parser.parseSimplified("move the queen to a foundation"));
        System.out.println(parser.parseForPlanner("move the queen to a foundation"));

        //freecell move
        System.out.println("");
        System.out.println(parser.parse("move the queen to a freecell"));
        System.out.println(parser.parseSimplified("move the queen to a freecell"));
        System.out.println(parser.parseForPlanner("move the queen to a freecell"));
        System.out.println(parser.parseForPlanner("move the two of diamonds to a freecell"));

        //foundation move
        System.out.println("");
        System.out.println(parser.parse("move the three from the tableau to a foundation"));
        System.out.println(parser.parseSimplified("move the three from the tableau to a foundation"));
        System.out.println(parser.parseForPlanner("move the three from the tableau to a foundation"));

        //freecell to column
        System.out.println("");
        System.out.println(parser.parse("the queen of hearts is on top of the stack"));
        System.out.println(parser.parseSimplified("the queen of hearts is on top of the stack"));
        System.out.println(parser.parseForPlanner("the queen of hearts is on top of the stack"));

        System.out.println("");
        //System.out.println(parser.parseForPlanner("if the queen of hearts is on top of the stack then move the queen of hearts from a freecell to the stack"));
        System.out.println(parser.parseForPlanner("move the queen from a freecell to the stack"));
        System.out.println(parser.parseForPlanner("move the queen from a freecell to the stack"));


        System.out.println("");
        System.out.println(parser.parse("the six of diamonds is below the five of clubs"));
        System.out.println(parser.parseSimplified("the six of diamonds is below the five of clubs"));
        System.out.println(parser.parseForPlanner("the six of diamonds is below the five of clubs"));

        parser.shutdown();
    }
}
