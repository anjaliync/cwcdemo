package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.model.searchtree.INode;
import generator.Action;
import generator.Gen;
import generator.Predicate;

/**
 * Move a card from a column to a foundation.
 * 
 * @author George Heineman
 */
public class ColumnToFoundationMove extends FreeCellAction {

	/** nth column from which card comes. */
	public final int nth;
	
	/** card to be removed. */
	short card;
	
	/** suit of this card. */
	short suit;
	
	/** rank of this card. */
	short rank;
	
	public ColumnToFoundationMove (int nth) {
		this.nth = nth;
		this.actionTypeId = 1;
	}
	
	/**
	 * Execute the move on the given board state.
	 * 
	 * @param n   state on which to execute the move.
	 */
	public boolean execute(INode n) {
		if (!isValid(n)) {
			return false;
		}
		
		FreeCellNode state = (FreeCellNode) n;

		//state.detach(nth);
		Column col = state.cols[nth];
		
		col.remove();
		if (col.num == 0) { 
			state.sortMap(); 
		}
		state.insertFoundation(card);
		
		return true;
	}

	/**
	 * Determine if move is valid for the given state.
	 * 
	 * @param n
	 */
	public boolean isValid(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		if (nth < 0) return false;
		Column col = state.cols[nth];
		if (col.num == 0) return false;
		
		card = col.cards[col.num-1];
		
		suit = (short)(((card-1)%4));     // subtract 1 since '0' is invalid card.
		rank = (short)(1 + ((card-1)>>2));  // rank extracted this way.
		
		// always move an ace.
		if (rank == 1) {
			return true;
		} else {
			return (state.foundationEncoding[suit] + 1 == rank);
		}
	}

	/** 
	 * Assume move had been valid, so the undo is a straightforward swap.

	 * @param n    game state whose move is to be undone.  
	 */
	public boolean undo(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		
		state.removeFoundation(suit);
		
		Column col = state.cols[nth];
		col.add(card);
		return true;
	}
	public int getActionTypeId(){
		return this.actionTypeId;
	}

	@Override
	public int cardCount() {
		return 1;
	}

	@Override
	public int cardSource() {
		return nth;
	}

	@Override
	public int cardDestination() {
		return -2;
	}

	@Override
	public int cardSuit() {
		return suit;
	}

	@Override
	public int cardRank() {
		return rank;
	}

	@Override
	public int cardColor() {
		return 0;
	}

	/** Reasonable implementation. */
	public String toString () {

		return "move " + FreeCellNode.out(card) + " from column " + nth + " to foundation.";
	}

	public String getSemanticRep() {
		Action preferAction = null;
		preferAction = new generator.Action(Predicate.FINISH);
		preferAction.setEntity("the " + getRankString(this.cardRank()) + " of " + getSuit(this.cardSuit(), true));
		preferAction.setSource(getCardSourceString(this.cardSource(), true));
		preferAction.setDestination(getCardDestinationString(this.cardDestination(), true));
		return Gen.movePerformed(preferAction);
	}
}
