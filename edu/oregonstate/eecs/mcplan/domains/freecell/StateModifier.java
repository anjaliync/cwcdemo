package edu.oregonstate.eecs.mcplan.domains.freecell;

import edu.oregonstate.eecs.mcplan.History;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Processes GUI events over the JList to ensure the current board represents
 * the state BEFORE the final selected item.
 * <p>
 * Maintains invariant that drawer shows the state of board PRIOR to invoking the
 * selected move.
 * 
 * @author George Heineman
 */
public class StateModifier implements ListSelectionListener {

	/** Controlled JList. */
	JList list;
	
	/** Current board state. */
	FreeCellNode initial;
	
	/** FreeCellDrawing entity. */
	FreeCellDrawing drawer;
	History<FreeCellNode, FreeCellAction> history_;
	
	public StateModifier (JList list, FreeCellNode node, FreeCellDrawing drawer,
						  History<FreeCellNode, FreeCellAction> history) {
		this.list = list;
		
		// initial state
		this.initial = (FreeCellNode) node.copy();
		this.drawer = drawer;
		this.history_ = history;
	}
	

	public void setNode(FreeCellNode fcn) {
		initial = fcn;
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		// must find the one that is selected
		int idx = list.getSelectedIndex();
		DefaultListModel dlm = (DefaultListModel) list.getModel();
		if (idx < 0) idx = 0;
		FreeCellNode node = (FreeCellNode) history_.getState(idx);

		drawer.setNode(node);
		drawer.repaint();
	}
}
