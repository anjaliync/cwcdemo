package edu.oregonstate.eecs.mcplan.domains.blocksworld;


import JSHOP2.InternalDomain;
import JSHOP2.Plan;
import convert.Location;
import convert.ProblemModified;
import convert.SemanticRepresentationtoPDDL;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mislam1 on 9/8/16.
 */
public class BlocksPlanner {


    public List<String> parseInput(String input) {
        List<String> finalPlans = new ArrayList<String>();
        finalPlans.add("Marker: CONSTRAINTS");
        int x=0,y=0;
        ArrayList<String> initCon = new ArrayList<String>();
        String finalPlanString = "";
        for(x=0;x<9;x++){
            for(y=0;y<9;y++){
                System.out.println(x+"--------"+y+"\n");
                SemanticRepresentationtoPDDL.ProcessSemanticRepresentation(input);
                SemanticRepresentationtoPDDL.TransformShapesAxis(new Location(x,y));
                boolean validated = SemanticRepresentationtoPDDL.ValidateTheShapes();
                if (!validated) {
                    initCon=SemanticRepresentationtoPDDL.CreateProblemPDDL(SemanticRepresentationtoPDDL.blocksCount, 1);
                    //continue;
                }
                //String finalPlanString = "";
                if (!validated) {

                    try {
                        InternalDomain internalDomain = (new InternalDomain(new FileInputStream(new File("cwcproblem")), 1));
                        internalDomain.getParser().command();
                        LinkedList<Plan> plans = ProblemModified.getPlans();
                        String initstate = "Constraints{\n";
                        for(String s:initCon)
                        {
                            initstate+=s;
                            finalPlans.add(s);
                        }
                        initstate+="}.\n";
                        finalPlans.add("Marker:PLAN");
                        for (Plan p : plans) {
                            boolean first = false;
                            String planString = p.toString();
                            String[] splittedSteps = planString.split("\n");
                            for (int i = 0; i < splittedSteps.length; ++i) {
                                if (splittedSteps[i].contains("putdown"))
                                {
                                    String towrite = "";
                                    if(splittedSteps[i].contains("pickup"))
                                    {
                                        if(Double.parseDouble(splittedSteps[i].split(" ")[2])>=100.0)
                                            continue;
                                        else
                                        {
                                            for(int j=0;j<initCon.size();j++)
                                            {
                                                if(initCon.get(j).contains(splittedSteps[i].split(" ")[1]) && initCon.get(j).contains("exists-movable"))
                                                {
                                                    towrite = splittedSteps[i].replaceAll("pickup", "exists");
                                                    break;
                                                }

                                            }
                                            towrite = splittedSteps[i];
                                        }
                                    }
                                    
                                    if(splittedSteps[i].contains("putdown"))
                                    {
                                        towrite = splittedSteps[i]; //change needed
                                        for(int j=0;j<initCon.size();j++)
                                        {
                                            if(initCon.get(j).contains(splittedSteps[i].split(" ")[1]) && initCon.get(j).contains("exists-movable"))
                                            {
                                                towrite = "(move "+splittedSteps[i].split(" ")[1]+" "+initCon.get(j).split(" ")[2]+" "+initCon.get(j).split(" ")[3].replaceAll("\\)\n", "")+" "+splittedSteps[i].split(" ")[2]+" "+splittedSteps[i].split(" ")[3];
                                                break;
                                            }
                                        }

                                    }
                                    finalPlanString += towrite.replaceAll("\\!", "") + ", ";
                                    finalPlans.add(towrite.replaceAll("\\!", ""));
                                }
                            }
                        }

                        //System.out.println("change" + ProblemModified.getPlans());
                    } catch (Exception e) {
                        //e.printStackTrace();
                        System.out.println(e.getMessage());
                        continue;
                    }
                }
                if(finalPlanString.length()>1)
                    break;
            }
            if(finalPlanString.length()>1)
                break;}
        return finalPlans;
        //return initstate+"Plan{\n"+finalPlanString+"}";
    }


    /*public static void main(String[] args)
    {
        BlocksPlanner bp = new BlocksPlanner();
        String pl = bp.parseInput("column(a)^height(a,5)^column(b)^height(b,3)^right(b,a)^block(c)^location(w1)^block-location(c,w1)^enum(b,c,0)^ block(d)^location(w2)^block-location(d,w2)^enum(a,d,2)^spatial-rel(east,0,w2,w1)^immobile_block(4,3)^immobile_block(3,6)");
        System.out.println(pl);
    }*/
}