package edu.oregonstate.eecs.mcplan.domains.blocksworld;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Anjali on 4/30/17.
 */
public class BlocksWorldUtilities {
    public static List<String> readPredicateList(String file) {
        List<String> predicates = new ArrayList<>();
        File f = new File(file);

        try {
            BufferedReader bf = new BufferedReader(new FileReader(f));
            String line;
            while((line=bf.readLine())!=null)
                predicates.add(line.trim());
            bf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return predicates;
    }

    public static List<String> fixSpatialRelations(List<String> parse) {
        List<String> fixed = new ArrayList<>();
        for (String increment : parse)
            fixed.add(increment.replaceAll("spatial-rel\\(([a-z]+),([0-9]+),([a-z][0-9]),([a-z][0-9])","spatial-rel\\($1,$2,$4,$3"));
        return fixed;
    }

    public static List<String> normalizeSpatialRelConsistency(List<String> parse) {
        HashMap<String, HashMap<String, String>> relations = new HashMap<>();
        HashMap<String, String> blockToShapeMap    = new HashMap<>();
        HashMap<String, String> locationToBlockMap = new HashMap<>();

        for (String increment : parse) {
            for (String predicate : increment.split("\\^")) {
                String name = predicate.split("[(]")[0].trim();
                String args = predicate.split("[(]")[1].replace(")","").trim();

//                if (name.equals("row") || name.equals("column") || name.equals("square") || name.equals("rectangle"))
//                    if (!relations.containsKey(args)) relations.put(args, new HashMap<>());

                if (name.equals("top") || name.equals("bottom") || name.equals("left") || name.equals("right")) {
                    String b = args.split(",")[0].trim();
                    String a = args.split(",")[1].trim();

                    if (!relations.containsKey(b))
                        relations.put(b, new HashMap<>());

                    if (relations.get(b).containsKey(a)) {
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- "+String.join("^",parse));
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- duplicate relation "+predicate);
                        return parse;
                    }

                    relations.get(b).put(a, name);
                }

                if (name.equals("block-row-ind") || name.equals("block-col-ind") || name.equals("left_side") || name.equals("right_side")
                    || name.equals("top_side") || name.equals("bottom_side") || name.equals("upper_left") || name.equals("upper_right")
                    || name.equals("lower_left") || name.equals("lower_right") || name.equals("top-end") || name.equals("bottom-end")
                    || name.equals("left-end") || name.equals("right-end")) {
                    String shape = args.split(",")[0].trim();
                    String block = args.split(",")[1].trim();

                    if (blockToShapeMap.containsKey(block)) {
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- "+String.join("^",parse));
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- duplicate block-shape "+predicate);
                        return parse;
                    }

                    blockToShapeMap.put(block, shape);
                }

                if (name.equals("block-location")) {
                    String block    = args.split(",")[0].trim();
                    String location = args.split(",")[1].trim();

                    if (locationToBlockMap.containsKey(location)) {
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- "+String.join("^",parse));
                        System.out.println("UTILITIES::normalizeSpatialRelConsistency -- duplicate location-block "+predicate);
                        return parse;
                    }

                    locationToBlockMap.put(location, block);
                }
            }
        }

//        System.out.println("Relations: ");
//        for (String b : relations.keySet()) {
//            System.out.println(b+": ");
//            for (String a : relations.get(b).keySet())
//                System.out.println("\ta,"+relations.get(b).get(a));
//            System.out.println();
//        }
//
//        System.out.println("Block to shape map: ");
//        for (String block : blockToShapeMap.keySet())
//            System.out.println("Block: "+block+" Shape: "+blockToShapeMap.get(block));
//
//        System.out.println("Location to block map: ");
//        for (String location : locationToBlockMap.keySet())
//            System.out.println("Location: "+location+" Block: "+locationToBlockMap.get(location));

        List<String> fixed = new ArrayList<>();
        for (String increment : parse) {
            if (increment.contains("spatial-rel")) {
                for (String predicate : increment.split("\\^")) {
                    String name = predicate.split("[(]")[0].trim();
                    String args = predicate.split("[(]")[1].replace(")","").trim();

                    if (name.equals("spatial-rel")) {
                        String d  = args.split(",")[1].trim();
                        String w1 = args.split(",")[2].trim();
                        String w2 = args.split(",")[3].trim();

                        if (!locationToBlockMap.containsKey(w1) || !locationToBlockMap.containsKey(w2)) {
                            System.out.println("UTILITIES::normalizeSpatialRelConsistency -- "+String.join("^",parse));
                            System.out.println("UTILITIES::normalizeSpatialRelConsistency -- no such location mapping "+predicate);
                            return parse;
                        }

                        String w1b = locationToBlockMap.get(w1);
                        String w2b = locationToBlockMap.get(w2);

                        if (!blockToShapeMap.containsKey(w1b) && !blockToShapeMap.containsKey(w2b)) {
                            System.out.println("UTILITIES::normalizeSpatialRelConsistency -- "+String.join("^",parse));
                            System.out.println("UTILITIES::normalizeSpatialRelConsistency -- no such block mapping "+predicate);
                            return parse;
                        }

                        String w1s = blockToShapeMap.get(w1b);
                        String w2s = blockToShapeMap.get(w2b);
                        String dir = null;

                        if (relations.containsKey(w1s) && relations.get(w1s).containsKey(w2s))
                            dir = getCardinalDirection(relations.get(w1s).get(w2s), false);

                        else if (relations.containsKey(w2s) && relations.get(w2s).containsKey(w1s))
                            dir = getCardinalDirection(relations.get(w2s).get(w1s), true);

                        if (dir != null)
                            increment = increment.replace(predicate.trim(), "spatial-rel("+dir+","+d+","+w1+","+w2+")");
                    }
                }
            }

            fixed.add(increment);
        }

        return fixed;
    }

    private static String getCardinalDirection(String direction, boolean flipped) {
        switch (direction) {
            case "top":
                if (!flipped) return "north";
                return "south";
            case "bottom":
                if (!flipped) return "south";
                return "north";
            case "left":
                if (!flipped) return "west";
                return "east";
            case "right":
                if (!flipped) return "east";
                return "west";
        }

        return null;
    }

    public static void main(String[] args) {
        String[] parse = {"row(a) ^ width(a,5)", "column(b)^height(b,4)^left(b,a)", "block(c)^location(w1)^block-location(c,w1)^top-end(b,c)^block(d)^location(w2)^block-location(d,w2)^left-end(a,d)^spatial-rel(west,0,w1,w2)"};
        System.out.println(normalizeSpatialRelConsistency(Arrays.asList(parse)));
    }
}
