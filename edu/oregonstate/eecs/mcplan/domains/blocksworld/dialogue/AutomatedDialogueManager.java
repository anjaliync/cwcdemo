//package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;
//
//import convert.BlocksPlanner;
//import convert.Response;
//import demo.blocksworld.BlocksConfiguration;
//import demo.blocksworld.InstructionGenerator;
//import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//
///**
// * Created by Anjali on 4/25/17.
// */
//public class AutomatedDialogueManager {
//        protected BlocksSemanticParser parser;         // semantic parser
//        protected BlocksPlanner planner;               // planner
//        protected InstructionGenerator generator;      // instruction generator
//
//        protected List<DialogueState> history;         // history of dialogue states & information
//        protected List<ParserOutput> descriptions;     // history of (successful) descriptions
//        protected List<ParserOutput> clarifications;   // history of (successful) clarifications
//        protected List<Response> plans;                // history of (successful) plans
//
//        protected State next;                          // next state
//
//        protected String systxt;                       // system output text
//        protected BlocksConfiguration sysconf;         // system output configuration
//
//        protected ParserOutput po;                     // parser output
//        protected Response plan;                       // planner output
//
//        protected Random rng;                          // random number generator
//
//        protected static final int ATTEMPT_LIMIT = 5;  // limit number of failed attempts before failing entirely
//        protected int descriptionAttempts;             // number of description attempts
//        protected int clarificationAttempts;           // number of clarification attempts
//
//        // dialogue states
//        protected enum State {
//            START,                      // start state
//            REQUEST_DESCRIPTION,        // request a description
//            PARSE_DESCRIPTION,          // parse a description
//            PLAN,                       // plan
//            REQUEST_CLARIFICATION,      // request a clarification
//            PARSE_CLARIFICATION,        // parse a clarification
//            FINISHED,                   // finished
//            FAILURE                     // failure
//        }
//
//        // planner flags
//        protected enum PlannerFlag {
//            COMPLETED,                  // completed
//            MISSING,                    // missing information
//            CLARIFICATION,              // unknown shape
//            SUGGESTION                  // planner suggestion (unused)
//        }
//
//        /**
//         * Constructs a dialogue manager.
//         */
//        public AutomatedDialogueManager() {
//            // initialize components
//            this.parser    = new BlocksSemanticParser();
//            this.planner   = new BlocksPlanner();
//            this.generator = new InstructionGenerator();
//
//            // initialize text and blocks configuration
//            this.systxt  = "[MACHINE]: Please describe your shape(s).\n";
//            this.sysconf = null;
//
//            // initialize histories
//            this.history        = new ArrayList<>();
//            this.descriptions   = new ArrayList<>();
//            this.clarifications = new ArrayList<>();
//            this.plans          = new ArrayList<>();
//            this.history.add(new DialogueState(START));
//
//            // initialize the state machine
//            this.next = START;
//
//            // reset number of attempts
//            this.descriptionAttempts   = 0;
//            this.clarificationAttempts = 0;
//
//            // initialize random number generator
//            this.rng = new Random();
//        }
//
//        /**
//         * Resets dialogue manager back to start state and clears
//         * saved system text and blocks configuration. Also reset the
//         * number of attempts used at each stage.
//         */
//        public void reset() {
//            // reset text and blocks configuration
//            this.systxt  = "[MACHINE]: Please describe your shape(s).\n";
//            this.sysconf = null;
//
//            // reset histories
//            this.history        = new ArrayList<>();
//            this.descriptions   = new ArrayList<>();
//            this.clarifications = new ArrayList<>();
//            this.history.add(new DialogueState(START));
//
//            // initialize the state machine
//            this.next = START;
//
//            // reset number of attempts
//            this.descriptionAttempts   = 0;
//            this.clarificationAttempts = 0;
//
//            // TODO: reset the parser history
//
//            // reset the planner history
//            this.planner.probe("", true);
//        }
//
//        public ParserOutput getCurrentParse() { return this.po; }
//
//        /**
//         * Gets the system text to output into the system text panel.
//         * If null or empty, there is no text to be printed.
//         * @return System text
//         */
//        public String getSystemText() { return this.systxt; }
//
//        /**
//         * Gets the system configuration to output into the test configuration panel.
//         * If null, there is no planned configuration to be displayed.
//         * @return System blocks configuration
//         */
//        public BlocksConfiguration getSystemBlocksConfiguration() { return this.sysconf; }
//
//        /**
//         * Returns whether or not the dialogue manager is in a 'finished' state.
//         * @return True if finished; false otherwise
//         */
//        public boolean isFinished() { return this.history.get(this.history.size()-1).state == FINISHED; }
//
//        /**
//         * Returns whether or not the dialogue manager is in a 'failure' state.
//         * @return True if failure; false otherwise
//         */
//        public boolean hasFailed() { return this.history.get(this.history.size()-1).state == FAILURE; }
//
//        /**
//         * Parses an input string and takes action based on the current state and behavior defined by the
//         * contained state machine.
//         * @param text (Human) input string
//         */
//        public void parse(String text) {
//            DialogueState ds;
//            switch (this.next) {
//                // request a (continuing) description from the user
//                case REQUEST_DESCRIPTION:
//                    ds = new DialogueState(REQUEST_DESCRIPTION);
//                    this.printState(this.next);
//
//                    this.systxt += "[MACHINE]: Please continue to describe your shape(s).\nAlternatively, type \"I'm finished\" when you're done with your description.\n";
//                    this.next = PARSE_DESCRIPTION;
//                    this.appendToHistory(ds, this.systxt);
//                    return; // quit and wait for new user input to be handled at the parsing state
//
//                // start state
//                case START:
//                    this.next = PARSE_DESCRIPTION;
//
//                    // parse user description
//                case PARSE_DESCRIPTION:
//                    ds = new DialogueState(PARSE_DESCRIPTION, text, null);
//                    this.printState(this.next);
//
//                    // user indicates they are finished with their description
//                    if (text.toLowerCase().contains("I'm finished")) {
//                        this.systxt = "[SYSTEM]: Complete!\n";
//                        this.next = FINISHED;
//                        this.appendToAndEndHistory(ds, this.systxt, FINISHED);
//                        return; // quit until state machine is reset
//                    }
//
//                    // parse the input
//                    this.po = parser.parseInput(text, false);
//                    this.descriptionAttempts += 1;
//                    ds.parse = this.po;
//                    System.out.println(this.po.getParse());
//
//                    // parse failure
//                    if (this.po.getParse() == null) {
//                        // ask for a rewording if under the attempt limit
//                        if (this.descriptionAttempts < ATTEMPT_LIMIT) {
//                            this.systxt = "[MACHINE]: Sorry, I didn't understand that. Could you reword your request?\n";
//                            this.appendToHistory(ds, this.systxt);
//                            this.next = PARSE_DESCRIPTION;
//                            return; // quit and wait for new user input to be handled at the parsing state
//                        }
//
//                        // quit if too many description attempts have been made
//                        this.systxt = "[MACHINE]: Sorry, attempt limit exceeded.\n";
//                        this.next = FAILURE;
//                        this.appendToAndEndHistory(ds, systxt, FAILURE);
//                        return; // quit until state machine is reset
//                    }
//
//                    this.descriptions.add(po);
//                    this.history.add(ds);
//                    this.next = PLAN;
//                    // when successful, falls through to the planning state below
//
//                    // planner
//                case PLAN:
//                    ds = new DialogueState(PLAN);
//                    this.printState(this.next);
//
//                    String[] responses = {"Let me think...\n", "Thinking...\n", "Hmm...\n"};
//                    this.systxt = "[MACHINE]: "+responses[rng.nextInt(responses.length)];
//
//                    // attempt planning
//                    this.plan = this.planner.probe(po.getParse().get(0), false);
//                    ds.plan = this.plan;
//                    System.out.println("RESPONSE::\n"+plan);
//
//                    // TODO: check if missing information & handle using clarification
//                    // TODO: check if unknown shape definition & handle
//
//                    // planning returned with 'finished' flag
//                    if (this.plan.getResponseFlag() == FINISHED.ordinal()) {
//
//                        // planning unexpectedly failed
//                        if (this.plan.getPlan() == null || plan.getPlan().isEmpty()) {
//
//                            // ... during the parsing stage
//                            if (this.previousState() == PARSE_DESCRIPTION) {
//
//                                // ask for a rewording if under the attempt limit
//                                if (this.descriptionAttempts < ATTEMPT_LIMIT) {
//                                    this.systxt += "[MACHINE]: Sorry, I wasn't able to build that. Could you reword your request?\n";
//                                    this.descriptions.remove(this.descriptions.size()-1);
//                                    this.next = PARSE_DESCRIPTION;
//                                    this.appendToHistory(ds, this.systxt);
//                                    return; // quit and wait for new user input to be handled at the parsing state
//                                }
//
//                                // quit if too many description attempts have been made
//                                this.systxt = "[MACHINE]: Sorry, attempt limit exceeded.\n";
//                                this.descriptions.remove(this.descriptions.size()-1);
//                                this.next = FAILURE;
//                                this.appendToAndEndHistory(ds, this.systxt, FAILURE);
//                                return; // quit until state machine is reset
//                            }
//                        }
//
//                        // TODO: planner unexpectedly fails during clarification
//                        if (this.previousState() == PARSE_CLARIFICATION) {
//                            return;
//                        }
//
//                        this.plans.add(this.plan);
//                        this.history.add(ds);
//                        this.next = REQUEST_VERIFICATION;
//                        // planning is finished and successful, so move to verification state
//                    }
//
//                    // TODO: implement missing flag. What is the difference between MISSING and CLARIFICATION?
//                    else if (this.plan.getResponseFlag() == MISSING.ordinal()) {
//                        this.plans.add(this.plan);
//                        this.next = REQUEST_CLARIFICATION;
//                        this.parse("");
//                        return;
//                    }
//
//                    // TODO: implement clarification flag
//                    else if (this.plan.getResponseFlag() == CLARIFICATION.ordinal()) {
//
//                    }
//
//                    // TODO: implement suggestion flag
//                    else if (this.plan.getResponseFlag() == SUGGESTION.ordinal()) {
//
//                    }
//
//                    // request user to verify correctness of planner output
//                case REQUEST_VERIFICATION:
//                    ds = new DialogueState(REQUEST_VERIFICATION);
//                    printState(this.next);
//
//                    this.systxt += "[MACHINE]: Is this the shape that you wanted? (y/n)\n";
//                    this.sysconf = BlocksConfiguration.fromPlannerRepresentation(plan.getPlan());
//                    this.next = PARSE_VERIFICATION;
//                    this.appendToHistory(ds, this.systxt);
//
//                    return; // quit and wait for new user input to be handled at verification parsing state
//
//                // handle user response to verification question
//                case PARSE_VERIFICATION:
//                    ds = new DialogueState(PARSE_VERIFICATION, text, null);
//                    printState(this.next);
//
//                    if (text.equalsIgnoreCase("yes") || text.startsWith("y")) {
//                        this.systxt = "[MACHINE]: Great!\n";
//                        this.next = REQUEST_DESCRIPTION;
//                        this.appendToHistory(ds, this.systxt);
//                        this.parse(""); // move to and execute 'request further description' state
//                        return;
//                    }
//
//                    return;
//
//                case REQUEST_CLARIFICATION:
//
//                case PARSE_CLARIFICATION:
//
//                case FINISHED:
//                    this.printState(this.next);
//                    this.clearSystemText();
//                    return; // quit until state machine is reset
//
//                case FAILURE:
//                    this.printState(this.next);
//                    this.clearSystemText();
//                    return; // quit until state machine is reset
//            }
//        }
//
//        /**
//         * Clears the system text.
//         */
//        private void clearSystemText() { this.systxt = ""; }
//
//        /**
//         * Prints the last seen state of the dialogue manager.
//         */
//        private void printState(edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State state) { System.out.println("\nDialogueManager::"+state); }
//
//        /**
//         * Returns the previous state of the dialogue manager.
//         * Returns null if only one state exists in the dialogue states.
//         * @return Dialogue manager previous state (null if does not exist)
//         */
//        private edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State previousState() {
//            if (this.history.size() >= 2) return this.history.get(this.history.size()-2).state;
//            else return null;
//        }
//
//        private void appendToHistory(DialogueState ds, String output) {
//            ds.output = output;
//            history.add(ds);
//        }
//
//        private void appendToAndEndHistory(DialogueState ds, String output, edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State end) {
//            this.appendToHistory(ds, output);
//            history.add(new DialogueState(end));
//        }
//
//        public void printHistory() {
//            for (DialogueState state : this.history) System.out.println(state);
//        }
//    }
//
//}
