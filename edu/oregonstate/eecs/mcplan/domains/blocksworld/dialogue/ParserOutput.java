package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import demo.blocksworld.GridCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Colin Graber on 9/8/16.
 */
public class ParserOutput {
    public static final int SUCCESS_CODE = 0;
    public static final int UKNOWN_VOCAB_CODE = 1;
    public static final int FAILURE_CODE = 2;

    private int statusCode;
    private List<String> parse;
    private String unconstrainedParse;
    private List<GridCoordinate> restricted = new ArrayList<>();
    private List<GridCoordinate> immobileBlocks = new ArrayList<>();
    private List<GridCoordinate> startingBlocks = new ArrayList<>();
    private List<String> unknownVocab;

    public ParserOutput(int statusCode, List<String> parse, List<String> unknownVocab) {
        this.statusCode = statusCode;
        this.parse = parse;
        this.unknownVocab = unknownVocab;
        if (parse == null || this.statusCode == FAILURE_CODE) {
            this.unconstrainedParse = null;
        } else {
            StringBuilder tempParse = new StringBuilder();
            for (String sentence : parse) {
                String[] temp = sentence.split("\\^");

                for (String pred : temp) {
                    if (pred.startsWith("immobile_block")) {
                        String[] info = pred.substring("immobile_block(".length(), pred.length() - 1).split(",");
                        try {
                            immobileBlocks.add(new GridCoordinate(Integer.parseInt(info[0]), Integer.parseInt(info[1])));
                        } catch (Exception e) {
                            System.out.println("FAILED TO GET VALUES FOR PREDICATE: "+pred);
                        }
                    } else if (pred.startsWith("starting_block")) {
                        String[] info = pred.substring("starting_block(".length(), pred.length() - 1).split(",");
                        try {
                            startingBlocks.add(new GridCoordinate(Integer.parseInt(info[0]), Integer.parseInt(info[1])));
                        } catch (Exception e) {
                            System.out.println("FAILED TO GET VALUES FOR PREDICATE: "+pred);
                        }
                    } else if (pred.startsWith("restricted")) {
                        String[] info = pred.substring("restricted(".length(), pred.length() - 1).split(",");
                        try {
                            restricted.add(new GridCoordinate(Integer.parseInt(info[0]), Integer.parseInt(info[1])));
                        } catch (Exception e) {
                            System.out.println("FAILED TO GET VALUES FOR PREDICATE: "+pred);
                        }
                    } else {
                        if (!tempParse.toString().isEmpty()) {
                            tempParse.append("^");
                        }
                        tempParse.append(pred);
                    }
                }
            }
            this.unconstrainedParse = tempParse.toString();
        }
    }

    public int getStatusCode() {
        return statusCode;
    }

    public List<String> getParse() {
        return parse;
    }

    public String getUnconstrainedParse() {
        return unconstrainedParse;
    }
    public List<GridCoordinate> getRestricted() {
        return restricted;
    }

    public List<GridCoordinate> getImmobileBlocks() {
        return immobileBlocks;
    }

    public List<GridCoordinate> getStartingBlocks() {
        return startingBlocks;
    }
    public List<String> getUnknownVocab() {
        return unknownVocab;
    }
}
