package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anjali on 5/1/17.
 */
public class MissingExample {
    protected int[][] gold;
    protected int count;
    protected List<String> utterances;


    public MissingExample(String[] input) {
        this.gold = new int[10][10];
        this.count = 0;
        this.utterances = new ArrayList<>();

        String line = input[0];
        int index = 0;
        while (line.charAt(0) != '=') {
            String[] tokens = line.trim().split("\\s+");
            for (int j = 0; j < tokens.length; j++) {
                if (tokens[j].equals("X")) {
                    this.gold[index][j] = 1;
                    this.count += 1;
                } else gold[index][j] = 0;
            }

            index++;
            line = input[index];
        }

        index++;

        while (index < input.length) {
            this.utterances.add(input[index].trim());
            index++;
        }
    }

    @Override
    public String toString() {
        String str = "Gold configuration:\n";
        for (int i = 0; i < this.gold.length; i++) {
            for (int j = 0; j < this.gold[i].length; j++)
                str += this.gold[i][j] + " ";
            str += "\n";
        }

        str += "Count: "+count+"\nUtterances:\n";
        for (String s : this.utterances) str += s+"\n";
        return str;
    }
}
