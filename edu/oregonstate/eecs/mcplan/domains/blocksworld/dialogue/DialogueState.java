package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import convert.Response;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State;

import java.util.List;


/**
 * Wrapper class that holds information about a state in a dialogue state machine
 * and associated values that were instantiated during that state.
 * Created by Anjali on 4/25/17.
 */
public class DialogueState {
    protected State state;          // dialogue state
    protected String input;         // text input
    protected String output;        // system output
    protected ParserOutput parse;   // parser output
    protected Response plan;        // planner output
    protected List<String> goal;    // planner goal

    /**
     * Constructs a dialogue state with given state, text input, system output, parser output, and planner output.
     * @param state Dialogue state
     * @param parse Parser output
     * @param plan Planner output
     */
    public DialogueState(State state, String input, String output, ParserOutput parse, Response plan, List<String> goal) {
        this.state   = state;
        this.input   = input;
        this.output  = output;
        this.parse   = parse;
        this.plan    = plan;
        this.goal    = goal;
    }

    /**
     * Constructs a dialogue state with null values for parser and planner outputs.
     * @param state Dialogue state
     */
    public DialogueState(State state) {
        this(state, null, null, null, null, null);
    }

    /**
     * Constructs a dialogue state with a given system output and null values for parser and planner outputs.
     * @param state Dialogue state
     * @param output System output
     */
    public DialogueState(State state, String input, String output) {
        this(state, input, output, null, null, null);
    }

    /**
     * Constructs a dialogue state with a null value for planner output.
     * @param state Dialogue state
     * @param parse Parser output
     */
    public DialogueState(State state, ParserOutput parse) {
        this(state, null, null, parse, null, null);
    }

    /**
     * Constructs a dialogue state with a null value for parser output.
     * @param state Dialogue state
     * @param plan Planner output
     */
    public DialogueState(State state, Response plan, List<String> goal) {
        this(state, null, null, null, plan, goal);
    }

    /**
     * Gets the dialogue state.
     * @return Dialogue state
     */
    public State getState() { return this.state; }

    /**
     * Gets the text input to the system at this state.
     * This value is null if no input was accepted at this state.
     * @return System input
     */
    public String getSystemInput() { return this.input; }

    /**
     * Gets the text output of the system at this state.
     * This value is null if no output was produced at this state.
     * @return System output
     */
    public String getSystemOutput() { return this.output; }

    /**
     * Gets the parser output at this dialogue state.
     * If this value is null when getState() is in the PARSE_DESCRIPTION or PARSE_CLARIFICATION states,
     * this indicates that a parser failure has occurred.
     * Otherwise, this value is null if there was nothing to be parsed at this state.
     * @return Parser output
     */
    public ParserOutput getParse() { return this.parse; }

    /**
     * Gets the planner output at this dialogue state.
     * This value is null if nothing was planned at this state.
     * @return Planner output
     */
    public Response getPlan() { return this.plan; }

    public String toString() {
        String s = "state: "+this.state+"\nsysinput: "+this.input+"\nsysoutput: "+this.output.trim()+"\ngoal: "+this.goal+"\n"+
                "parse: "+(this.parse != null ? this.parse.getParse() : "null")+"\n"+
                "plan: "+(this.plan != null ? this.plan.toString() : "null");
        return s;
    }
}
