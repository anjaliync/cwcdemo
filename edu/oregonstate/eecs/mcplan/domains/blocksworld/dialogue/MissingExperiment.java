package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import convert.BlocksPlanner;
import convert.Response;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;

import java.util.ArrayList;
import java.util.List;
import java.io.*;
/**
 * Created by sdan on 4/29/17.
 */
import convert.BlocksPlanner;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

public class MissingExperiment {
    public static void main(String args[]) {
        BlocksSemanticParser parser = new BlocksSemanticParser();
        BlocksPlanner planner = new BlocksPlanner();
        List<MissingExample> examples = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("experiment_2_data.txt"))) {
            String line;
            line = br.readLine();

            String example = "";
            while (line != null) {
                if (line.charAt(0) != '#') example += line+"\n";
                else {
                    examples.add(new MissingExample(example.split("\n")));
                    example = "";
                }
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int success = 0, failure = 0;
        for (int i = 0; i < examples.size(); i++) {
            System.out.println("-----------------------------------------------------------------------------");
            parser.reset();
            planner.reset();

            MissingExample example = examples.get(i);
            System.out.println("Example "+i+"\n"+example);

            ParserOutput po;
            Response plan = null;
            List<String> queries;
            boolean failed = false;

            try {
                for (int index = 0; index < example.utterances.size(); index++) {
                    po = parser.parseInput(example.utterances.get(index));
                    System.out.println("Parser output: "+String.join("^",po.getParse()));
                    plan = planner.probe(String.join("^", po.getParse()));
                    System.out.println(plan);

                    if (plan == null || (plan.getResponseFlag() == Response.PlannerFlag.COMPLETED && plan.getPlan().isEmpty())) {
                        System.out.println("FAILURE: Empty or null plan");
                        failed = true;
                        break;
                    }

                    if (plan.getResponseFlag() == Response.PlannerFlag.COMPLETED) continue;
                    else if (plan.getResponseFlag() != Response.PlannerFlag.MISSING) {
                        System.out.println("FAILURE: Unexpected planner flag");
                        failed = true;
                        break;
                    }

                    queries = plan.getMissing();

                    while (!queries.isEmpty()) {
                        // height + width = size
                        if (queries.size() >= 2 && queries.get(0).contains("dim") && queries.get(1).contains("dim") &&
                                queries.get(0).split("\\s+")[2].equals(queries.get(1).split("\\s+")[2]) &&
                                ((queries.get(0).split("\\s+")[3].equals("height)") && queries.get(1).split("\\s+")[3].equals("width)")) ||
                                        (queries.get(0).split("\\s+")[3].equals("width)") && queries.get(1).split("\\s+")[3].equals("height)")))) {
                            queries.remove(0);
                            queries.set(0, "(dim " + queries.get(0).split("\\s+")[1] + " " + queries.get(0).split("\\s+")[2] + " size)");
                        }

                        List<String> dims = new ArrayList<>();
                        List<String> rels = new ArrayList<>();

                        for (String query : queries) {
                            if (query.contains("Not-Connected")) rels.add(query);
                            else dims.add(query);
                        }

                        index++;
                        if (index == example.utterances.size()) {
                            System.out.println("FAILURE: No more clarifications provided");
                            failed = true;
                            break;
                        }

                        String query = (dims.isEmpty() ? rels.get(0) : dims.get(0));
                        String response = example.utterances.get(index);
                        if (!response.startsWith(":")) {
                            System.out.println("FAILURE: no clarification needed");
                            failed = true;
                            break;
                        }

                        if (query.startsWith("(dim")) {
                            parser.undoParses(1);
                            planner.undoGoals(1);
                        }

                        po = parser.parseClarification(response.replace(":",""), query);
                        System.out.println("Parser clarification output: " + String.join("^", po.getParse()));
                        plan = planner.probe(String.join("^", po.getParse()));
                        System.out.println(plan);
                        queries = plan.getMissing();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                failed = true;
                parser.reset();
                planner.reset();
            }

            if (plan == null) failed = true;

            if (!failed) {
                int X[] = new int[plan.getPlan().size()];
                int Y[] = new int[plan.getPlan().size()];

                for (int j = 0; j < plan.getPlan().size(); j++) {
                    Y[j] = plan.getPlan().get(j).charAt(plan.getPlan().get(j).length() - 8) - 48;
                    X[j] = plan.getPlan().get(j).charAt(plan.getPlan().get(j).length() - 4) - 48;
                }

                int flg = 0, kx, ky;
                for (kx = 0; kx < 10; kx++) {
                    for (int l = 0; l < 10; l++) {
                        if (example.gold[kx][l] == 1) {
                            flg = 1;
                            break;
                        }
                    }
                    if (flg == 1) break;
                }

                flg = 0;
                for (ky = 0; ky < 10; ky++) {
                    for (int l = 0; l < 10; l++) {
                        if (example.gold[l][ky] == 1) {
                            flg = 1;
                            break;
                        }
                    }
                    if (flg == 1) break;
                }

                int minx = Integer.MAX_VALUE, miny = Integer.MAX_VALUE;
                for (int l = 0; l < X.length; l++)
                    if (X[l] < minx) minx = X[l];

                for (int l = 0; l < Y.length; l++)
                    if (Y[l] < miny) miny = Y[l];

                int flag = 0;
                if (X.length != example.count) flag = 1;

                for (int k = 0; k < X.length; k++) {
                    if (example.gold[X[k] - minx + kx][Y[k] - miny + ky] != 1) {
                        flag = 1;
                        break;
                    }
                }

                if (flag != 0) failed = true;
            }

            if (failed) failure++;
            else success++;

            System.out.println("success: " + success + "\tfailure: " + failure);

        }
    }
}









