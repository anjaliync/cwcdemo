package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import convert.BlocksPlanner;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
/**
 * Created by sdan2 on 4/28/17.
 */
public class Experiment {


    public static void main(String args[]) {

        BlocksSemanticParser parser = new BlocksSemanticParser();
        BlocksPlanner planner = new BlocksPlanner();

        try (BufferedReader br = new BufferedReader(new FileReader("data.txt"))) {
            int[][] gold = new int[10][10];
            int success = 0, failure = 0;
            String input = "";
            int count = 0;
            int pos = 0;

            String line;
            line = br.readLine();
            while (line != null) {
                // end of configuration; reset all fields
                if(line.charAt(0) == '.') {
                    for (int i = 0; i < 10; i++)
                        for (int j = 0; j < 10; j++)
                            gold[i][j]=0;

                    input = "";
                    count = 0;
                    pos = 0;
                    line = br.readLine();
                }

                else {
                    while (line.charAt(0) != '=') {
                        //System.out.println(pos);
                        //System.out.println(line);
                        String [] split_line=line.trim().split(" ");
                        for (int j = 0; j < split_line.length; j++) {
                            if (split_line[j].equals("X")) {
                                gold[pos][j] = 1;
                                count += 1;
                            } else gold[pos][j] = 0;
                        }
                        pos++;
                        line = br.readLine();
                    }

                    System.out.println("Read gold configuration: ");
                    for (int i = 0; i < gold.length; i++) {
                        for (int j = 0; j < gold[i].length; j++)
                            System.out.print(gold[i][j]+" ");
                        System.out.println();
                    }


                    input = br.readLine();
                    line = br.readLine();

                    while(line.charAt(0)!='.'){
                        input += " "+line;
                        line = br.readLine();
                    }

                    List<String> plan;
                    try{
                        ParserOutput po = parser.parseInput(input);
                        System.out.println("Parser output: "+String.join("^",po.getParse()));
                        plan = planner.probe(String.join("^", po.getParse())).getPlan();
                        for (String p : plan) System.out.println(p);
                    }
                    catch (Exception e){
                        System.out.println("Exception during parse/plan");
                        e.printStackTrace();
                        failure++;
                        parser.reset();
                        planner.reset();
                        continue;
                    }

                    int X[] = new int[plan.size()];
                    int Y[] = new int[plan.size()];
                    for (int i = 0; i < plan.size(); i++) {
                        Y[i] = plan.get(i).charAt(plan.get(i).length() - 8)-48;
                        X[i] = plan.get(i).charAt(plan.get(i).length() - 4)-48;
                    }

//                    System.out.println("X[]:");
//                    for (int i = 0; i < X.length; i++) System.out.print(X[i]+" ");
//
//                    System.out.println("\n\nY[]:");
//                    for (int i = 0; i < Y.length; i++) System.out.print(Y[i]+" ");
//
//                    System.out.println();

                    // find closest column
                    int flg = 0, kx, ky;
                    for (kx = 0; kx < 10; kx++) {
                        for (int l = 0; l < 10; l++) {
                            if (gold[kx][l] == 1){
                                flg = 1;
                                break;
                            }
                        }
                        if (flg == 1) break;
                    }

                    // find closest row
                    flg = 0;
                    for (ky = 0; ky < 10; ky++) {
                        for (int l = 0; l < 10; l++){
                            if (gold[l][ky] == 1){
                                flg = 1;
                                break;
                            }
                        }
                        if (flg == 1) break;
                    }

                    int minx = Integer.MAX_VALUE, miny = Integer.MAX_VALUE;
                    for (int l = 0; l < X.length; l++)
                        if (X[l] < minx) minx = X[l];

                    for (int l = 0; l < Y.length; l++)
                        if (Y[l] < miny) miny = Y[l];

                    System.out.println("minx:"+minx+"\tminy:"+miny);

                    int flag = 0;
                    if (X.length != count) flag = 1;

                    for (int k = 0; k < X.length; k++) {
                        //System.out.println("Y: "+Y[k]);
                        if (gold[X[k]-minx+kx][Y[k]-miny+ky] != 1){
                            flag = 1;
                            break;
                        }
                    }

                    if (flag == 0) success++;
                    else failure++;

                    System.out.println("success"+success+"failure"+failure);
                    parser.reset();
                    planner.reset();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

