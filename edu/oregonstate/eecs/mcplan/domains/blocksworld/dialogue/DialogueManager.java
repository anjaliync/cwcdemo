package edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue;

import convert.BlocksPlanner;
import convert.Response;
import demo.blocksworld.BlocksConfiguration;
import demo.blocksworld.GridCoordinate;
import demo.blocksworld.InstructionGenerator;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksWorldUtilities;

import java.util.*;

import static convert.Response.PlannerFlag.*;
import static edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State.*;

/**
 * Dialogue manager for the GUI demo.
 * Created by Anjali on 4/24/17.
 */
public class DialogueManager {
    protected BlocksSemanticParser parser;         // semantic parser
    protected BlocksPlanner planner;               // planner
    protected InstructionGenerator generator;      // instruction generator

    protected List<DialogueState> history;         // history of dialogue states & information
    protected List<ParserOutput> parses;           // history of (successful) parses
    protected List<Integer> ordinals;              // ordinals of described shapes
    protected List<Response> plans;                // history of (successful) plans

    protected State next;                          // next state

    protected String systxt;                       // system output text
    protected BlocksConfiguration sysconf;         // system output configuration

    protected ParserOutput po;                     // parser output
    protected Set<GridCoordinate> restricted;      // restricted locations
    protected Set<GridCoordinate> immobile;        // immobile blocks
    protected Response plan;                       // planner output

    protected static Random rng = new Random();    // random number generator

    protected static final int ATTEMPT_LIMIT = 4;  // limit number of failed attempts before failing entirely
    protected int descriptionAttempts;             // number of description attempts
    protected int clarificationAttempts;           // number of clarification attempts
    protected int verificationAttempts;            // number of verification attempts

    protected static String predicateFile
            = "predicates.txt";
    protected static List<String> knownPredicates  // list of known predicates
            = BlocksWorldUtilities.readPredicateList(predicateFile);

    protected String missing;                      // missing information
    protected List<String> lastGoal;               // last known planner goal
    protected boolean clarificationFailed;         // whether the last clarification step failed
    protected boolean firstClarification;
    protected boolean wasParseFailure;

    protected static List<String> shapes           // shape types
            = new ArrayList<>(Arrays.asList("row", "column", "square", "rectangle"));
    protected static Set<String> learnedShapes;    // set of planner-learned shapes
    protected boolean userDefiningShape;           // is the user currently defining a shape?
    protected String newShape;                     // name of new shape

    // dialogue states
    public enum State {
        START,                        // start state
        REQUEST_DESCRIPTION,          // request a description
        PARSE_DESCRIPTION,            // parse a description
        PLAN,                         // plan
        REQUEST_CLARIFICATION,        // request a clarification
        PARSE_CLARIFICATION,          // parse a clarification
        REQUEST_VERIFICATION,         // request a verification
        PARSE_VERIFICATION,           // parse a verification
        REQUEST_NEW_SHAPE_DEFINITION, // request definition of new shape
        FINISH_NEW_SHAPE_DEFINITION,  // parse definition of new shape
        ADD_NEW_SHAPE_PARAMETER,      // add parameter to new shape definition
        FINISHED,                     // finished
        FAILURE                       // failure
    }

    /**
     * Constructs a dialogue manager.
     */
    public DialogueManager() {
        // initialize components
        this.parser    = new BlocksSemanticParser();
        this.planner   = new BlocksPlanner();
        this.generator = new InstructionGenerator();

        // initialize text and blocks configuration
        this.systxt  = "[MACHINE]: Please describe your configuration.\n";
        this.sysconf = null;

        // initialize lists of restricted locations and immobile blocks
        this.restricted = new HashSet<>();
        this.immobile   = new HashSet<>();

        // initialize histories
        this.history = new ArrayList<>();
        this.parses  = new ArrayList<>();
        this.plans   = new ArrayList<>();
        this.history.add(new DialogueState(START));

        // initialize the state machine
        this.next = START;

        // initialize number of attempts and shapes
        this.descriptionAttempts   = 0;
        this.clarificationAttempts = 0;
        this.verificationAttempts  = 0;

        this.clarificationFailed   = false;
        this.firstClarification    = true;
        this.wasParseFailure       = false;

        // initialize learned shape fields
        this.learnedShapes = this.planner.getLearnedShapes();
        this.userDefiningShape = false;
        this.newShape = null;

        // reset the currently processed missing information
        this.missing = null;
    }

    /**
     * Resets dialogue manager back to start state and clears
     * saved system text and blocks configuration. Also reset the
     * number of attempts used at each stage.
     */
    public void reset() {
        // reset text and blocks configuration
        this.systxt  = "[MACHINE]: Please describe your configuration.\n";
        this.sysconf = null;

        // reset lists of restricted locations and immobile blocks
        this.restricted = new HashSet<>();
        this.immobile   = new HashSet<>();

        // reset histories
        this.history = new ArrayList<>();
        this.parses  = new ArrayList<>();
        this.plans   = new ArrayList<>();
        this.history.add(new DialogueState(START));

        // initialize the state machine
        this.next = START;

        // reset number of attempts and shapes
        this.descriptionAttempts   = 0;
        this.clarificationAttempts = 0;
        this.verificationAttempts  = 0;

        this.clarificationFailed   = false;
        this.firstClarification    = true;
        this.wasParseFailure       = false;

        // reset learned shape fields
        this.learnedShapes = this.planner.getLearnedShapes();
        this.userDefiningShape = false;
        this.newShape = null;

        // reset the currently processed missing information
        this.missing = null;

        // reset the parser history
        this.parser.reset();

        // reset the planner history
        this.planner.reset();
    }

    /**
     * Gets the system text to output into the system text panel.
     * If null or empty, there is no text to be printed.
     * @return System text
     */
    public String getSystemText() { return this.systxt; }

    /**
     * Gets the system configuration to output into the test configuration panel.
     * If null, there is no planned configuration to be displayed.
     * @return System blocks configuration
     */
    public BlocksConfiguration getSystemBlocksConfiguration() { return this.sysconf; }

    /**
     * Returns whether or not the dialogue manager is in a 'finished' state.
     * @return True if finished; false otherwise
     */
    public boolean isFinished() { return this.history.get(this.history.size()-1).state == FINISHED; }

    /**
     * Returns whether or not the dialogue manager is in a 'failure' state.
     * @return True if failure; false otherwise
     */
    public boolean hasFailed()  { return this.history.get(this.history.size()-1).state == FAILURE; }

    /**
     * Parses an input string and takes action based on the current state and behavior defined by the
     * contained state machine.
     * @param text (Human) input string
     */
    public void parse(String text) {
        DialogueState ds;
        switch (this.next) {
            // request a (continuing) description from the user
            case REQUEST_DESCRIPTION:
                ds = new DialogueState(REQUEST_DESCRIPTION);
                this.printState(this.next);

                this.systxt += "[MACHINE]: Please continue to describe your configuration.\n"+
                        "Alternatively, type \"I'm finished\" when you're done with your description.\n";
                this.next = PARSE_DESCRIPTION;
                this.appendToHistory(ds, this.systxt);
                return; // quit and wait for new user input to be handled at the parsing state

            // start state
            case START:
                this.next = PARSE_DESCRIPTION;

            // parse user description
            case PARSE_DESCRIPTION:
                ds = new DialogueState(PARSE_DESCRIPTION, text, null);
                this.printState(this.next);

                // user indicates they are finished with their description
                if (isFinished(text)) {
                    if (this.userDefiningShape) {
                        this.next = FINISH_NEW_SHAPE_DEFINITION;
                        this.appendToHistory(ds);
                        this.parse("");
                        return;
                    }

                    this.systxt = "[MACHINE]: Complete!\n";
                    this.next = FINISHED;
                    this.appendToAndEndHistory(ds, this.systxt, FINISHED);
                    return; // quit until state machine is reset
                }

                // parse the input
                this.po = this.parser.parseInput(text);
                this.descriptionAttempts += 1;

                // set dialogue state fields
                ds.parse = this.po;

                // parse failure
                if (this.po == null) {
                    this.rewordOrQuit(ds, PARSE_DESCRIPTION, true, false, false);
                    return;
                }

                this.parses.add(po);
                this.appendToHistory(ds);
                this.next = PLAN;
                // when successful, falls through to the planning state below

            // planner
            case PLAN:
                ds = new DialogueState(PLAN);
                this.printState(this.next);
                this.clarificationFailed = false;

                String[] responses = {"Let me think...\n", "Thinking...\n", "Hmm...\n"};
                this.systxt = "[MACHINE]: "+chooseRandomResponse(responses);

                // attempt planning
                System.out.println("Planner past goal: "+this.planner.goal());
                System.out.println("Input to planner: "+String.join("^", BlocksWorldUtilities.normalizeSpatialRelConsistency(po.getParse())));

                try {
                    // fixme: use for debugging
//                    this.plan = this.planner.probe("poop(a)^height(a,2)^width(a,5)");
//                    this.plan = this.planner.probe(String.join("^", po.getParse()));
                    // fixme: this is a (hopefully) temporary fix for inconsistent spatial relations
                    this.plan = this.planner.probe(String.join("^", BlocksWorldUtilities.normalizeSpatialRelConsistency(po.getParse())));
                } catch (Exception e) { // planner exception
                    System.out.println("PLAN::Planner exception occurred");
                    e.printStackTrace();

                    this.plan = null;
                    this.plans.add(this.plan);
                    if (this.previousState() == PARSE_CLARIFICATION) this.clarificationFailed = true;
                    this.rewordOrQuit(ds, this.previousState(), true, false, this.clarificationFailed);
                    if (this.clarificationFailed) this.parse("");
                    return;
                }

                if (this.plan == null) {
                    System.out.println("PLAN::null response received from planner");
                    this.plans.add(this.plan);
                    if (this.previousState() == PARSE_CLARIFICATION) this.clarificationFailed = true;
                    this.rewordOrQuit(ds, this.previousState(), false, true, this.clarificationFailed);
                    if (this.clarificationFailed) this.parse("");
                    return;
                }

                ds.plan = this.plan;
                ds.goal = this.planner.goal();
                this.plans.add(this.plan);

                System.out.println("Planner new goal: "+this.planner.goal());
                System.out.println("RESPONSE::\n"+plan);

                // planning returned with 'finished' flag
                if (this.plan.getResponseFlag() == COMPLETED) {
                    // planning unexpectedly failed
                    if (this.plan.getPlan() == null || plan.getPlan().isEmpty()) {
                        this.clarificationFailed = this.previousState() == PARSE_CLARIFICATION;
                        this.rewordOrQuit(ds, this.previousState(), false, true, this.clarificationFailed);
                        if (this.clarificationFailed) this.parse("");
                        return;
                    }

                    this.missing = null;
                    this.descriptionAttempts = 0;
                    this.appendToHistory(ds);
                    this.next = REQUEST_VERIFICATION;
                    this.parse("");
                    return; // planning is finished and successful, so move to verification state
                }

                // planning returned with 'missing information' flag
                else if (this.plan.getResponseFlag() == MISSING) {
                    this.descriptionAttempts = 0;
                    this.appendToHistory(ds);
                    this.next = REQUEST_CLARIFICATION;
                    this.parse("");
                    return;
                }

                // planning returned with 'unknown shape' flag
                else if (!userDefiningShape && this.previousState() != PARSE_CLARIFICATION && this.plan.getResponseFlag() == UNKNOWN) {
                    // currently only handles:
                    // 1) there is only one unknown shape
                    // 2) the shape is the only shape being described for the given configuration
                    // todo, eventually: be able to handle new shapes with spatial relations to other shapes; save and reset the parser/planner state appropriately
                    if (hasMultipleUnknownShapes(this.plan.getOther()) || this.parses.size() > 1) {
                        this.rewordOrQuit(ds, this.previousState(), false, true, false);
                        return;
                    }

                    this.newShape = this.planner.getShapeBeingLearned().get(0).split("[(]")[0];
                    this.descriptionAttempts = 0;
                    this.removeParsesAndPlans(1);
                    this.appendToHistory(ds);
                    this.next = REQUEST_NEW_SHAPE_DEFINITION;
                    this.parse("");
                    return;
                    // when successful, falls through to the request new definition state below
                }

                // TODO: implement suggestion flag
                else if (this.plan.getResponseFlag() == SUGGESTION) {
                    this.rewordOrQuit(ds, this.previousState(), false, true, false);
                    if (this.clarificationFailed) this.parse("");
                    return;
                }

                else if (this.plan.getResponseFlag() == DUPLICATE_IDENTIFIER) {
                    if (this.previousState() == PARSE_CLARIFICATION) this.clarificationFailed = true;
                    this.rewordOrQuit(ds, this.previousState(), false, true, this.clarificationFailed);
                    if (this.clarificationFailed) this.parse("");
                    return;
                }

                else {
                    if (this.previousState() == PARSE_CLARIFICATION) this.clarificationFailed = true;
                    this.rewordOrQuit(ds, this.previousState(), false, true, this.clarificationFailed);
                    if (this.clarificationFailed) this.parse("");
                    return;
                }

            case REQUEST_NEW_SHAPE_DEFINITION:
                ds = new DialogueState(REQUEST_NEW_SHAPE_DEFINITION);
                this.printState(this.next);

                if (!userDefiningShape)
                    this.systxt = "[MACHINE]: Sorry, I don't know what '"+this.newShape+"' is.\n"+
                            "[MACHINE]: Can you describe how to build the "+this.newShape+" for me using rows, columns, squares, and rectangles?\n";
                else
                    this.systxt = "[MACHINE]: Please continue to define '"+this.newShape +"'.\n"+
                            "Alternatively, type \"I'm finished\" when you're done with your definition.\n";

                this.userDefiningShape = true;
                this.next = PARSE_DESCRIPTION;
                this.appendToHistory(ds, this.systxt);
                return;

            case FINISH_NEW_SHAPE_DEFINITION:
                ds = new DialogueState(FINISH_NEW_SHAPE_DEFINITION, text, null);
                this.printState(this.next);

                //fixme?
                System.out.println(this.planner.getShapeBeingLearned());
                if (this.planner.getShapeBeingLearned().size() <= 1) {
                    this.systxt = "[MACHINE]: "+chooseRandomConfirmationResponse()+
                            "[MACHINE]: One last thing -- could you tell me the dimensions of the "+this.newShape+" you just built?\n";
                    this.next = ADD_NEW_SHAPE_PARAMETER;
                    this.appendToHistory(ds, this.systxt);
                    return;
                }

                this.planner.learnShape();
                this.systxt = "[MACHINE]: "+chooseRandomConfirmationResponse()+
                        "[MACHINE]: I've now learned what a "+this.newShape+" is.\n";
                this.addShape(this.newShape);
                this.learnedShapes = this.planner.getLearnedShapes();
                this.newShape = null;
                this.next = FINISHED;
                this.appendToAndEndHistory(ds, this.systxt, FINISHED);
                return; // quit until state machine is reset

            case ADD_NEW_SHAPE_PARAMETER:
                ds = new DialogueState(ADD_NEW_SHAPE_PARAMETER, text, null);
                this.printState(this.next);

                String query = "(dim "+this.newShape+" a size)";
                this.po = this.parser.parseClarification(text, query);
                this.planner.replaceShapeKey(this.po.getParse().get(0));
                this.next = FINISH_NEW_SHAPE_DEFINITION;
                this.appendToHistory(ds);
                this.parse("");
                return;

            // request user to verify correctness of planner output
            case REQUEST_VERIFICATION:
                ds = new DialogueState(REQUEST_VERIFICATION);
                printState(this.next);

                this.clarificationAttempts = 0;
                this.systxt += "[MACHINE]: Is this okay so far? (y/n)\n";

                // add restricted locations and immobile blocks to running sets
                this.restricted.addAll(po.getRestricted());
                this.immobile.addAll(po.getImmobileBlocks());

                // update the system configuration
                this.sysconf = BlocksConfiguration.fromPlannerRepresentation(plan.getPlan());
                this.sysconf.setRestrictedLocations(new ArrayList<>(this.restricted));
                this.sysconf.setImmobileLocations(new ArrayList<>(this.immobile));

                this.next = PARSE_VERIFICATION;
                this.appendToHistory(ds, this.systxt);
                return; // quit and wait for new user input to be handled at verification parsing state

            // handle user response to verification question
            case PARSE_VERIFICATION:
                ds = new DialogueState(PARSE_VERIFICATION, text, null);
                printState(this.next);

                if (text.equalsIgnoreCase("yes") || text.startsWith("y")) {
                    this.verificationAttempts = 0;

                    // force quit if user asked to build a 'new' (complex) shape
                    if (describesLearnedShape(this.getFirstParse())) {
                        this.systxt = "[MACHINE]: Complete!\n";
                        this.next = FINISHED;
                        this.appendToAndEndHistory(ds, this.systxt, FINISHED);
                        return; // quit until state machine is reset
                    }

                    this.systxt = "[MACHINE]: "+chooseRandomConfirmationResponse();
                    this.next = (userDefiningShape ? REQUEST_NEW_SHAPE_DEFINITION : REQUEST_DESCRIPTION);
                    this.appendToHistory(ds, this.systxt);
                    this.parse(""); // move to and execute 'request further description' state
                    return;
                }

                else if (text.equalsIgnoreCase("no") || text.startsWith("n")) {
                    this.verificationAttempts += 1;
                    System.out.println("Number of verification attempts: "+this.verificationAttempts);

                    if (this.verificationAttempts <= ATTEMPT_LIMIT) {
                        this.systxt = "[MACHINE]: Sorry to hear that.\n" +
                                "[MACHINE]: Could you try rewording your last description from scratch?\n";
                        this.removeShapeFromHistory(ds);

                        // update the system configuration
                        if (this.planner.goal().isEmpty())
                            this.sysconf = new BlocksConfiguration();
                        else {
                            Response previous = this.planner.probe("");
                            this.sysconf = BlocksConfiguration.fromPlannerRepresentation(previous.getPlan());
                        }

                        this.sysconf.setRestrictedLocations(new ArrayList<>());
                        this.sysconf.setImmobileLocations(new ArrayList<>());

                        this.next = PARSE_DESCRIPTION;
                        this.appendToHistory(ds, this.systxt);
                        return;
                    }
                    else {
                        // quit if too many description attempts have been made
                        this.systxt = "[MACHINE]: Sorry, attempt limit exceeded.\n";
                        this.appendToAndEndHistory(ds, systxt, FAILURE);
                        this.next = FAILURE;
                        return; // quit until state machine is reset
                    }
                }

                else {
                    this.systxt = "[MACHINE]: "+chooseRandomMisunderstandingResponse()+
                        "[MACHINE]: Is this okay so far? (y/n)\n";
                    this.next = PARSE_VERIFICATION;
                    this.appendToHistory(ds, this.systxt);
                    return;
                }

            case REQUEST_CLARIFICATION:
                ds = new DialogueState(REQUEST_CLARIFICATION);
                printState(this.next);

                this.clarificationAttempts += 1;
                this.firstClarification = (this.clarificationAttempts < 2);
                reformatMissingQuery(this.plan.getMissing());
                System.out.println("REQUEST_CLARIFICATION::Previously requested: "+this.missing+"; attempts: "+this.clarificationAttempts);
                System.out.println("REQUEST_CLARIFICATION::Previous clarification failed? "+this.clarificationFailed);

                //todo: check that the following isn't actually doing one extra removal of parse/plan (successful parse and plan, but the missing information still exists)
                //todo: if only one of height and width are missing, and the shape is not row or column, augment clarification_heuristic to take known arg "width=" or "height=" and make new input based on that. still reset the parser/planner.
                //todo: spatial-rel vs coarse spatial???
                // the missing information at the last step was not resolved -- remove last parse/plan and request again
                List<String> missing = this.plan.getMissing();
                if (this.clarificationFailed || (this.missing != null && missing.contains(this.missing))) {
                    this.systxt = "[MACHINE]: "+chooseRandomMisunderstandingResponse();
                    this.clarificationFailed = true;
                    if (!this.wasParseFailure) {
                        System.out.println("Removing parses and plans at REQUEST_CLARIFICATION");
                        this.removeParsesAndPlans(1);
                    }
                }

                if (!this.clarificationFailed) {
                    this.missing = chooseMissingQuery(missing);
                    this.clarificationAttempts = 1;
                    this.firstClarification = true;
                }

                System.out.println("Missing query: "+this.missing);

                if (this.clarificationAttempts == 1) this.systxt = "";
                this.lastGoal = this.planner.goal();
                this.systxt  += (this.clarificationAttempts == 1 ? "[MACHINE]: I seem to be missing some information...\n" : "")+
                        this.generateClarificationQuestion(this.missing);

                this.next = PARSE_CLARIFICATION;
                this.appendToHistory(ds, this.systxt);
                return;

            case PARSE_CLARIFICATION:
                ds = new DialogueState(PARSE_CLARIFICATION);
                printState(this.next);
                this.wasParseFailure = false;
                this.systxt = "";

                System.out.println("Requesting missing: "+this.missing);

                //fixme: debug reset logic for errors and for separate width-height queries
                if (!this.missing.contains("Not-Connected") && this.firstClarification) {
                    System.out.println("Removing parses and plans at PARSE_CLARIFICATION");
                    this.removeParsesAndPlans(1);
                }

                this.po = this.parser.parseClarification(text, this.missing);

                // parse failure
                if (this.po == null || this.po.getParse() == null) {
                    this.clarificationFailed = true;
                    this.wasParseFailure = true;
                    this.rewordOrQuit(ds, REQUEST_CLARIFICATION, true, false, true);
                    this.removeParses(1);
                    this.parse("");
                    return;
                }

                // set dialogue state fields
                ds.parse = this.po;
                this.parses.add(this.po);

                // plan the clarification
                this.appendToHistory(ds);
                this.next = PLAN;
                this.parse("");
                return;

            case FINISHED:
                this.printState(this.next);
                return; // quit until state machine is reset

            case FAILURE:
                this.printState(this.next);
                return; // quit until state machine is reset
        }
    }

    //fixme!!! (eventually): written with the assumption that we are composing no more than two shapes. this is super restrictive and will break with more shapes
    private String generateClarificationQuestion(String missing) {
        HashMap<String, String> built = this.getBuiltShapes();
        String[] responses = {"Can you tell me about ", "What is ", "What's ", "Can you describe "};
        String[] restricted = {"Can you tell me ", "Can you describe ", "Can you explain "};

        String var = missing.split("\\s+")[2].replace(")", "");
        String first = null, second = null;
        for (String identifier : built.keySet()) {
            if (identifier.equals(var)) second = built.get(identifier);
            else first = built.get(identifier);
        }

        if (missing.contains("dim")) {
            String shape = missing.split("\\s+")[1];
            String dim = missing.split("\\s+")[3].replace(")", "");

            if (this.clarificationAttempts < 2)
                return "[MACHINE]: " + chooseRandomResponse(responses) + "its " + dim + "?\n";
            else {
                if (first != null && first.equals(second))
                    return "[MACHINE]: " + chooseRandomResponse(responses) + "the " + dim + " of the second " + shape +
                            (this.clarificationAttempts < 3 ? "?\n" : " that you're building?\n");

                String example = (dim.equals("size") ? "3 by 4" : "3");
                return "[MACHINE]: " + chooseRandomResponse(responses) + "the " + dim + " of the " + shape +
                        (this.clarificationAttempts < 3 ? "?\n" : " that you're building?\n") +
                        (this.clarificationAttempts > 3 ? "[MACHINE]: For example, you could say, 'The " + dim + " of the " + shape + " is " + example + ".'\n" : "");
            }
        }

        else if (missing.contains("Not-Connected") && built.size() <= 2) {
            if (first.equals(second)) {
                if (this.clarificationAttempts < 3)
                    return "[MACHINE]: " + chooseRandomResponse(responses) + "the second " + second + "'s location relative to the first" +
                            (this.clarificationAttempts < 2 ? "?\n" : " " + first + "'s location?\n");
                else if (this.clarificationAttempts < 4)
                    return "[MACHINE]: " + chooseRandomResponse(restricted) + "how any one block in the second " + second + " aligns to blocks in the first " +
                            first + "?\n";
                return "[MACHINE]: " + chooseRandomResponse(restricted) + "how any one block in the second " + second + " aligns to any one block in the first " +
                        first + "?\n";
            }

            if (this.clarificationAttempts < 2)
                return "[MACHINE]: " + chooseRandomResponse(responses) + "the " + second + "'s location relative to the " + first + "?\n";
            else if (this.clarificationAttempts < 4)
                return "[MACHINE]: " + chooseRandomResponse(restricted) + "how any one block in the " + second + " aligns to " +
                        (this.clarificationAttempts > 2 ? "any one block" : "blocks") + " in the " + first + "?\n";
            else {
                String[] directions = {"bottom left", "bottom right", "upper left", "upper right"};
                String[] colends = {"top end", "bottom end"};
                String[] rowends = {"left end", "right end"};
                String firstEx = (first.equals("row") ? chooseRandomResponse(rowends) : (first.equals("column") ? chooseRandomResponse(colends) : chooseRandomResponse(directions) + " corner"));
                String secondEx = (second.equals("row") ? chooseRandomResponse(rowends) : (second.equals("column") ? chooseRandomResponse(colends) : chooseRandomResponse(directions) + " corner"));
                return "[MACHINE]: " + chooseRandomResponse(restricted) + "any one block in the " + second + " aligns to blocks in the " + first + "?\n" +
                        "[MACHINE]: For example, you could say, 'Ensure that the " + secondEx + " of the " + second + " is next to the " + firstEx + " of the " + first + ".'\n";
            }
        }

        else return missing;
    }

    private HashMap<String, String> getBuiltShapes() {
        String[] sarr = {"row", "column", "square", "rectangle"};
        List<String> shapes = Arrays.asList(sarr);
        int[] counts = {0, 0, 0, 0};

        HashMap<String, String> builtShapes = new HashMap<>();
        for (String increment : this.planner.goal()) {
            for (String predicate : increment.split("\\^")) {
                for (String shape : shapes) {
                    if (predicate.startsWith(shape)) {
                        counts[shapes.indexOf(shape)]++;
                        builtShapes.put(predicate.split("[(]")[1].replace(")",""), shape);
                    }
                }
            }
        }

        return builtShapes;
    }

    /**
     * Clears the system text.
     */
    private void clearSystemText() { this.systxt = ""; }

    /**
     * Prints the last seen state of the dialogue manager.
     */
    private void printState(State state) {
        System.out.println("\nDialogueManager::"+state);
        System.out.println("Current planner goal: "+this.planner.goal());
    }

    /**
     * Returns the previous state of the dialogue manager.
     * Returns null if only one state exists in the dialogue states.
     * @return Dialogue manager previous state (null if does not exist)
     */
    public State previousState() {
        if (this.history.size() >= 1) return this.history.get(this.history.size()-1).state;
        else return null;
    }

    /**
     * Asks for a rewording if under the attempt limit; quits otherwise.
     * Defines the next state then returns to wait for continued user input.
     * @param ds Dialogue state
     * @param next Next state
     */
    private void rewordOrQuit(DialogueState ds, State next, boolean parseFailure, boolean planFailure, boolean clarification) {
        System.out.println("\nInitiating reword or quit...");
        System.out.println("next: "+next+"  parseFailure: "+parseFailure+"  planFailure: "+planFailure+"  clarification: "+clarification);
        int attempts = (next == PARSE_DESCRIPTION ? this.descriptionAttempts : this.clarificationAttempts);

        // ask for a rewording if under the attempt limit
        if (attempts < ATTEMPT_LIMIT) {
            System.out.println("Next state: "+next+"; Number of attempts: "+attempts);
            if (!planFailure) this.systxt = "[MACHINE]: "+chooseRandomMisunderstandingResponse()+
                    (next == PARSE_DESCRIPTION ? "[MACHINE]: "+chooseRandomRewordingResponse() : "\n");
            else this.systxt = "[MACHINE]: Unfortunately, I am not able to build that. "+
                    (next == PARSE_DESCRIPTION ? "[MACHINE]: "+chooseRandomRewordingResponse() : "\n");
            if (!parseFailure && !clarification) this.removeParsesAndPlans(1);
            this.appendToHistory(ds, this.systxt);
            if (next == PARSE_CLARIFICATION) this.next = REQUEST_CLARIFICATION;
            else this.next = next;
            return; // quit and wait for new user input to be handled at the parsing state
        }

        // quit if too many description attempts have been made
        this.systxt = "[MACHINE]: Sorry, attempt limit exceeded.\n";
        if (!parseFailure && !clarification) this.removeParsesAndPlans(1);
        this.appendToAndEndHistory(ds, systxt, FAILURE);
        this.next = FAILURE;
        return; // quit until state machine is reset
    }

    private static boolean describesShape(String parse) {
        if (parse.isEmpty()) return false;
        return (shapes.contains(parse.split("[(]")[0]));
    }

    private static boolean describesLearnedShape(String parse) {
        if (parse.isEmpty()) return false;
        return (learnedShapes.contains(parse.split("[(]")[0]));
    }

    private static boolean describesUnknownShape(String parse) {
        return !knownPredicates.contains(parse.split("[(]")[0]);
    }

    private static boolean hasMultipleUnknownShapes(List<String> parse) {
        if (parse.size() < 1 || (parse.size() == 1 && parse.get(0).split("\\^").length <= 1)) return false;

        int unknown = 0;
        for (String increment : parse) {
            String[] predicates = increment.split("\\^");
            for (String predicate : predicates) {
                if (describesUnknownShape(predicate))
                    unknown++;
                if (unknown > 1) return true;
            }
        }

        return false;
    }

    private void addShape(String name) {
        this.shapes.add(name);
    }

    private String getFirstParse() {
        return this.parses.get(0).getParse().get(0);
    }

    private void removeShapeFromHistory(DialogueState ds) {
        System.out.println("\nRemoving a shape from the history...");

        // count the number of parses and plans that need to be removed
        // by identifying the most recent parse containing a shape predicate
        int removed = 0;
        boolean stop = false;
        String leftover = "";
        for (int i = this.parses.size()-1; i >= 0; i--) {
            ParserOutput parse = this.parses.get(i);
            System.out.println("Parse "+(i)+": "+parse.getParse());

            for (int j = 0; j < parse.getParse().size(); j++) {
                if (describesShape(parse.getParse().get(j))) {
                    leftover = String.join("^", parse.getParse().subList(0, j));
                    stop = true;
                    removed++;
                }
            }

            if (stop) break;
            removed++;
        }

        System.out.println("Removing "+removed+" parses, with leftover parse: "+leftover);
//        System.out.println("Old immobile: "+this.immobile+", Old restricted: "+this.restricted);
        for (int i = this.parses.size()-1; i > this.parses.size()-1-removed; i--) {
            this.immobile.removeAll(this.parses.get(i).getImmobileBlocks());
            this.restricted.removeAll(this.parses.get(i).getRestricted());
        }
//        System.out.println("New immobile: "+this.immobile+", New restricted: "+this.restricted);

        this.removeParsesAndPlans(removed);

        if (!leftover.isEmpty()) {
            ParserOutput parse = this.parser.parseInput(leftover);
            this.immobile.addAll(parse.getImmobileBlocks());
            this.restricted.addAll(parse.getRestricted());
            this.parses.add(parse);
            this.plans.add(this.planner.probe(leftover));
        }

        if (ds != null) ds.goal = this.planner.goal();
    }

    private void removeParsesAndPlans(int n) {
        System.out.println("\nRemoving "+n+" parses and plans.");
        this.parses.subList(this.parses.size()-n, this.parses.size()).clear();
        this.plans.subList(this.plans.size()-n, this.plans.size()).clear();

        this.parser.undoParses(n);
        this.planner.undoGoals(n);
    }

    private void removeParses(int n) {
        System.out.println("\nRemoving "+n+" parses.");
        this.parser.undoParses(n);
    }

    private void appendToHistory(DialogueState ds) {
        this.appendToHistory(ds, null);
    }

    /**
     * Appends given dialogue state to the dialogue history with given system output string.
     * @param ds Dialogue state
     * @param output System output string
     */
    private void appendToHistory(DialogueState ds, String output) {
        ds.output = output;
        history.add(ds);
    }

    /**
     * Appends given dialogue state to the dialogue history with given system output string, then appends
     * a finished state (FINISHED or FAILURE) to the dialogue history.
     * @param ds Dialogue state
     * @param output System output string
     * @param end Finished state (FINISHED or FAILURE)
     */
    private void appendToAndEndHistory(DialogueState ds, String output, State end) {
        this.appendToHistory(ds, output);
        history.add(new DialogueState(end));
    }

    private static String chooseRandomResponse(String[] responses) {
        return responses[rng.nextInt(responses.length)];
    }

    private static String chooseRandomMisunderstandingResponse() {
        String[] misunderstandings = {"I'm sorry, I seem to have misunderstood you.\n", "Sorry, I couldn't understand your response.\n", "Apologies, but that didn't make sense to me.\n"};
        return chooseRandomResponse(misunderstandings);
    }

    private static String chooseRandomConfirmationResponse() {
        String[] confirmations = {"Great!\n", "Awesome!\n", "Cool!\n"};
        return chooseRandomResponse(confirmations);
    }

    private static String chooseRandomRewordingResponse() {
        String[] rewordings = {"Could you reword your request?\n", "Could you try something else?\n", "Can you explain it differently?\n"};
        return chooseRandomResponse(rewordings);
    }

    private static boolean isFinished(String response) {
        response = response.toLowerCase();
        return response.contains("finished") || response.contains("done");
    }

    private static void reformatMissingQuery(List<String> missing) {
        // height + width = size
        if (missing.size() >= 2 && missing.get(0).contains("dim") && missing.get(1).contains("dim") &&
                missing.get(0).split("\\s+")[2].equals(missing.get(1).split("\\s+")[2]) &&
                ((missing.get(0).split("\\s+")[3].equals("height)") && missing.get(1).split("\\s+")[3].equals("width)")) ||
                        (missing.get(0).split("\\s+")[3].equals("width)") && missing.get(1).split("\\s+")[3].equals("height)")))) {
            missing.remove(0);
            missing.set(0, "(dim " + missing.get(0).split("\\s+")[1] + " " + missing.get(0).split("\\s+")[2] + " size)");
        }

        // todo!!!: do same for rectangle single param case
//        else if (missing.get(0))
    }

    private static String chooseMissingQuery(List<String> missing) {
        if (missing.isEmpty()) return null;

        List<String> dims = new ArrayList<>();
        List<String> rels = new ArrayList<>();

        for (String query : missing) {
            if (query.contains("Not-Connected")) rels.add(query);
            else dims.add(query);
        }

        return (dims.isEmpty() ? rels.get(rels.size()-1) : dims.get(0));
    }

    public void clearLearnedShapes() {
        this.planner.clearLearnedShapes();
    }

    public State nextState() { return this.next; }

    /**
     * Prints the dialogue history.
     */
    public void printHistory() {
        System.out.println("==============================================\n[ DIALOGUE HISTORY ]\n");
        for (int i = 0; i < this.history.size(); i++) {
            DialogueState state = this.history.get(i);
            System.out.println("("+i+")\n"+state+"\n");
        }
        System.out.println("==============================================\n");
    }
}
