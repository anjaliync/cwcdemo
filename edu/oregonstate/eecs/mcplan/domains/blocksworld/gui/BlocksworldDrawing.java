package edu.oregonstate.eecs.mcplan.domains.blocksworld.gui;

import demo.blocksworld.BlocksConfiguration;
import demo.blocksworld.GridCoordinate;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Colin Graber on 9/1/16.
 */
public class BlocksworldDrawing extends JPanel {

    private BlocksConfiguration config;
    private int type = 0;
    private java.util.List<Integer> idMap = null;
    private boolean withGrid;

    public static int BLANK_BLOCKS = 0;
    public static int NUMBER_BLOCKS = 1;
    public static int LOGO_BLOCKS = 2;

    private static int GRID_SIZE = 10;

    public BlocksworldDrawing(boolean withGrid) {
        this.withGrid = withGrid;
    }

    @Override
    public void paintComponent(Graphics g) {
        BlockImages bi = BlockImagesLoader.loadedImages;
        if (bi == null) {
            g.drawString("Waiting for block images to load...", 100, 100);
            return;
        }
        g.clearRect(0, 0, getWidth(), getHeight());

        int dim = bi.getDimension();
        if (withGrid) {
            for (int x = 0; x < GRID_SIZE+1; x++) {
                for (int y = 0; y < GRID_SIZE+1; y++) {
                    if (x == 0 && y == 0) {
                        continue;
                    } else if (x == 0) {
                        g.drawImage(bi.getGridIndexImage(y-1), dim*x, dim*y, this);
                    } else if (y == 0) {
                        g.drawImage(bi.getGridIndexImage(x-1), dim*x, dim*y, this);
                    } else {
                        g.drawImage(bi.getGridTileImage(), dim * x, dim * y, this);
                    }
                }
            }
        }
        if (config != null) {
            if (withGrid) {
                //Constraint drawing
                if (idMap == null) {
                    for (GridCoordinate coord : config.getRestrictedLocations()) {
                        g.drawImage(bi.getRestrictedImage(), dim * (coord.getX()+1), dim * (coord.getY()+1), this);
                    }
                }
                for (GridCoordinate coord : config.getMovableLocations()) {
                    Image draw;
                    if (type == LOGO_BLOCKS) {
                        if (idMap != null) {
                            draw = bi.getLogoImage(idMap.get(coord.getID()-1));
                        } else {
                            draw = bi.getLogoImage(coord.getID());
                        }
                    } else {
                        draw = bi.getBlankBlockImage();
                    }
                    g.drawImage(draw, dim * (coord.getX()+1), dim * (coord.getY()+1), this);
                }
                for (GridCoordinate coord : config.getImmobileLocations()) {
                    Image draw;
                    if (type == LOGO_BLOCKS) {
                        if (idMap != null) {
                            draw = bi.getLogoImage(idMap.get(coord.getID()-1));
                        } else {
                            draw = bi.getLogoImage(coord.getID());
                        }
                    } else {
                        draw = bi.getImmobileBlockImage();
                    }
                    g.drawImage(draw, dim * (coord.getX()+1), dim * (coord.getY()+1), this);
                }
            }
            //Config Drawing

            for (GridCoordinate coord : config.getBlockLocations()) {
                if (type == BLANK_BLOCKS) {
                    g.drawImage(bi.getBlankBlockImage(), dim * (coord.getX()+1), dim * (coord.getY()+1), this);
                } else if (type == NUMBER_BLOCKS) {
                    g.drawImage(bi.getNumberImage(coord.getID()), dim*(coord.getX()+1), dim*(coord.getY()+1), this);
                } else if (type == LOGO_BLOCKS) {
                    g.drawImage(bi.getLogoImage(coord.getID()), dim*(coord.getX()+1), dim*(coord.getY()+1), this);
                }
            }

        }
    }

    public void setBlocksConfiguration(BlocksConfiguration newConfig) {
        config = newConfig;
        System.out.println("SETTING THE CONFIG");
        repaint();
    }

    public void setBlocksConfiguration(BlocksConfiguration newConfig, int type) {
        config = newConfig;
        this.type = type;
        repaint();
    }

    public void setBlocksConfiguration(BlocksConfiguration newConfig, int type, java.util.List<Integer> idMap) {
        config = newConfig;
        this.type = type;
        this.idMap = idMap;
        repaint();
    }
}
