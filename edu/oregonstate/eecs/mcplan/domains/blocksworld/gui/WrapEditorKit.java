package edu.oregonstate.eecs.mcplan.domains.blocksworld.gui;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.ViewFactory;

/**
 * Created by Colin Graber on 9/12/16.
 */
public class WrapEditorKit extends StyledEditorKit {
    ViewFactory defaultFactory=new WrapColumnFactory();
    public ViewFactory getViewFactory() {
        return defaultFactory;
    }
}
