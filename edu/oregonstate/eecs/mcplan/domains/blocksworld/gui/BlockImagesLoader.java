package edu.oregonstate.eecs.mcplan.domains.blocksworld.gui;

import java.util.List;
import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Colin Graber on 9/1/16.
 */
public class BlockImagesLoader implements Runnable {
    // base image directory
    protected static final String IMAGE_DIR = "assets/blocksworld/";

    // number blocks
    protected static final String NUMBERS_DIR = IMAGE_DIR + "numbers/";
    protected static final String SMALL_NUMBERS_DIR = IMAGE_DIR + "numbers/";

    // grid indices
    protected static final String GRID_IND_DIR = IMAGE_DIR + "grid_coordinates/";
    protected static final String SMALL_GRID_IND_DIR = IMAGE_DIR + "grid_coordinates_small/";

    // logo blocks
    protected static final String LOGO_DIR = IMAGE_DIR + "logos/";
    protected static final String SMALL_LOGO_DIR = IMAGE_DIR + "logos_small/";

    // blank blocks
    protected static final String BLOCK_IMG = "block_larger.png";
    protected static final String SMALL_BLOCK_IMG = "block.png";

    // tiles
    protected static final String TILE_IMG = "grid_tile_larger.png";
    protected static final String SMALL_TILE_IMG = "grid_tile.png";

    // immobile blocks
    protected static final String IMMOBILE_IMG = "immobile_larger.png";
    protected static final String SMALL_IMMOBILE_IMG = "immobile.png";

    // restricted cells
    protected static final String RESTRICTED_IMG = "restricted_larger.png";
    protected static final String SMALL_RESTRICTED_IMG = "restricted.png";

    // loaded images
    protected static BlockImages loadedImages = null;

    protected boolean readyStatus = false;
    protected Component peer = null;
    protected Thread thread = null;

    public BlockImagesLoader(Component peer) {
        this.peer = peer;
    }

    public static BlockImages loadBlocks(Component peer) {
        if (loadedImages != null)
            return loadedImages;

        BlockImagesLoader bil = new BlockImagesLoader(peer);
        bil.start();
        System.out.println("Starting the Block Images loader...");

        while (!bil.ready()) {
            try {
                System.out.println("Waiting for Block Images loader...");
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                System.err.println("BlockImagesLoader::loadBlocks(). Unable to completely load blocks");
                break;
            }
        }

        loadedImages = bil.getLoadedBlockImages();
        System.out.println("Images are loaded.");
        return loadedImages;
    }

    synchronized BlockImages getLoadedBlockImages() {
        return loadedImages;
    }

    public boolean ready() { return readyStatus; }

    public void run() {
        MediaTracker mt = new MediaTracker(peer);
        BlockImages  bi = new BlockImages();
        int idx = 1;

        try {
            File f;

            // blank blocks
            f = new File(IMAGE_DIR + SMALL_BLOCK_IMG);
            URL url = f.toURI().toURL();
            Image img = Toolkit.getDefaultToolkit().getImage(url);
            mt.addImage(img, idx++);
            bi.setBlankBlockImage(img);

            // tiles
            f = new File(IMAGE_DIR + SMALL_TILE_IMG);
            url = f.toURI().toURL();
            img = Toolkit.getDefaultToolkit().getImage(url);
            mt.addImage(img, idx++);
            bi.setGridTileImage(img);

            // restricted cells
            f = new File(IMAGE_DIR + SMALL_RESTRICTED_IMG);
            url = f.toURI().toURL();
            img = Toolkit.getDefaultToolkit().getImage(url);
            mt.addImage(img, idx++);
            bi.setRestrictedImage(img);

            // immobile blocks
            f = new File(IMAGE_DIR + SMALL_IMMOBILE_IMG);
            url = f.toURI().toURL();
            img = Toolkit.getDefaultToolkit().getImage(url);
            mt.addImage(img, idx++);
            bi.setImmobileBlockImage(img);

            // number blocks
            List<Image> numberImages = new ArrayList<Image>();

            // logo blocks
            List<Image> logoImages = new ArrayList<>();

            for (int i = 1; i <= 20; i++) {
                // number blocks
                f = new File(SMALL_NUMBERS_DIR + i + ".png");
                url = f.toURI().toURL();
                img = Toolkit.getDefaultToolkit().getImage(url);
                numberImages.add(img);

                // logo blocks
                f = new File(SMALL_LOGO_DIR + i + ".png");
                url = f.toURI().toURL();
                img = Toolkit.getDefaultToolkit().getImage(url);
                logoImages.add(img);
            }

            bi.setNumberImages(numberImages);
            bi.setLogoImages(logoImages);

            // grid indices
            List<Image> gridIndImages = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                f = new File(SMALL_GRID_IND_DIR + i + ".png");
                url = f.toURI().toURL();
                img = Toolkit.getDefaultToolkit().getImage(url);
                gridIndImages.add(img);
            }
            bi.setGridIndexImages(gridIndImages);

        } catch(MalformedURLException mue) {
            mue.printStackTrace();
        }

        try {
            mt.waitForAll();
        } catch (InterruptedException ie) {

        }

        loadedImages = bi;
        readyStatus  = true;
    }

    public void start() {
        thread = new Thread(this);
        thread.start();
    }
}
