package edu.oregonstate.eecs.mcplan.domains.blocksworld.gui;

import convert.BlocksPlanner;
import demo.blocksworld.BlocksConfiguration;
import demo.blocksworld.GeneratedSequence;
import demo.blocksworld.InstructionGenerator;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager;
import static edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.DialogueManager.State.*;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * BlocksWorld GUI class for the BlocksWorld tasks demo.
 * Created by Colin Graber on 9/1/16.
 */
public class BlocksworldGUI {
    // location and String map of blocks configuration files
    private static final String CONFIG_DIR = "data/blocksworld/configs/DemoMay2017";
    private static Map<String,String> fileMap = new HashMap<>();
    private static String[] description = null;
    private static int cfidx = 0;

    // frame height & width
    private static final int FRAME_SIZE_HEIGHT = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
    private static final int FRAME_SIZE_WIDTH = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();

    private static final int XBUFFER = 20;
    private static final int YBUFFER = 10;

    // frame
    private static JFrame frame = null;

    // buttons & drop-downs
    private static JComboBox    configComboBox;     // drop-down list of configurations
    private static ButtonGroup  modeButtonGroup;    // radio buttons: parsing vs generation mode
    private static JRadioButton parseModeButton;    // 'parse' button
    private static JRadioButton planModeButton;     // 'plan' button

    // text fields
    private static JTextPane systemInputField;       // input text field
    private static JTextPane systemOutputPane;       // output text field
    private static JButton   inputSubmitButton;      // 'submit' button
    private static JButton   pasteButton;            // 'paste last' button
    private static JButton   resetButton;            // 'reset' button
    private static BlocksworldDrawing  goldDrawing;  // image of gold configuration
    private static BlocksworldDrawing  testDrawing;  // image of test (planner-generated) configuration
    private static BlocksConfiguration blocksConfig; // block configuration

    // mac mode
    private static boolean MAC_MODE = System.getProperty("os.name").contains("Mac");

    // last input to the system
    private static List<String> lastInput = new LinkedList<>();
    private static int pointer = 0;

    // semantic parser, planner, instruction generator
    private static DialogueManager dm;
    private static InstructionGenerator ig = new InstructionGenerator();

    public static void main(String[] args) {
        try {
            System.out.println("Creating the GUI...");
            BlocksworldGUI.createGUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addToInputHistory(String input) {
        if (lastInput.size() >= 5) lastInput.remove(0);
        lastInput.add(input);
    }

    private static String getLastNthInput(int n) {
        if (lastInput.isEmpty()) return "";
        else if (n > lastInput.size()-1) return lastInput.get(0);
        return lastInput.get(lastInput.size()-1-n);
    }

    /**
     * Clears the screen.
     */
    private static void reset() {
        systemOutputPane.setText("");
        systemInputField.setText("");
        testDrawing.setBlocksConfiguration(new BlocksConfiguration());
    }

    /**
     * Initializes the components of the GUI.
     * @throws InterruptedException
     */
    private static void createGUI() throws InterruptedException {
        if (MAC_MODE) {
            UIManager.getDefaults().put("Label.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("RadioButton.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("Button.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("TextPane.font", UIManager.getFont("TextField.font").deriveFont(18f));
        } else {
            UIManager.getDefaults().put("Label.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("RadioButton.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("Button.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("TextPane.font", UIManager.getFont("TextField.font").deriveFont(25f));
        }

        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                System.out.println("Loading block images...");
                BlockImagesLoader.loadBlocks(e.getWindow());
            }
        });

        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

        // add widgets at proper location
        frame.setLayout(null);

        // top left: drop-down component selecting block configuration
        JPanel configPanel = new JPanel();
        configPanel.setBounds(0,0,FRAME_SIZE_WIDTH/2-50,50);
        configPanel.add(new JLabel("Select configuration:"));
        java.util.List<String> configOptions = findConfigOptions();
        configComboBox = new JComboBox(configOptions.toArray(new String[configOptions.size()]));
        configComboBox.setSelectedIndex(-1);
        configComboBox.insertItemAt("--",0);
        configComboBox.addActionListener(new ConfigSelectionListener());
        configComboBox.setEnabled(false);
        configPanel.add(configComboBox);
        frame.add(configPanel);

        // top right: radio buttons selecting parsing or generation mode
        JPanel modePanel = new JPanel();
        modePanel.setBounds(FRAME_SIZE_WIDTH/2+1-50, 0, FRAME_SIZE_WIDTH/2, 50);
        ActionListener modeListener = new ModeActionListener();
        modeButtonGroup = new ButtonGroup();
        parseModeButton = new JRadioButton("Human to machine");
        parseModeButton.setSelected(true);
        parseModeButton.setActionCommand("Parse");
        parseModeButton.addActionListener(modeListener);
        planModeButton = new JRadioButton("Machine to human");
        planModeButton.setActionCommand("Plan");
        planModeButton.addActionListener(modeListener);
        modeButtonGroup.add(parseModeButton);
        modeButtonGroup.add(planModeButton);
        modePanel.add(parseModeButton);
        modePanel.add(planModeButton);
        frame.add(modePanel);

        // components showing block configurations
        goldDrawing = new BlocksworldDrawing(true);
        testDrawing = new BlocksworldDrawing(true);

        if (MAC_MODE) {
            testDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 460, 330, 330);
            goldDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 80, 330, 330);
        } else {
            testDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 630, 495, 495);
            goldDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 80, 495, 495);
        }

        frame.add(goldDrawing);
        frame.add(testDrawing);

        // labels for configuration panes
        JPanel h2pLabelPanel = new JPanel();
        JPanel p2hLabelPanel = new JPanel();

        h2pLabelPanel.setLayout(new GridBagLayout());
        p2hLabelPanel.setLayout(new GridBagLayout());

        if (MAC_MODE) {
            h2pLabelPanel.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 40, 330, 40);
            p2hLabelPanel.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 420, 330, 40);
        } else {
            h2pLabelPanel.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 40, 330, 40);
            p2hLabelPanel.setBounds((FRAME_SIZE_WIDTH / 2 - 330 - 60) / 2, 420, 330, 40);
        }

        String h2pLabel = "Gold Block Configuration";
        h2pLabelPanel.add(new JLabel(h2pLabel));
        frame.add(h2pLabelPanel);

        String p2hLabel = "Test Block Configuration";
        p2hLabelPanel.add(new JLabel(p2hLabel));
        frame.add(p2hLabelPanel);

        // system output text pane
        EmptyBorder eb = new EmptyBorder(new Insets(10, 10, 10, 10));
        systemOutputPane = new JTextPane();
        systemOutputPane.setEditorKit(new WrapEditorKit());

        if (MAC_MODE)
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 - 50 - XBUFFER, 80, FRAME_SIZE_WIDTH / 2 + 2 * XBUFFER, 600);
        else
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 - 50 - XBUFFER, 80, FRAME_SIZE_WIDTH / 2 + 2 * XBUFFER, 800);

        systemOutputPane.setText("Initializing parser...");
        systemOutputPane.setEditable(false);
        systemOutputPane.setBorder(eb);
        systemOutputPane.setMargin(new Insets(5, 5, 5, 5));

        JPanel noWrapPanel = new JPanel(new BorderLayout());
        noWrapPanel.add(systemOutputPane);
        JScrollPane parseScrollingPane = new JScrollPane(noWrapPanel);
        parseScrollingPane.setViewportView(systemOutputPane);
        parseScrollingPane.setAutoscrolls(true);
        parseScrollingPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        parseScrollingPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        if (MAC_MODE)
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 - 50 - XBUFFER, 80, FRAME_SIZE_WIDTH / 2 + 2 * XBUFFER, 600);
        else
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 - 50 - XBUFFER, 80, FRAME_SIZE_WIDTH / 2 + 2 * XBUFFER, 800);

        frame.add(parseScrollingPane);


        // input text field
        JPanel parseInputPanel = new JPanel();
        parseInputPanel.setLayout(new GridBagLayout());

        GridBagConstraints left = new GridBagConstraints();
        left.fill = GridBagConstraints.BOTH;
        left.weightx = 1000D;
        left.weighty = 1.0D;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0D;
        right.weighty = 1.0d;

        systemInputField = new JTextPane();
        systemInputField.setEnabled(false);
        systemInputField.setEditorKit(new WrapEditorKit());

        // allows for submitting using 'enter' keypress
        int condition = JComponent.WHEN_FOCUSED;
        InputMap iMap = systemInputField.getInputMap(condition);
        ActionMap aMap = systemInputField.getActionMap();
        iMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "enter");
        aMap.put("enter", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputSubmitButton.doClick();
            }
        });

        JScrollPane inputScrollingPane = new JScrollPane(systemInputField);
        inputScrollingPane.setAutoscrolls(true);
        inputScrollingPane.setViewportView(systemInputField);
        inputScrollingPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        inputScrollingPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        // submit, paste, reset buttons
        inputSubmitButton = new JButton("Parse");
        inputSubmitButton.addActionListener(new InputSubmitButtonListener());
        inputSubmitButton.setEnabled(false);
        pasteButton = new JButton("Paste");
        pasteButton.addActionListener(new PasteButtonListener());
        pasteButton.setEnabled(false);
        resetButton = new JButton("Reset");
        resetButton.addActionListener(new ResetButtonListener());
        resetButton.setEnabled(false);
        Box box = Box.createVerticalBox();
        box.add(inputSubmitButton);
        box.add(Box.createVerticalStrut(5));
        box.add(pasteButton);
        box.add(Box.createVerticalStrut(5));
        box.add(resetButton);

        parseInputPanel.add(inputScrollingPane, left);
        parseInputPanel.add(box, right);
        parseInputPanel.setBounds(FRAME_SIZE_WIDTH/2-50-XBUFFER, parseScrollingPane.getY()+parseScrollingPane.getHeight()+YBUFFER, FRAME_SIZE_WIDTH/2+2*XBUFFER, 100);
        frame.add(parseInputPanel);

        frame.setVisible(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                dm = new DialogueManager();
                dm.clearLearnedShapes();
                inputSubmitButton.setEnabled(true);
                pasteButton.setEnabled(true);
                resetButton.setEnabled(true);
                systemInputField.setEnabled(true);
                configComboBox.setEnabled(true);
                systemOutputPane.setEditable(true);
                systemOutputPane.setText("");
                appendToPane(systemOutputPane, dm.getSystemText(), true, new Color(0x000000));
                systemOutputPane.setEditable(false);
            }
        }).start();
    }

    /**
     * Gets the list of options for configuration files available to use as starting points.
     * @return List of configuration file names
     */
    private static java.util.List<String> findConfigOptions() {
        File folder = new File(CONFIG_DIR);
        List<String> options = new ArrayList<>();
        for (File file : folder.listFiles()) {
            int pos = file.getName().lastIndexOf(".");
            String fName = file.getName().substring(0, pos);
            options.add(fName);
            fileMap.put(fName, file.getAbsolutePath());
        }
        return options;
    }

    /**
     * Listener for configuration selection. Sets the blocks configuration for the gold
     * configuration drawing and populates the system input with a sample description or
     * logical form based on mode (parsing or generation).
     */
    private static class ConfigSelectionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            reset();
            cfidx = 0;
            JComboBox cb = (JComboBox)e.getSource();
            String fileName = (String)cb.getSelectedItem();
            systemOutputPane.setEditable(true);
            if (fileName.equals("--")) {
                blocksConfig = new BlocksConfiguration();
                goldDrawing.setBlocksConfiguration(blocksConfig);
                description = null;
                systemInputField.setText("");
                systemOutputPane.setText("");
            }
            else {
                blocksConfig = BlocksConfiguration.loadFromFile(fileMap.get(fileName));
                goldDrawing.setBlocksConfiguration(blocksConfig);
                if (parseModeButton.isSelected()) {
                    description = blocksConfig.getSampleDescription().split("\\|");
                    for (int i = 0; i < description.length; i++) description[i] = description[i].trim();
                    systemInputField.setText(description[0]);
                } else {
                    systemInputField.setText(blocksConfig.getSampleLogicForm());
                }
            }

            dm.reset();
            if (parseModeButton.isSelected()) appendToPane(systemOutputPane, dm.getSystemText(), true, new Color(0x000000));
            systemOutputPane.setEditable(false);
        }
    }

    /**
     * Listener for reset button.
     * TODO: add functionality during generation mode
     */
    private static class ResetButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            systemOutputPane.setEditable(true);
            systemOutputPane.setText("");
            lastInput = new LinkedList<>();
            pointer = 0;
            cfidx = 0;
            if (description != null) systemInputField.setText(description[0]);
            dm.reset();
            if (parseModeButton.isSelected()) appendToPane(systemOutputPane, dm.getSystemText(), true, new Color(0x000000));
            systemOutputPane.setEditable(false);
            testDrawing.setBlocksConfiguration(dm.getSystemBlocksConfiguration());
        }
    }

    /**
     * Listener for paste button.
     * TODO: add functionality during generation mode
     */
    private static class PasteButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            systemInputField.setText(getLastNthInput(pointer));
            pointer++;
        }
    }

    /**
     * Listener for human input button.
     * TODO: reimplement using dialogue manager
     */
    private static class InputSubmitButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (systemInputField.getText().isEmpty()) return;

            pointer = 0;
            systemOutputPane.setEditable(true);
            if (dm.nextState() != DialogueManager.State.PARSE_VERIFICATION &&
                    !systemInputField.getText().equalsIgnoreCase("I'm finished"))
                addToInputHistory(systemInputField.getText());

            if (parseModeButton.isSelected()) {
                appendToPane(systemOutputPane, "[HUMAN]: " + systemInputField.getText() + "\n\n", false, new Color(0x0000000));
                dm.parse(systemInputField.getText());

//                System.out.println(dm.previousState()+"; "+cfidx);
                if (description != null && cfidx < description.length - 1 && (dm.previousState() != REQUEST_DESCRIPTION && !systemInputField.getText().equals(description[cfidx])))
                    cfidx = description.length;

                if ((dm.nextState() == PARSE_DESCRIPTION || dm.nextState() == PARSE_CLARIFICATION)
                        && description != null && cfidx < description.length - 1) {
                    cfidx++;
                    systemInputField.setText(description[cfidx]);
                } else systemInputField.setText("");

                if (!dm.getSystemText().isEmpty())
                    appendToPane(systemOutputPane, dm.getSystemText(), true, new Color(0x0000000));

//                if (dm.getSystemBlocksConfiguration() != null)
                testDrawing.setBlocksConfiguration(dm.getSystemBlocksConfiguration());

                if (dm.isFinished() || dm.hasFailed()) {
                    appendToPane(systemOutputPane, "----------------------------------------------------\n", true, new Color(0x000000));
                    lastInput = new LinkedList<>();
                    pointer = 0;
//                    dm.printHistory();
                    dm.reset();
                    appendToPane(systemOutputPane, "\n" + dm.getSystemText(), true, new Color(0x0000000));
                }
            }

            else {
                BlocksPlanner planner = new BlocksPlanner();
                List<String> result = planner.probe(systemInputField.getText()).getPlan();
                if (result != null) {
                    System.out.println("RESULT: " + result);
                    systemInputField.setText("");
                    BlocksConfiguration config = BlocksConfiguration.fromPlannerRepresentation(result);
                    GeneratedSequence seq = ig.generate(config);
                    appendToPane(systemOutputPane, "[MACHINE]: \n", true, new Color(0x000000));
                    appendToPane(systemOutputPane, seq + "\n", false, new Color(0x000000));
                    if (!result.isEmpty()) {
                        BlocksConfiguration testConfig = BlocksConfiguration.fromGeneratorRepresentation(result, seq);
                        int type = seq.logos ? BlocksworldDrawing.LOGO_BLOCKS : BlocksworldDrawing.NUMBER_BLOCKS;
                        testDrawing.setBlocksConfiguration(testConfig, type);
                    }
                }
                else appendToPane(systemOutputPane, "[MACHINE]: I'm sorry, but the input you gave me seems to be invalid.", true, new Color(0x000000));
            }

            systemOutputPane.setEditable(false);

//            } else {
//                if (!prevWasPlanner) {
//                    appendToPane(systemOutputPane, "----------------------------------------------------------------------------------------------------------------------------------\n", true, new Color(0x000000));
//                }
//                appendToPane(systemOutputPane, "<PLANNER INPUT>: "+ systemInputField.getText()+"\n", true, new Color(0xFF0800));
//                //TODO: PLANNER/GENERATION STUFF
//                List<String> result = planner.parseInput(systemInputField.getText());
//
//                if (result != null) {
//
//                    System.out.println("RESULT: " + result);
//                    appendToPane(systemOutputPane, "<PLANS>: " + result + "\n", true, new Color(0xFF0800));
//                    systemInputField.setText("");
//                    BlocksConfiguration config = BlocksConfiguration.fromPlannerRepresentation(result);
//                    GeneratedSequence seq = ig.generate(config);
//                    appendToPane(systemOutputPane, "<MACHINE>: " + seq + "\n", true, new Color(0x0000FF));
//                    prevWasPlanner = true;
//                    if (!result.isEmpty()) {
//                        BlocksConfiguration testConfig = BlocksConfiguration.fromGeneratorRepresentation(result, seq);
//                        int type = seq.logos ? BlocksworldDrawing.LOGO_BLOCKS : BlocksworldDrawing.NUMBER_BLOCKS;
//
//                        testDrawing.setBlocksConfiguration(testConfig, type);
//                    }
//                } else {
//                    appendToPane(systemOutputPane, "<MACHINE>: I'm sorry, but the input you gave me seems to be invalid.", true, new Color(0x0000FF));
//                }
//            }
//            systemOutputPane.setEditable(false);
        }
    }

    /**
     * Listener for the parsing/generation mode radio button.
     * Changes the text of the submit button and populates the system input with
     * a sample description or logical form based on the mode.
     */
    private static class ModeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            inputSubmitButton.setText(e.getActionCommand());
            systemOutputPane.setEditable(true);
            systemOutputPane.setText("");

            if (blocksConfig != null) {
                if (parseModeButton.isSelected()) {
                    systemInputField.setText(blocksConfig.getSampleDescription());
                    dm.reset();
                    appendToPane(systemOutputPane, dm.getSystemText(), true, new Color(0x000000));
                } else {
                    systemInputField.setText(blocksConfig.getSampleLogicForm());
                }
            }

            systemOutputPane.setEditable(false);
        }
    }

    static class MyListCellRenderer extends DefaultListCellRenderer {
        public MyListCellRenderer() {
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
        }
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
                                                      boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            //TODO: what to do here?
            return c;
        }
    }

    /**
     * Appends a text string to a text pane.
     * @param tp Text pane
     * @param msg String to append
     * @param isBold Whether or not string should be bold
     * @param c Color
     */
    public static void appendToPane(JTextPane tp, String msg, boolean isBold, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        if (isBold) aset = sc.addAttribute(aset, StyleConstants.Bold, true);
        else aset = sc.addAttribute(aset, StyleConstants.Bold, false);

        if (MAC_MODE) aset = sc.addAttribute(aset, StyleConstants.FontSize, 18);
        else aset = sc.addAttribute(aset, StyleConstants.FontSize, 25);

        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
}
