package edu.oregonstate.eecs.mcplan.domains.blocksworld.gui;

import demo.blocksworld.BlocksConfiguration;
import demo.blocksworld.GeneratedSequence;
import demo.blocksworld.GridCoordinate;
import demo.blocksworld.InstructionGenerator;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksPlanner;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.BlocksSemanticParser;
import edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.ParserOutput;
import semantics.utils.Utils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Colin Graber on 11/1/16.
 */
public class BlocksworldEvaluation {
    public static final boolean MAC_MODE = true;

    public static String CONFIG_DIR = "data/blocksworld/configs/MITRE_evaluation";

    public static String LOG_DIR = "evaluation/log/";
    static {
        new File(LOG_DIR).mkdirs();
    }
    public static String LOG_FILE = "";
    public static PrintWriter LOG_FILE_WRITER;

    public static Boolean GUI_LOCK = true;
    public static boolean SETTING_LOG = false;
    public static Boolean GAME_STARTED = false;
    public static final int FRAME_SIZE_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
    public static final int FRAME_SIZE_WIDTH = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();

    public static final int XBUFFER = 50;
    public static final int YBUFFER = 10;

    private static JFrame frame = null;
    private static JComboBox configComboBox;
    private static ButtonGroup modeButtonGroup;
    private static JRadioButton parseModeButton;
    private static JRadioButton planModeButton;
    public static JScrollPane parseScrollingPane;

    public static JTextPane systemInputField;
    public static JTextPane systemOutputPane;
    public static JTextField logTextField;
    public static JButton logSetButton;
    public static JButton inputSubmitButton;
    public static JPanel parseInputPanel;
    public static BlocksworldDrawing gridDrawing;
    public static BlocksworldDrawing configDrawing;
    public static BlockImages blockImages;
    public static BlocksConfiguration blocksConfig;
    public static Map<String,String> fileMap = new HashMap<>();
    public static BlocksSemanticParser parser = null;
    public static BlocksPlanner planner = new BlocksPlanner();
    public static InstructionGenerator ig = new InstructionGenerator();

    private static int gameNumber = -1;
    private static int uctConstant = -1;
    private static int simulationCount = -1;
    private static int sampleCount = -1;

    public static void main(String[] args) {
        try {
            BlocksworldEvaluation.createGUI();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void reset() {
        systemOutputPane.setText("");
        systemInputField.setText("");
        configDrawing.setBlocksConfiguration(new BlocksConfiguration());
        gridDrawing.setBlocksConfiguration(new BlocksConfiguration());

    }

    public static void createGUI() throws InterruptedException {
        //UIManager.getDefaults().put("TextArea.font", UIManager.getFont("TextArea.font").deriveFont(25f));
        if (MAC_MODE) {
            UIManager.getDefaults().put("Label.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("RadioButton.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("Button.font", UIManager.getFont("TextField.font").deriveFont(18f));
            UIManager.getDefaults().put("TextPane.font", UIManager.getFont("TextField.font").deriveFont(18f));
        } else {
            UIManager.getDefaults().put("Label.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("ComboBox.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("RadioButton.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("Button.font", UIManager.getFont("TextField.font").deriveFont(25f));
            UIManager.getDefaults().put("TextPane.font", UIManager.getFont("TextField.font").deriveFont(25f));
        }
        frame = new JFrame();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                System.out.println("Loading block images...");
                blockImages = BlockImagesLoader.loadBlocks(e.getWindow());
                configDrawing.repaint();
                gridDrawing.repaint();
            }
        });
        //frame.setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());
        //frame.setSize(FRAME_SIZE_WIDTH, FRAME_SIZE_HEIGHT);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //frame.setResizable(false);
        // add widgets at proper location
        frame.setLayout(null);

        // TOP ROW
        // component selecting block configuration
        JPanel configPanel = new JPanel();
        configPanel.setBounds(0,0,FRAME_SIZE_WIDTH/2,50);
        configPanel.add(new JLabel("Select Configuration:"));
        java.util.List<String> configOptions = findConfigOptions();
        configComboBox = new JComboBox(configOptions.toArray(new String[configOptions.size()]));
        configComboBox.setSelectedIndex(-1);
        configComboBox.addActionListener(new BlocksworldEvaluation.ConfigSelectionListener());
        configComboBox.setEnabled(false);
        configPanel.add(configComboBox);
        logTextField = new JTextField(20);
        configPanel.add(logTextField);
        logSetButton = new JButton("Set Log");
        logSetButton.setEnabled(false);
        logSetButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("HERE");
                if (LOG_FILE_WRITER == null) {
                    //No Log active - we start a new one
                    boolean result = initializeLog(logTextField.getText());
                    if (result) {
                        unlockAndClear();
                        logTextField.setEnabled(false);
                        logSetButton.setText("Close Log");
                    }
                } else {
                    //We're closing a currently open log
                    LOG_FILE_WRITER.close();
                    LOG_FILE_WRITER = null;
                    logSetButton.setText("Set Log");
                    logTextField.setEnabled(true);
                    lockAndClear();
                }
            }
        });
        configPanel.add(logSetButton);
        frame.add(configPanel);

        JPanel modePanel = new JPanel();
        modePanel.setBounds(FRAME_SIZE_WIDTH/2+1, 0, FRAME_SIZE_WIDTH/2, 50);
        ActionListener modeListener = new BlocksworldEvaluation.ModeActionListener();
        modeButtonGroup = new ButtonGroup();
        parseModeButton = new JRadioButton("Human to machine");
        parseModeButton.setSelected(true);
        parseModeButton.setActionCommand("Parse");
        parseModeButton.addActionListener(modeListener);
        planModeButton = new JRadioButton("Machine to human");
        planModeButton.setActionCommand("Plan");
        planModeButton.addActionListener(modeListener);
        modeButtonGroup.add(parseModeButton);
        modeButtonGroup.add(planModeButton);
        modePanel.add(parseModeButton);
        modePanel.add(planModeButton);
        parseModeButton.setEnabled(false);
        planModeButton.setEnabled(false);
        frame.add(modePanel);

        // BLOCK DRAWING ROW
        // Component showing block configuration
        //JPanel goldDrawingPanel = new JPanel();
        //goldDrawingPanel.setLayout(new GridBagLayout());
        //goldDrawingPanel.setBounds(0, 50, FRAME_SIZE_WIDTH/2, 450);
        //goldDrawing = new BlocksworldDrawing();
        //goldDrawingPanel.add(goldDrawing);
        //goldDrawing.setBounds((FRAME_SIZE_WIDTH/2-450)/2, 50, 450, 450);
        //goldDrawing.setBackground(new java.awt.Color(0, 128, 0));
        //frame.add(goldDrawing);

        configDrawing = new BlocksworldDrawing(false);
        if (MAC_MODE) {
            configDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330) / 2, 410, 330, 330);
        } else {
            configDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330) / 2, 580, 495, 495);
        }
        frame.add(configDrawing);

        gridDrawing = new BlocksworldDrawing(true);
        if (MAC_MODE) {
            gridDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330) / 2, 70, 330, 330);
        } else {
            configDrawing.setBounds((FRAME_SIZE_WIDTH / 2 - 330) / 2, 70, 495, 495);
        }
        frame.add(gridDrawing);


        //JPanel h2pLabelPanel = new JPanel();
        //h2pLabelPanel.setLayout(new GridBagLayout());
        //h2pLabelPanel.setBounds(0, 500, FRAME_SIZE_WIDTH/2, 40);
        //String instructions = "Gold Block Configuration";
        //h2pLabelPanel.add(new JLabel(instructions));
        //frame.add(h2pLabelPanel);


        // Component showing output of systems
        EmptyBorder eb = new EmptyBorder(new Insets(10, 10, 10, 10));
        systemOutputPane = new JTextPane();
        systemOutputPane.setEditorKit(new WrapEditorKit());
        if (MAC_MODE) {
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 500);
        } else {
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 900);
        }
        systemOutputPane.setText("Initializing Parser...");
        systemOutputPane.setEditable(false);
        systemOutputPane.setBorder(eb);
        systemOutputPane.setMargin(new Insets(5, 5, 5, 5));

        JPanel noWrapPanel = new JPanel(new BorderLayout());
        noWrapPanel.add(systemOutputPane);
        parseScrollingPane = new JScrollPane(noWrapPanel);
        parseScrollingPane.setViewportView(systemOutputPane);
        parseScrollingPane.setAutoscrolls(true);
        parseScrollingPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        parseScrollingPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        if (MAC_MODE) {
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 500);
        } else {
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 700);
        }
        frame.add(parseScrollingPane);


        // Text Field, Button to provide input to/start semantic parser
        parseInputPanel = new JPanel();
        parseInputPanel.setLayout(new GridBagLayout());
        GridBagConstraints left = new GridBagConstraints();
        //left.anchor = GridBagConstraints.LINE_START;
        left.fill = GridBagConstraints.BOTH;

        left.weightx = 1000D;
        left.weighty = 1.0D;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0D;
        right.weighty = 1.0d;

        systemInputField = new JTextPane();
        //systemInputField.setBounds(10,920,FRAME_SIZE_WIDTH-20, 160);
        //systemInputField.setFont(systemInputField.getFont().deriveFont(25f));

        //systemInputField.setLineWrap(true);
        systemInputField.setEnabled(false);
        systemInputField.setEditorKit(new WrapEditorKit());
        JScrollPane inputScrollingPane = new JScrollPane(systemInputField);
        inputScrollingPane.setAutoscrolls(true);
        inputScrollingPane.setViewportView(systemInputField);
        inputScrollingPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        inputScrollingPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        inputSubmitButton = new JButton("Parse");
        inputSubmitButton.addActionListener(new BlocksworldEvaluation.InputSubmitButtonListener());
        inputSubmitButton.setEnabled(false);
        parseInputPanel.add(inputScrollingPane, left);
        parseInputPanel.add(inputSubmitButton, right);
        parseInputPanel.setBounds(FRAME_SIZE_WIDTH/2+XBUFFER, parseScrollingPane.getY()+parseScrollingPane.getHeight()+YBUFFER, FRAME_SIZE_WIDTH/2-2*XBUFFER, 100);
        frame.add(parseInputPanel);

        // LEFT COLUMN


        frame.setVisible(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                parser = new BlocksSemanticParser();
                SETTING_LOG = true;
                logSetButton.setEnabled(true);
                systemOutputPane.setEditable(true);
                systemOutputPane.setText("Enter log file newShape");
                systemOutputPane.setEditable(false);

            }
        }).start();
    }
    private static void lockAndClear() {
        inputSubmitButton.setEnabled(false);
        systemOutputPane.setEditable(true);
        systemOutputPane.setText("");
        systemOutputPane.setEditable(false);
        systemInputField.setEnabled(false);
        configComboBox.setEnabled(false);
        planModeButton.setEnabled(false);
        parseModeButton.setEnabled(false);
        reset();
    }
    private static void unlockAndClear() {
        inputSubmitButton.setEnabled(true);
        systemOutputPane.setEditable(true);
        systemOutputPane.setText("");
        systemOutputPane.setEditable(false);
        systemInputField.setEnabled(true);
        configComboBox.setEnabled(true);
        planModeButton.setEnabled(true);
        parseModeButton.setEnabled(true);
        reset();
    }

    private static java.util.List<String> findConfigOptions() {
        File folder = new File(CONFIG_DIR);
        java.util.List<String> options = new ArrayList<>();
        for (File file: folder.listFiles()) {
            int pos = file.getName().lastIndexOf(".");
            String fName = file.getName().substring(0, pos);
            options.add(fName);
            fileMap.put(fName, file.getAbsolutePath());
        }
        Collections.sort(options);
        return options;
    }
    private static class ConfigSelectionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            reset();
            JComboBox cb = (JComboBox)e.getSource();
            String fileName = (String)cb.getSelectedItem();
            blocksConfig = BlocksConfiguration.loadFromFile(fileMap.get(fileName));
            log("[SETTING CONFIG: "+fileName+"]");
            if (planModeButton.isSelected()) {
                runPlanner();
            }
        }
    }

    private static boolean initializeLog(String logName) {
        if (LOG_FILE_WRITER != null) {
            LOG_FILE_WRITER.close();
        }
        String temp_log_file = LOG_DIR + logName;
        try {
            LOG_FILE_WRITER = new PrintWriter(new BufferedWriter(new FileWriter(temp_log_file, true)));
            log("STARTING LOG FILE");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        LOG_FILE = temp_log_file;
        return true;
    }
    private static void log(String msg) {
        LOG_FILE_WRITER.println((new SimpleDateFormat("dd/MM/yyyy:::HH:mm:ss").format(new Date()))+(": "+msg));
        LOG_FILE_WRITER.flush();
    }
    private static void setGuiToParse() {
        frame.remove(parseScrollingPane);
        if (MAC_MODE) {
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 500);
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 500);
        } else {
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 700);
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 700);
        }
        //gridDrawing.setBounds((FRAME_SIZE_WIDTH/2-495)/2,70, 495, 495);
        frame.add(configDrawing);
        frame.add(gridDrawing);
        frame.add(parseScrollingPane);
        frame.add(parseInputPanel);
        frame.repaint();
    }
    private static void setGuiToPlan() {
        frame.remove(configDrawing);

        frame.remove(parseInputPanel);
        frame.remove(parseScrollingPane);
        if (MAC_MODE) {
            //gridDrawing.setBounds((FRAME_SIZE_WIDTH/2-495)/2,200, 495, 495);
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 700);
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 700);
        } else {
            //gridDrawing.setBounds((FRAME_SIZE_WIDTH/2-495)/2,200, 495, 495);
            systemOutputPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 900);
            parseScrollingPane.setBounds(FRAME_SIZE_WIDTH / 2 + XBUFFER, 80, FRAME_SIZE_WIDTH / 2 - 2 * XBUFFER, 900);
        }
        frame.add(parseScrollingPane);
        frame.repaint();
    }
    private static class InputSubmitButtonListener implements ActionListener {
        private boolean prevWasPlanner = false;
        private boolean first = true;
        @Override
        public void actionPerformed(ActionEvent e) {
            systemOutputPane.setEditable(true);
            if (SETTING_LOG) {
                SETTING_LOG = false;
            }else {
                if (first) {
                    first = false;
                    if (planModeButton.isSelected()) {
                        prevWasPlanner = true;
                    }
                }
                if (parseModeButton.isSelected()) {
                    String log_output = "<HUMAN>: " + systemInputField.getText();
                    appendToPane(systemOutputPane, log_output + "\n", true, new Color(0x0000000));
                    log(log_output);
                    ParserOutput output = parser.parseInput(systemInputField.getText());
                    systemInputField.setText("");
                    if (output == null) {
                        appendToPane(systemOutputPane, "<MACHINE>: Sorry, I didn't understand what you said. Could you try again?\n", true, new Color(0x0000FF));
                        log("<MACHINE>: Sorry, I didn't understand what you said. Could you try again?");
                    } else if (output.getStatusCode() == ParserOutput.UKNOWN_VOCAB_CODE) {
                        StringBuilder result = new StringBuilder("<MACHINE>: Sorry, but I didn't understand the following words: ");
                        for (String word : output.getUnknownVocab()) {
                            result.append("\n\t" + word);
                        }
                        result.append("\n");
                        appendToPane(systemOutputPane, result.toString(), true, new Color(0x0000FF));
                        log(result.toString());

                    } else if (output.getStatusCode() == ParserOutput.FAILURE_CODE){
                        log("[INPUT TO POST PROCESSING: "+output.getParse()+"]");
                        appendToPane(systemOutputPane, "<MACHINE>: Sorry, I didn't understand what you said. Could you try again?\n", true, new Color(0x0000FF));
                        log("<MACHINE>: Sorry, I didn't understand what you said. Could you try again?");
                    } else {
                        log("[PARSE OBTAINED: "+output.getParse()+"]");
                        System.out.println("NEW PARSE: "+output.getUnconstrainedParse());
                        java.util.List<String> plannerResult = planner.parseInput(output.getUnconstrainedParse());
                        System.out.println(plannerResult);
                        if (plannerResult != null) {
                            if (plannerResult.isEmpty()||plannerResult.size() == 1) {
                                String msg = "<MACHINE>: Your description is not specific enough for me to fully understand the configuration!";
                                appendToPane(systemOutputPane, msg+"\n", true, new Color(0x0000FF));
                                log(msg);
                            } else {
                                BlocksConfiguration testConfig = BlocksConfiguration.fromPlannerRepresentation(plannerResult);
                                testConfig.setImmobileLocations(output.getImmobileBlocks());
                                testConfig.setRestrictedLocations(output.getRestricted());
                                testConfig.setMovableLocations(output.getStartingBlocks());
                                configDrawing.setBlocksConfiguration(testConfig);
                                gridDrawing.setBlocksConfiguration(testConfig);
                                appendToPane(systemOutputPane, "<MACHINE>: Here's the configuration I think you described." + "\n", true, new Color(0x0000FF));
                                log("<MACHINE>: Here's the configuration I think you described.");
                            }
                        } else {
                            appendToPane(systemOutputPane, "<MACHINE>: If I understood the configuration correctly, it is impossible to build it given your constraints.\n", true, new Color(0x0000FF));
                            log("<MACHINE>: If I understood the configuration correctly, it is impossible to build it given your constraints.");
                        }
                    }
                    prevWasPlanner = false;
                } else {
                    //TODO: PLANNER/GENERATION STUFF

                }
            }
            systemOutputPane.setEditable(false);
        }
    }

    private static class ModeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            inputSubmitButton.setText(e.getActionCommand());
            unlockAndClear();
            if (parseModeButton.isSelected()) {
                //systemInputField.setText(blocksConfig.getSampleDescription());
                log("[SWITCHED TO PARSING MODE]");
                setGuiToParse();
            } else {
                //systemInputField.setText(blocksConfig.getSampleLogicForm());
                log("[SWITCHED TO GENERATION MODE]");
                setGuiToPlan();
                if (blocksConfig != null) {
                    runPlanner();
                }
            }
        }
    }
    private static void runPlanner() {
        log("[INPUT TO PLANNER: "+blocksConfig.getSampleLogicForm()+"]");
        java.util.List<String> result = planner.parseInput(blocksConfig.getSampleLogicForm());
        systemOutputPane.setEditable(true);
        if (result != null) {
            log("[PLANS: " + result + "]");
            systemInputField.setText("");
            BlocksConfiguration config = BlocksConfiguration.fromPlannerRepresentation(result);
            GeneratedSequence seq = ig.generate(config);
            if (!config.getMovableLocations().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Starting blocks: ");
                for (GridCoordinate coord: config.getMovableLocations()) {
                    System.out.println(coord);
                    sb.append(String.format("%s @ (%d, %d)\t", Utils.brands[seq.randomIDs.get(coord.getID()-1)], coord.getX(), coord.getY()));
                }
                log(sb.toString());

            }
            if (!config.getImmobileLocations().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Immobile blocks: ");
                for (GridCoordinate coord: config.getImmobileLocations()) {
                    sb.append(String.format("%s @ (%d, %d)\t", Utils.brands[seq.randomIDs.get(coord.getID()-1)], coord.getX(), coord.getY()));
                }
                log(sb.toString());
            }



            appendToPane(systemOutputPane, "<MACHINE>: \n" + seq + "\n", true, new Color(0x0000FF));
            StringBuilder logInfo = new StringBuilder();
            logInfo.append("<MACHINE>: ");
            for (int i = 0; i < seq.instructions.size(); i++) {
                logInfo.append("\n\t"+Utils.brands[seq.randomIDs.get(seq.blockIDs.get(i)-1)]+config.getPlans().get(i)+"~~~~"+seq.instructions.get(i));
            }
            log(logInfo.toString());
            gridDrawing.setBlocksConfiguration(config, BlocksworldDrawing.LOGO_BLOCKS, seq.randomIDs);
        } else {
            appendToPane(systemOutputPane, "<MACHINE>: I'm sorry, but the input you gave me seems to be invalid.", true, new Color(0x0000FF));
        }
        systemOutputPane.setEditable(false);
    }
    static class MyListCellRenderer extends DefaultListCellRenderer {
        public MyListCellRenderer() {
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
        }
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
                                                      boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            //TODO: what to do here?
            return c;
        }
    }
    public static void appendToPane(JTextPane tp, String msg, boolean isBold, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();

        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
        if (!isBold) {
            //StyleConstants.setBold((MutableAttributeSet) aset, true);
            aset = sc.addAttribute(aset, StyleConstants.Bold, true);
            if (MAC_MODE) {
                aset = sc.addAttribute(aset, StyleConstants.FontSize, 18);
            } else {
                aset = sc.addAttribute(aset, StyleConstants.FontSize, 25);
            }
        }
        else {
            aset = sc.addAttribute(aset, StyleConstants.Bold, true);
            if (MAC_MODE) {
                aset = sc.addAttribute(aset, StyleConstants.FontSize, 18);
            } else {
                aset = sc.addAttribute(aset, StyleConstants.FontSize, 25);
            }
        }
        /*if (c.getRed() == 68 && c.getGreen() == 195 && c.getBlue() == 198) {
            aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        }*/
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);


        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
}
