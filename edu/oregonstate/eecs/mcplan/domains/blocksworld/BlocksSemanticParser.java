package edu.oregonstate.eecs.mcplan.domains.blocksworld;

import edu.oregonstate.eecs.mcplan.domains.blocksworld.dialogue.ParserOutput;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Colin Graber on 9/4/16.
 */
public class BlocksSemanticParser {
    private static final boolean IS_MULTIPLE_MODEL = true;

    public static final String PYTHON_DIR = "python/parser/";
    public static final String PARSER_FILE = PYTHON_DIR + "parse_input.py";
    public static final String MODEL_DIR = PYTHON_DIR + "models/";
    public static final String MODEL_FILE = MODEL_DIR + "mixed_model_v3"; // TODO: change back to updated model
    public static final String CONF_FILE = MODEL_DIR + "mixed_model_v3.conf";
    public static final String OUTPUT_FILE = PYTHON_DIR + "output_parse.txt";

    public static final String MULT_PARSER_FILE = PYTHON_DIR + "double_parser.py";
    public static final String MULT_MODEL_FILE_1 = MODEL_DIR + "twoparser_oldstuff_fixed";
    public static final String MULT_MODEL_FILE_2 = MODEL_DIR + "twoparser_newvocab";
    public static final String MULT_CONF_FILE_1 = MODEL_DIR + "twoparser_oldstuff_fixed.conf";
    public static final String MULT_CONF_FILE_2 = MODEL_DIR + "twoparser_newvocab.conf";

    private Process parserProcess;
    private BufferedReader procInputStream;
    private BufferedWriter procOutputStream;
    private BufferedReader procErrorStream;

    public BlocksSemanticParser() {
        String [] init_command;
        if (IS_MULTIPLE_MODEL) {
            init_command = new String[]{"python", "-u", MULT_PARSER_FILE, MULT_MODEL_FILE_1, MULT_CONF_FILE_1, MULT_MODEL_FILE_2, MULT_CONF_FILE_2, OUTPUT_FILE};
        }else {
            init_command = new String[]{"python", "-u", PARSER_FILE, MODEL_FILE, CONF_FILE, OUTPUT_FILE};
        }
        try {

            parserProcess = Runtime.getRuntime().exec(init_command);
            procInputStream = new BufferedReader(new InputStreamReader(parserProcess.getInputStream()));
            procOutputStream =new BufferedWriter(new OutputStreamWriter(parserProcess.getOutputStream()));
            procErrorStream = new BufferedReader(new InputStreamReader(parserProcess.getErrorStream()));
            System.out.println("STARTING");
            waitForResult();
            System.out.println("FINISHED");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void reset() {
        System.out.println("Resetting parser");
        try {
            procOutputStream.write("1\n");
            procOutputStream.flush();
            String line = "";
            String err = "";
            waitForResult();
        } catch (Exception e) {
            System.out.println("RESETTING PARSER FAILED");
            e.printStackTrace();
        }
    }
    public ParserOutput parseClarification(String humanInput, String plannerQuestion) {
        try {
            procOutputStream.write("3\n");
            procOutputStream.write(plannerQuestion+"\n");
            procOutputStream.write(humanInput + "\n");
            procOutputStream.flush();

            waitForResult();
            return readParseResult();
        }catch (Exception e) {
            System.out.println("UNABLE TO RUN PARSER");
            e.printStackTrace();
        }
        return null;
    }

    public void undoParses(int num) {
        try {
            procOutputStream.write("2\n");
            procOutputStream.write(""+num+"\n");
            procOutputStream.flush();
            waitForResult();
        } catch (Exception e) {
            System.out.println("UNABLE TO CLEAR PARSER");
            e.printStackTrace();
        }
    }
    public ParserOutput parseInput(String input) {
        input = input.trim();
        System.out.println("Parser received input: "+input);
        if (input.isEmpty()) {
            return null;
        }
        try {
            procOutputStream.write("0\n");
            procOutputStream.write(input+"\n");
            procOutputStream.flush();
            waitForResult();

            return readParseResult();
        } catch (Exception e) {
            System.out.println("UNABLE TO RUN PARSER");
            e.printStackTrace();
        }
        return null;
    }
    private void waitForResult() throws Exception {

        String line = "";
        String err = "";
        while (line == null || !line.equals("ready")) {

            if (procErrorStream.ready()) {
                err = procErrorStream.readLine();
            } else if (procInputStream.ready()) {
                line = procInputStream.readLine();
            } else {
                Thread.sleep(100);
            }
            if (err != null) {
                System.out.println(err);
                err = null;
            }
            if (line != null) {
                System.out.println(line);
                if (line.equals("ready")) {
                    break;
                }
                line = null;
            }
        }
    }

    private ParserOutput readParseResult() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(OUTPUT_FILE));
        String line = br.readLine();
        int status = Integer.parseInt(line);
        ParserOutput output;
        if (status == ParserOutput.SUCCESS_CODE) {
            List<String> parse = new ArrayList<String>();
            while ((line = br.readLine()) != null) {
                parse.add(line);
            }
            System.out.println("Parse result: " + parse);
            output = new ParserOutput(status, parse, null);
        } else if (status == ParserOutput.UKNOWN_VOCAB_CODE) {
            List<String> unknownVocab = new ArrayList<String>();
            while ((line = br.readLine()) != null) {
                unknownVocab.add(line);
            }
            output = new ParserOutput(status, null, unknownVocab);
        } else if (status == ParserOutput.FAILURE_CODE) {
            return null;
        }else {
            throw new Exception("WTF YO");
        }
        br.close();
        return output;
    }

}
