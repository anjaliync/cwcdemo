package edu.oregonstate.eecs.mcplan.agents;

import edu.oregonstate.eecs.mcplan.Arbiter;
import edu.oregonstate.eecs.mcplan.History;
import edu.oregonstate.eecs.mcplan.State;
import edu.oregonstate.eecs.mcplan.domains.freecell.FreeCellAction;
import edu.oregonstate.eecs.mcplan.domains.freecell.FreeCellNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mislam1 on 2/24/16.
 */
public class FreeCellFilterAgent implements ActionFilter  {
    @Override
    //just removing the reverse action of the earlier one
    public <A> List<A> filter(List<A> actionList, State state) {
        FreeCellAction la = Arbiter.lastAction;
        History<FreeCellNode, FreeCellAction> history =  Arbiter.hist;
        int histSZ = history.getSize();
        List <Integer> removalList = new ArrayList<>();
        if (la != null) {
            for ( int i = 0; i < actionList.size(); ++i) {
                FreeCellAction fca = (FreeCellAction) actionList.get(i);
                boolean dup = isDuplicate(la, fca);
                if (dup) {
                    removalList.add(i);
                    //actionList.remove(i);
                }
            }
        }

        for (int index = 0; index < actionList.size(); ++index) {

            if (histSZ >= 5) {
                for (int i = histSZ - 1; (histSZ - i -1 <= 5) && (i >= 0); --i) {
                    try {
                        FreeCellAction fcaOld = history.getAction(i);
                        boolean dup = isDuplicate(fcaOld, (FreeCellAction) actionList.get(index));
                        if (dup) {
                            removalList.add(index);
                            //actionList.remove(index);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        Collections.sort(removalList);
        HashSet<Integer> set = new HashSet<>(removalList);
        List<A> actionListReturn = new ArrayList<>();
        for (int i = 0; i < actionList.size(); ++i) {
            if (!set.contains(i)) {
                actionListReturn.add(actionList.get(i));
            }
        }
        set.clear();
        removalList.clear();
        for (A a : actionListReturn) {
           // System.out.println("  "+ a);
        }
        return actionListReturn;
    }

    public boolean isDuplicate (FreeCellAction la, FreeCellAction fca) {
        if (fca.cardDestination() == la.cardSource()
                && fca.cardSource() == la.cardDestination()
                && fca.cardSuit() == la.cardSuit()
                && fca.cardRank() == la.cardRank()
                ) {
            /*if (fca.cardSource() == -1 && la.cardSource() >= 0) {
                System.out.println("bakkas");
            }
            else if (fca.cardSource() > 0 && la.cardSource() == -1) {
                System.out.println("bakkas");
            }*/
            //System.out.println("filtered: " +fca);
            return true;

        }
        return false;
    }
}
