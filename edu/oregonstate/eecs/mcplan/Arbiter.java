package edu.oregonstate.eecs.mcplan;

import algs.model.searchtree.IMove;
import algs.model.searchtree.IScore;
import edu.illinois.cs.cogcomp.ccg2.parser.TrainedSemanticParser;
import edu.oregonstate.eecs.mcplan.agents.ActionOutcome;
import edu.oregonstate.eecs.mcplan.agents.FreeCellFilterAgent;
import edu.oregonstate.eecs.mcplan.domains.freecell.*;
import generator.*;
import generator.Action;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import static edu.oregonstate.eecs.mcplan.Main.GUI_LOCK_SEND;

/**
 * Arbiter is used to regulate agents with the simulator. It also collects
 * reward and timing data.
 */
public class Arbiter<S extends State, A> {
    /** Real world domain being used. */
    private Simulator<S, A> world_;

    /** Agents taking action in domain. */
    private List<Agent> agents_;

    /** Keeps track of actions taken and states visited. */
    private History<S, A> history_;
    public static History <FreeCellNode, FreeCellAction> hist = new History<>();

    private List<long[]> totalMoveTimeData_;

    private List<double[]> totalRewardsData_;

    private List<int[]> actionCountsData_;

    private BufferedWriter outputFile_;
    public static IScore scoringFunction;
    private static FreeCellFilterAgent freeCellFilterAgent = new FreeCellFilterAgent();
    private static List<String> possibleCards = new ArrayList<>();

    /** Frame in which app runs. */
    final JFrame frame_;

    /** Viewer to represent game on screen. */
    final FreeCellDrawing drawer_;

    /** List containing moves. */
    final JList list_;

    /** Last listener (so we can delete with each redeal). */
    StateModifier listener;
    public static FreeCellAction lastAction = null;
    public static Object dummyObject = new Object();

    public Arbiter(Simulator<S, A> world, List<Agent> agents, String fileName, JFrame frame, FreeCellDrawing drawer, JList list) {
        if (world.getNumberOfAgents() != agents.size())
            throw new IllegalArgumentException("Expects "
                    + world.getNumberOfAgents() + " agents: "
                    + agents.size() + " provided");
        world_ = world;
        agents_ = agents;
        totalMoveTimeData_ = new ArrayList<long[]>();
        totalRewardsData_ = new ArrayList<double[]>();
        actionCountsData_ = new ArrayList<int[]>();
        try {
            outputFile_ = new BufferedWriter(new FileWriter(fileName,true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        scoringFunction = new BoardScorer();
        this.frame_ = frame;
        this.drawer_ = drawer;
        this.list_ = list;
    }

    public Arbiter(Simulator<S, A> world, List<Agent> agents, String fileName) {
        if (world.getNumberOfAgents() != agents.size())
            throw new IllegalArgumentException("Expects "
                    + world.getNumberOfAgents() + " agents: "
                    + agents.size() + " provided");
        world_ = world;
        agents_ = agents;
        totalMoveTimeData_ = new ArrayList<long[]>();
        totalRewardsData_ = new ArrayList<double[]>();
        actionCountsData_ = new ArrayList<int[]>();
        try {
            outputFile_ = new BufferedWriter(new FileWriter(fileName,true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        scoringFunction = new BoardScorer();
        this.frame_ = null;
        this.drawer_ = null;
        this.list_ = null;

    }
    /**
     * Play a single game to the end.
     * 
     * @param simulatedWorld
     *            simulator used by agents to determine move.
     */
    private void runSimulation(Simulator<S, A> simulatedWorld,
            List<Agent> agents, int[] agentMoveOrder) {
        long[] totalMoveTime = new long[agents.size()];
        double[] totalRewards = new double[agents.size()];
        int[] actionCounts = new int[agents.size()];
        boolean suggestionGiven = false;
        String humanInput = "";
        world_.setInitialState();
        history_ = new History<S, A>(world_.getState());
        for (int i = 0; i < totalRewards.length; i++)
            totalRewards[i] = world_.getReward(i);
        /*added to handle the loopy behavior*/
        int singleActionCount = 0;
        int lastLeagalActionCount = simulatedWorld.getLegalActions().size();
        HashMap <Integer, S> exploredStates = new HashMap<>();
        if (this.drawer_ != null) {
            frame_.setTitle("Solution for "+ DrawGUI.getGameNumber());
            frame_.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }

        while (!world_.isTerminalState() && actionCounts[0] <= Main.MAX_ALLOWED_MOVES) {
            if (actionCounts[0] % 500 == 0) {
                System.out.println("actions taken till now: " + actionCounts[0]);
            }
            int hashKey = world_.getState().hashCode();
            if (exploredStates.containsKey(hashKey)){
                /*System.out.println("found same state");
                System.out.println(exploredStates.get(hashKey).toString());
                System.out.println(world_.getState().toString());*/

            }
            else{
                exploredStates.put(hashKey, world_.getState());
            }
            int agentTurn = world_.getState().getAgentTurn();
            long startTime = System.currentTimeMillis();
            /*if ( lastLeagalActionCount == simulatedWorld.getLegalActions().size() ) {
                singleActionCount++;
                if (singleActionCount == 5){

                    else {
                        //world_.legalActions_ = null;
                    }
                }
                lastLeagalActionCount = simulatedWorld.getLegalActions().size();
            }
            else {
                singleActionCount = 0;
                lastLeagalActionCount = simulatedWorld.getLegalActions().size();
            }*/

            //frame chat box code
            if (DrawGUI.chatTextToPlanner.size() > 0) {
                System.out.println(DrawGUI.chatTextToPlanner.get(0));
                DrawGUI.chatTextToPlanner.clear();
            }
            else {

            }
            /*System.out.println("legal actions");
            if (world_.legalActions_ != null) {
                int sz = world_.legalActions_.size();
                for (int i = 0; i < sz; ++i) {
                    System.out.println(world_.legalActions_.get(i));
                }
                System.out.println("");
            }*/
            /*if (freeCellFilterAgent != null) {
                List <FreeCellAction> freeCellActionList = null;
                freeCellActionList = (List<FreeCellAction>) freeCellFilterAgent.filter(simulatedWorld.copy().getLegalActions(),
                        simulatedWorld.copy().getState());
                if (freeCellActionList.size() == 0 ) {
                    simulatedWorld.legalActions_.clear();
                }
            }*/

            List<ActionOutcome> actionList = agents.get(agentTurn).selectActions(world_.getState(), simulatedWorld.copy());

            totalMoveTime[agentTurn] += System.currentTimeMillis() - startTime;
            //check if we are selecting same action
            //lastAction = (FreeCellAction) action;
            if (actionList.size() == 0 && world_.legalActions_.size() == 0) {
                System.out.println("no suitable action found!");
                break;
            }
            A action = null;
            //todo: before taking an action check the UCT returned action

            Simulator<S,A> dummySimulator = simulatedWorld.copy();
            dummySimulator.setState(world_.getState());
            if (actionList.size() == 1) {
                FreeCellAction faction = actionList.get(0).getAction();
                simulatedWorld.takeAction((A)actionList.get(0).getAction());

                int legalActionsSimulated = simulatedWorld.getLegalActions().size();
                if (faction.actionTypeId == 1) {
                    action = (A) actionList.get(0).getAction();
                }
                //if the no of possible actions reduced drastically!!
                /*else if ((legalActionsSimulated * 3) < world_.getLegalActions().size()) {
                    if (Main.SHOW_GUI) {
                        boolean loopContinue = true;
                        this.SendMessageToHuman("\n<Planner> : I need move suggestion!");
                        this.SendMessageToHuman(actionList);
                        while (loopContinue) {
                            synchronized (GUI_LOCK_SEND) {
                                //DrawGUI.sendButtonAction();
                                try {
                                    GUI_LOCK_SEND.wait();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                            if (Main.parser == null) {
                                try {
                                    Main.parser = new TrainedSemanticParser(new File("cardsModel"), new File("cards.types"), new File("cards.ont"));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            Arbiter.SendMessageToHuman(Main.parser.parseForPlanner(DrawGUI.chatTextToPlanner.get(0)));
                            //todo: convert the command to action
                            action = (A) getAction(Main.parser.parseForPlanner(DrawGUI.chatTextToPlanner.get(0)), world_);
                            FreeCellAction fca = (FreeCellAction) action;
                            DrawGUI.chatTextToPlanner.clear();
                            if (fca.cardSource() == -1) {
                                this.SendMessageToHuman("<Planner>  : which card you want to move? I found more than one possible action");
                                continue;
                            }
                            else {loopContinue = false;}

                            Arbiter.SendMessageToHuman(action.toString());
                            //System.out.println(action);
                            //System.out.println(Main.parser.parseForPlanner(humanInput));
                        }
                    }
                }*/
                else {
                    if (actionList.size() > 0) {
                        action = (A) actionList.get(0).getAction();
                    }
                }
            }
            else if (actionList.size() >=1 ) {
                boolean actionFound = false;
                List<Action> movePreference = new ArrayList<>();
                for (ActionOutcome actionOutcome : actionList) {

                    FreeCellAction freeCellAction =  actionOutcome.getAction();
                    if (freeCellAction.actionTypeId == 1 || freeCellAction.actionTypeId == 2) {
                        action = (A) actionList.get(0).getAction();
                        actionFound = true;
                        break;
                    }
                    else if (actionOutcome.getReward() >= 10000.0 ) {
                        Action preferAction = null;
                        if (freeCellAction.actionTypeId == 3) {
                            String[] moveParam = {"the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true),
                                    getCardSourceString(freeCellAction.cardSource(), true),
                                    getCardDestinationString(freeCellAction.cardDestination(), true)};
                            preferAction = new generator.Action(Predicate.MOVE);
                            preferAction.setEntity("the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(freeCellAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(freeCellAction.cardDestination(), true));
                        }
                        else if (freeCellAction.actionTypeId == 4) {
                            String[] moveParam = {"the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true),
                                    getCardSourceString(freeCellAction.cardSource(), true),
                                    getCardDestinationString(freeCellAction.cardDestination(), true)};
                            preferAction = new Action(Predicate.UNFREE);
                            preferAction.setEntity("the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(freeCellAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(freeCellAction.cardDestination(), true));
                        }
                        else if (freeCellAction.actionTypeId == 5) {
                            String[] moveParam = {"the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true),
                                    getCardSourceString(freeCellAction.cardSource(), true),
                                    getCardDestinationString(freeCellAction.cardDestination(), true)};
                            preferAction = new Action(Predicate.FREE);
                            preferAction.setEntity("the " + getRankString(freeCellAction.cardRank()) + " of " + getSuit(freeCellAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(freeCellAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(freeCellAction.cardDestination(), true));
                        }
                        movePreference.add(preferAction);
                    }
                }
                int moveCount = movePreference.size();
                if (moveCount > 1) actionFound = false;
                else if (moveCount == 1 && actionList.get(0).getAction().actionTypeId == 1) {
                    action = (A) actionList.get(0).getAction();
                    actionFound = true;
                }

                if (Main.SHOW_GUI && actionFound == false) {
                    boolean loopContinue = true;
                    if (moveCount > 1) {
                        this.SendMessageToHuman("\n<Planner> " + Gen.movePreference(movePreference));
                        /*this.SendMessageToHuman("\n<Planner> : I need move preference!");
                        this.SendMessageToHuman(movePreference);*/
                    }
                    else {
                        this.SendMessageToHuman("\n<Planner> " + Gen.movePreference(movePreference));
                    }
                    while (loopContinue) {
                        synchronized (GUI_LOCK_SEND) {
                            try {
                                GUI_LOCK_SEND.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (Main.parser == null) {
                            try {
                                Main.parser = new TrainedSemanticParser(new File("cardsModel"), new File("cards.types"), new File("cards.ont"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        //Arbiter.SendMessageToHuman(Main.parser.parseForPlanner(DrawGUI.chatTextToPlanner.get(0)));
                        //todo: convert the command to action
                        if (DrawGUI.chatTextToPlanner.size() > 0) {
                            humanInput = DrawGUI.chatTextToPlanner.get(0);
                        }
                        else {
                            DrawGUI.chatTextToPlanner.clear();
                            continue;
                        }
                        action = (A) getAction(Main.parser.parseForPlanner(humanInput), world_);
                        if (action == null && possibleCards.size() < 1) {
                            this.SendMessageToHuman("\n<Planner> " + Gen.invalidMove());
                            DrawGUI.chatTextToPlanner.clear();
                            continue;
                        }
                        //Arbiter.SendMessageToHuman(action.toString());

                        FreeCellAction fca = (FreeCellAction) action;
                        if (fca != null) {
                            fca.setActionSourcePlanner(false);
                        }
                        if (fca == null || fca.cardSource() == -1) {
                            //this.SendMessageToHuman("<Planner>  : which card you want to move? I got more than one.");
                            DrawGUI.chatTextToPlanner.clear();
                            //this.SendMessageToHuman("\n<Planner>  : I believe something is wrong");
                            continue;
                        }
                        else loopContinue = false;
                    }

                    //System.out.println(action);
                    //System.out.println(Main.parser.parseForPlanner(humanInput));
                }

            }
            else if (actionList.size() == 0) {
               FreeCellNode freeCellNode = (FreeCellNode) world_.getState();
                int score = freeCellNode.score();
                if (score == -30) {
                    this.SendMessageToHuman("\n<Planner> GAME OVER!! with " + history_.getSize() + " moves");
                    break;
                }
            }
            else {
                if (Main.SHOW_GUI) {
                    FreeCellNode freeCellNode = (FreeCellNode) world_.getState();
                    if (world_.isTerminalState() && freeCellNode.score() == -30) {
                        this.SendMessageToHuman("\n<Planner> GAME OVER!! with " + history_.getSize() + " moves");
                        break;
                    }
                    boolean loopContinue = true;
                    List<Action> movePreference = new ArrayList<>();
                    this.SendMessageToHuman("\n<Planner> " + Gen.movePreference(movePreference));
                    //this.SendMessageToHuman(actionList);
                    while (loopContinue) {
                        synchronized (GUI_LOCK_SEND) {
                            try {
                                GUI_LOCK_SEND.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (Main.parser == null) {
                            try {
                                Main.parser = new TrainedSemanticParser(new File("cardsModel"), new File("cards.types"), new File("cards.ont"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        //Arbiter.SendMessageToHuman(Main.parser.parseForPlanner(DrawGUI.chatTextToPlanner.get(0)));
                        //todo: convert the command to action
                        if (DrawGUI.chatTextToPlanner.size() > 0) {
                            humanInput = DrawGUI.chatTextToPlanner.get(0);
                        }
                        else {
                            continue;
                        }
                        action = (A) getAction(Main.parser.parseForPlanner(humanInput), world_);
                        if (action == null) {
                            //this.SendMessageToHuman("\n<Planner>  : " + Gen.invalidMove());
                            continue;
                        }
                        //DrawGUI.chatTextToPlanner.clear();
                        FreeCellAction fca = (FreeCellAction) action;
                        fca.setActionSourcePlanner(false);
                        if (fca.cardSource() == -1) {
                            //this.SendMessageToHuman("<Planner>  : which card you want to move?");
                            DrawGUI.chatTextToPlanner.clear();
                            continue;
                        }
                        else loopContinue = false;
                    }
                }
            }
            dummySimulator = world_.copy();
            FreeCellNode fcn = (FreeCellNode) dummySimulator.getState();
            FreeCellAction fca = (FreeCellAction) action;

            boolean isValidAction = fca.isValid(fcn);
            boolean betterActionAvailable = false;
            //in valid move
            if (!isValidAction && fca != null) {
                this.SendMessageToHuman("\n<Planner> " + Gen.invalidMove());
                continue;
            }
            else {
                int actionsCount = actionList.size();
                List<Action> movePreference = new ArrayList<>();
                for (int i = 0; i< actionsCount; ++i) {
                    FreeCellAction listAction = actionList.get(i).getAction();
                    double actionReward = actionList.get(i).getReward();
                    if (listAction.cardRank() == ((FreeCellAction) action).cardRank()
                            && listAction.cardSuit() == ((FreeCellAction) action).cardSuit()
                            && listAction.cardSource() == ((FreeCellAction) action).cardSource()
                            && listAction.cardDestination() == ((FreeCellAction) action).cardDestination()) {
                        //this.SendMessageToHuman("\n<Planner>  : I same action" + ((FreeCellAction) action).cardRank());

                    }
                    else  if (actionReward >= 100000 && ((FreeCellAction) action).actionSourcePlanner == false){
                        betterActionAvailable = true;
                        Action preferAction = null;
                        if (listAction.actionTypeId == 3) {
                            String[] moveParam = {"the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true),
                                    getCardSourceString(listAction.cardSource(), true),
                                    getCardDestinationString(listAction.cardDestination(), true)};
                            preferAction = new generator.Action(Predicate.MOVE);
                            preferAction.setEntity("the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(listAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(listAction.cardDestination(), true));
                        }
                        else if (listAction.actionTypeId == 4) {
                            String[] moveParam = {"the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true),
                                    getCardSourceString(listAction.cardSource(), true),
                                    getCardDestinationString(listAction.cardDestination(), true)};
                            preferAction = new Action(Predicate.UNFREE);
                            preferAction.setEntity("the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(listAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(listAction.cardDestination(), true));
                        }
                        else if (listAction.actionTypeId == 5) {
                            String[] moveParam = {"the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true),
                                    getCardSourceString(listAction.cardSource(), true),
                                    getCardDestinationString(listAction.cardDestination(), true)};
                            preferAction = new Action(Predicate.FREE);
                            preferAction.setEntity("the " + getRankString(listAction.cardRank()) + " of " + getSuit(listAction.cardSuit(), true));
                            preferAction.setSource(getCardSourceString(listAction.cardSource(), true));
                            preferAction.setDestination(getCardDestinationString(listAction.cardDestination(), true));
                        }
                        movePreference.add(preferAction);

                        //break;
                    }
                }
                if (betterActionAvailable && !suggestionGiven) {

                    this.SendMessageToHuman("\n<Planner> " + Gen.badReward(movePreference));
                    DrawGUI.chatTextToPlanner.clear();
                    suggestionGiven = true;
                    A internalAction = null;
                    boolean loopContinue = true;
                    while (loopContinue) {
                        synchronized (GUI_LOCK_SEND) {
                            try {
                                GUI_LOCK_SEND.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (DrawGUI.chatTextToPlanner.size() > 0) {
                            humanInput = DrawGUI.chatTextToPlanner.get(0);
                        }
                        else {
                            continue;
                        }
                        internalAction = (A) getAction(Main.parser.parseForPlanner(humanInput), world_);
                        if (internalAction == null && possibleCards.size() < 1) {
                            this.SendMessageToHuman("\n<Planner> " + Gen.invalidMove());
                            DrawGUI.chatTextToPlanner.clear();
                            continue;
                        }
                        //Arbiter.SendMessageToHuman(action.toString());

                        FreeCellAction fcAction = (FreeCellAction) internalAction;
                        if (fcAction != null) {
                            fcAction.setActionSourcePlanner(false);
                        }
                        if (fcAction == null || fcAction.cardSource() == -1) {
                            //this.SendMessageToHuman("<Planner>  : which card you want to move? I got more than one.");
                            DrawGUI.chatTextToPlanner.clear();
                            //this.SendMessageToHuman("\n<Planner>  : I believe something is wrong");
                            continue;
                        }
                        else {
                            loopContinue = false;
                            Action semanticAction = new Action(Predicate.MOVE);
                            semanticAction.setEntity(fcAction.getCardString());
                            semanticAction.setSource(fcAction.getSrcString());
                            semanticAction.setDestination(fcAction.getDstString());
                            this.SendMessageToHuman("\n<Planner> " + Gen.movePerformed(semanticAction));
                            action = (A) fcAction;
                        }

                    }
                    //continue;
                }
                else {
                    String[] actionObject = {fca.getCardString(), fca.getSrcString(), fca.getDstString()};
                    Action semanticAction = new Action(Predicate.MOVE);
                    semanticAction.setEntity(fca.getCardString());
                    semanticAction.setSource(fca.getSrcString());
                    semanticAction.setDestination(fca.getDstString());
                    if (fca.actionSourcePlanner == false)
                        this.SendMessageToHuman("\n<Planner> " + Gen.movePerformed(semanticAction));
                }
            }

            world_.takeAction(action);
            String semanticRepresentation =  ((FreeCellAction) action).getSemanticRep();
            ((FreeCellAction) action).setSemanticString(semanticRepresentation);
            DrawGUI.chatTextToPlanner.clear();
            try {
                Thread.sleep(2000);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            System.out.println("state:\n" + world_.getState());
            //System.out.println("reward: " + world_.getReward(0));
            for (int i = 0; i < totalRewards.length; i++)
                totalRewards[i] += world_.getReward(i);
            actionCounts[agentTurn]++;
            history_.add(world_.getState(), action);
            hist.add((FreeCellNode)world_.getState(), (FreeCellAction)action);
            if (this.drawer_ != null) {
                this.UpdateGUI((FreeCellNode) world_.getState());
            }


        }        
        long[] totalMoveTime2 = new long[agents.size()];
        double[] totalRewards2 = new double[agents.size()];
        int[] actionCounts2 = new int[agents.size()];
        for (int i = 0; i < agents_.size(); i++) {
            totalMoveTime2[i] = totalMoveTime[agentMoveOrder[i]];
            totalRewards2[i] = totalRewards[agentMoveOrder[i]];
            actionCounts2[i] = actionCounts[agentMoveOrder[i]];
        }
        totalMoveTimeData_.add(totalMoveTime2);
        totalRewardsData_.add(totalRewards2);
        actionCountsData_.add(actionCounts2);
        
        //Record history
        try {
            FileWriter fw = new FileWriter(new File(Main.CURRENT_GAME +".game"));
            BufferedWriter bw = new BufferedWriter(fw);
			//FileOutputStream fileOut = new FileOutputStream("last.game");
			//ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			
			ArrayList<String> agent_names = new ArrayList<String>();
			for (int i = 0; i < agents.size(); i++) {
				agent_names.add(agents_.get(i).name_);
			}
			//objectOut.writeObject(agent_names);
            bw.write(agent_names.get(0) + "\n");
            System.out.println("Total action count: " + history_.getSize());
            outputFile_.write(Main.CURRENT_GAME + "," + history_.getSize() + ",");
            FreeCellNode fcn = (FreeCellNode) world_.getState();
            int scoreOfFinalState = scoringFunction.eval(fcn);
            if (scoreOfFinalState == -30) {
                outputFile_.write("solved,");
            }
            else {
                outputFile_.write("not_solved,");
            }
			for (int i = 1; i < history_.getSize(); i++) {
				State state = history_.getState(i);
				A a = history_.getAction(i);
				//System.out.println(a);
                //System.out.println(state);
                bw.write(state.toString() + "\n");
				bw.write(a.toString() + "\n");
			}
			
			bw.close();


		} catch (FileNotFoundException e) {
			System.err.println("This exception doesn't even make any sense");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
    }

    /*private String[] convertActionForGenerator(A action) {
        String[] splittedPortion = new String[3];
        FreeCellAction fca = (FreeCellAction) action;
        String rankStr = getRankString(fca.cardRank());

        //from freecell to column
        if (fca.cardSource() == -1) {

        }
        else if (fca.cardDestination() == -1) {

        }
        else if (fca.cardSource() >= 0 && fca.cardDestination() >=0) {
            this.SendMessageToHuman("\n<Planner>  : " + Gen.movePerformed("the " + fca.cardRank() + " of " + fca.cardSuit() ,
                    getCardSourceString(fca.cardSource(), true), getCardDestinationString(fca.cardDestination(), true)));

        }
        return splittedPortion;
    }*/

    private void SendMessageToHuman(List<A> legalActions) {
        for (A action : legalActions) {
            SendMessageToHuman("\n" + action.toString());
        }
    }

    public static void SendMessageToHuman(String message) {
        DrawGUI.appendToPane(DrawGUI.getChatBox(), message, false, Color.BLACK);
        //DrawGUI.getChatBox().append(message);
    }

    public void UpdateGUI (FreeCellNode fcn) {

        History<FreeCellNode, FreeCellAction> _history = new History<>();
        for (int i = 0; i < history_.getSize(); ++i) {
            _history.add((FreeCellNode)history_.getState(i), (FreeCellAction)history_.getAction(i));
        }
        Stack<IMove> st = new Stack<>();
        for (int i = 0; i < history_.getSize(); ++i) {
            st.push(_history.getAction(i));
        }
        //FreeCellNode fcn;

        DefaultListModel dlm = new DefaultListModel();
        while (!st.isEmpty()) {
            dlm.add(0, st.pop());
        }
/*          FreeCellAction freeCellAction = (FreeCellAction) st.peek();
            if (freeCellAction != null) {
                dlm.add(0, freeCellAction.getSemanticString());
            }
            st.pop();
* */


        list_.setModel(dlm);
        list_.setSelectedIndex(0);
        if (listener != null) {
            list_.removeListSelectionListener(listener);
        }

        listener = new StateModifier(list_, fcn, drawer_, _history);
        list_.addListSelectionListener(listener);
        frame_.setCursor(Cursor.getDefaultCursor());

        drawer_.setNode (fcn);
        drawer_.repaint();

    }

    public void runSimulations(Simulator<S, A> simulatedWorld, int numTrials) {
        int moveOrderDisplacement = 0;
        int[] agentMoveOrder = new int[agents_.size()];
        List<Agent> agents = new ArrayList<Agent>();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < agents_.size(); i++)
            agents.add(agents_.get(i));
        for (int i = 0; i < numTrials; i++) {
        	System.out.println("Game " + i );
        	// Rotate Agent Order
            for (int j = 0; j < agentMoveOrder.length; j++)
                agentMoveOrder[j] = (j + moveOrderDisplacement)
                        % agents_.size();
            runSimulation(simulatedWorld.copy(), agents, agentMoveOrder);
            agents.add(0, agents.remove(agents.size() - 1));
            moveOrderDisplacement += 1;
        }
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.println(duration + "ms");
        try {
            outputFile_.write(duration + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Simulator<S, A> getSimulator() {
        return world_;
    }

    public List<Agent> getAgents() {
        return agents_;
    }

    public History<S, A> getHistory() {
        return history_;
    }

    public double[] getRewardsData(int agentId) {
        double[] rewardsData = new double[totalRewardsData_.size()];
        for (int i = 0; i < totalRewardsData_.size(); i++)
            rewardsData[i] = totalRewardsData_.get(i)[agentId];
        return rewardsData;
    }
    
    public double getRewardsSum(int agentId) {
    	double total = 0;
    	for (int i = 0; i < totalRewardsData_.size(); i++) {
    		total += totalRewardsData_.get(i)[agentId];
    	}
    	return total;
    }

    public double[] getAvgMoveTimeData(int agentId) {
        double[] avgMoveTimeData = new double[totalRewardsData_.size()];
        for (int i = 0; i < totalRewardsData_.size(); i++) {
            avgMoveTimeData[i] = totalMoveTimeData_.get(i)[agentId]
                    / (double) actionCountsData_.get(i)[agentId];
        }
        return avgMoveTimeData;
    }
    
    public int getNumWins(int agentId) {
    	int totalWins = 0;
    	for (int i = 0; i < totalRewardsData_.size(); i++) {
    		double[] rewards = totalRewardsData_.get(i);
    		int maxid = 0;
    		for (int j = 1; j < agents_.size(); j++) {
    			if (rewards[j] > rewards[maxid]) {
    				maxid = j;
    			}
    		}
    		if (maxid == agentId && rewards[maxid] > 0) totalWins++;
    	}
    	return totalWins;
    }

    public boolean CloseResultFile () {
        try {
            outputFile_.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    public FreeCellAction getAction (String command, Simulator simulator) {
        String ranks = ".A23456789TJQK";
        String suits = "CDHS";
        String tempString  = "";
        FreeCellAction fca = null;
        command = command.replace("(","").replace(")","");
        String[] splitted = command.split(" ");
        FreeCellNode tempState = (FreeCellNode) simulator.getState();
        Column[] cols = tempState.getCols();

        //(topCard st0 h12)^(move st0 h13)
        //move the stack below the queen of hearts to the king of hearts
        if (command.contains("^")) {
            String[] splittedCommands = command.split("\\^");
            String[] firstPartSplitted =  splittedCommands[0].replace("(", "").replace(")","").split(" ");
            String[] secondPartSplitted =  splittedCommands[1].replace("(","").replace(")","").split(" ");
            //todo: find the source column
            int sourceCol = -1;
            String sourceCard = firstPartSplitted[2];
            int sourceCardInt = encodeStringToCard(sourceCard);
            String rankStr = getRankString(getRank(sourceCardInt));
            String suitStr = getSuit(sourceCardInt);
            //todo: find destincation column
            int destCol = -1;
            int cardCount = 0;
            String destinationCard = secondPartSplitted[2];
            int destinationCardInt = encodeStringToCard(destinationCard);
            for (int i = 0; i < cols.length; ++i) {
                Column col = cols[i];
                int j = 0;
                while (col.cards[j] > 0 && j < 20) {
                    if (col.cards[j] == sourceCardInt) {
                        sourceCol = i;
                        cardCount++;
                        j++;
                        //todo: how many cards
                        while (col.cards[j] > 0 && j < 20) {
                            cardCount++;
                            j++;
                        }
                    }
                    else if (col.cards[j] == destinationCardInt) {
                        destCol = i;
                    }
                    j++;
                }
            }
            fca = new ColumnToColumnMove(sourceCol, destCol, cardCount);
            fca.setCardString("the " + rankStr + " of " + suitStr);
            fca.setSrcString(getCardSourceString(fca.cardSource(), true));
            fca.setDstString(getCardDestinationString(fca.cardDestination(), true));
            /*String[] actionObject = {"the " + rankStr + " of " + suitStr , getCardSourceString(fca.cardSource(), true),
                    getCardDestinationString(fca.cardDestination(), true)};
            Action action = new Action(Predicate.MOVE, actionObject);
            this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(action));*/


        }
        //column to column move
        else if (command.contains("move") && !command.contains("?")
                && !command.contains("fr") && !command.contains("fo")
                && !command.contains("st")) {
            //detect the source column
            String sourceCard = splitted[1];
            String destinationCard = splitted[2];
            int sourceCardInt = encodeStringToCard(sourceCard);
            int destinationCardInt = encodeStringToCard(destinationCard);
            int sourceCol = -1;
            int destCol = -1;
            String rankStr = getRankString(getRank(sourceCardInt));
            String suitStr = getSuit(sourceCardInt);

            for (int i = 0; i < cols.length; ++i) {
                Column col = cols[i];
                int j = 0;
                while (col.cards[j] > 0 && j < 20) {
                    if (col.cards[j] == sourceCardInt) {
                        sourceCol = i;
                    }
                    else if (col.cards[j] == destinationCardInt) {
                        destCol = i;
                    }
                    j++;
                }
            }
            //System.out.println("source col: " + sourceCol + " dest: " + destCol);
            fca = new ColumnToColumnMove(sourceCol, destCol, 1);
            fca.setCardString("the " + rankStr + " of " + suitStr);
            fca.setSrcString(getCardSourceString(fca.cardSource(), true));
            fca.setDstString(getCardDestinationString(fca.cardDestination(), true));
            /*String[] actionObject = {"the " + rankStr + " of " + suitStr , getCardSourceString(fca.cardSource(), true),
                    getCardDestinationString(fca.cardDestination(), true)};
            Action action = new Action(Predicate.MOVE, actionObject);
            this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(action));*/
            /*this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed("the " + rankStr + " of " + suitStr ,
                    getCardSourceString(fca.cardSource(), true), getCardDestinationString(fca.cardDestination(), true)));*/
        }
        //column to freecell move
        else if (command.contains("move") && command.contains("fr")) {
            String sourceCard = splitted[1];
            boolean foundOne = false;
            int rank = -1;
            String rankStr = "";
            String suitStr = "";
            int suitValue = 0;
            if (sourceCard.length() > 2) {
                rank = (sourceCard.charAt(1) - '0') * 10 + (sourceCard.charAt(2) - '0');
            }
            else {
                rank = (sourceCard.charAt(1)- '0');
            }
            rankStr = getRankString(rank);
            if (sourceCard.charAt(0) != '?') {
                int suit = sourceCard.charAt(0);
                int suitIndex = -1;
                for (int i = 0; i < 4; ++i) {
                    if (suits.charAt(i) - 'A' == suit - 'a') {
                        suitIndex = i;
                        suitValue = i;
                        break;
                    }
                }
            }

            int cardInt = (rank - 1) * 4 + 1 + suitValue;
            int sourceCol = -1;
            possibleCards.clear();
            for (int i = 0; i < cols.length; ++i) {
                Column col = cols[i];
                int j = 0;
                while (col.cards[j] > 0 && j < 20) {
                    if (col.cards[j] > 0) {
                        j++;
                    }
                }
                if (j ==0) continue;
                --j;
                if (sourceCard.charAt(0) == '?') {
                    if (col.cards[j] >= ((rank - 1) * 4 + 1) && col.cards[j] < ((rank - 1) * 4 + 1) + 4) {
                        //if (!foundOne) {
                            sourceCol = i;
                            foundOne = true;
                            suitStr = getSuit(col.cards[j]);
                            String str = new String("the " + rankStr + " of " + suitStr);
                            possibleCards.add(str);
                        //}
                        /*else {
                            this.SendMessageToHuman("<Planner>  : which card you want to move?");
                            return new ColumnToFreeMove(-1);
                        }*/
                    }
                }
                else {
                    if (col.cards[j] == cardInt) {
                        sourceCol = i;
                        suitStr = getSuit(col.cards[j]);
                        break;
                    }
                }
            }
            if (possibleCards.size() == 2) {
                sourceCol = -1;
                this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1)));
            }
            else if (possibleCards.size() == 3) {
                sourceCol = -1;
                this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2)));
            }
            else if (possibleCards.size() == 4) {
                sourceCol = -1;
                this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2), possibleCards.get(3)));
            }


            if (sourceCol != -1) {
                fca = new ColumnToFreeMove(sourceCol);
                fca.setCardString("the " + rankStr + " of " + suitStr);
                fca.setSrcString(getCardSourceString(fca.cardSource(), true));
                fca.setDstString(getCardDestinationString(fca.cardDestination(), true));
                /*String[] actionObject = {"the " + rankStr + " of " + suitStr , getCardSourceString(fca.cardSource(), true),
                        getCardDestinationString(fca.cardDestination(), true)};
                Action action = new Action(Predicate.FREE, actionObject);
                this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(action));*/
                /*this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(" the " + rankStr + " of " + suitStr ,
                        getCardDestinationString(fca.cardDestination()), getCardSourceString(fca.cardSource())));*/

            }
            else {
                return null;
            }


        }
        //column to foundation move
        else if (command.contains("move") && command.contains("fo")) {
            String sourceCard = splitted[1];
            boolean foundOne = false;
            int rank = -1;
            int suitValue = 0;
            String rankStr = "";
            String suitStr = "";
            possibleCards.clear();
            if (sourceCard.length() > 2) {
                rank = (sourceCard.charAt(1) - '0') * 10 + (sourceCard.charAt(2) - '0');
            }
            else {
                rank = (sourceCard.charAt(1)- '0');
            }
            rankStr = getRankString(rank);
            if (sourceCard.charAt(0) != '?') {
                int suit = sourceCard.charAt(0);
                int suitIndex = -1;
                for (int i = 0; i < 4; ++i) {
                    if (suits.charAt(i) - 'A' == suit - 'a') {
                        suitIndex = i;
                        suitValue = i;
                        break;
                    }
                }
            }

            int cardInt = (rank - 1) * 4 + 1 + suitValue;
            int sourceCol = -1;

            for (int i = 0; i < cols.length; ++i) {
                Column col = cols[i];
                int j = 0;
                while (col.cards[j] > 0 && j < 20) {
                    if (col.cards[j] > 0) {
                        j++;
                    }
                }
                --j;
                if (sourceCard.charAt(0) == '?') {
                    if (col.cards[j] >= ((rank - 1) * 4 + 1) && col.cards[j] < ((rank - 1) * 4 + 1) + 4) {
                        //if (!foundOne) {
                            sourceCol = i;
                            foundOne = true;
                            suitStr = getSuit(col.cards[j]);
                            String str = new String("the " + rankStr + " of " + suitStr);
                            possibleCards.add(str);

                        /*} else {
                            return new ColumnToFoundationMove(-1);
                        }*/
                    }
                }
                else{
                    if (col.cards[j] == cardInt) {
                        sourceCol = i;
                        break;
                    }
                }

            }
            if (possibleCards.size() > 1) {
                sourceCol = -1;
            }
            fca = new ColumnToFoundationMove(sourceCol);
            boolean validAction = fca.isValid(tempState);
            if (validAction) {
                if (sourceCol != -1) {
                    fca.setCardString("the " + rankStr + " of " + suitStr);
                    fca.setSrcString(getCardSourceString(fca.cardSource(), true));
                    fca.setDstString(getCardDestinationString(fca.cardDestination(), true));
                    /*String[] actionObject = {"the " + rankStr + " of " + suitStr};
                    Action action = new Action(Predicate.MOVE, actionObject);
                    this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(action));*/
                    /*this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(" the " + rankStr + " of " + suitStr,
                            getCardSourceString(fca.cardSource()), getCardDestinationString(fca.cardDestination())));*/
                }
            }
            else {
                if (possibleCards.size() ==2) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1)));
                }
                else if (possibleCards.size() ==3) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2)));
                }
                else if (possibleCards.size() ==4) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2), possibleCards.get(3)));
                }
            }

        }
        //freecell to column move
        else if (command.contains("move") && command.contains("st")) {
            short[] encoding = tempState.getFreeEncoding();
            boolean foundOne = false;
            String rankStr = "";
            String suitStr = "";
            possibleCards.clear();
            String sourceCard = splitted[1];
            int rank = -1;
            int rankIndex = -1;
            int suitValue = 0;

            if (sourceCard.charAt(0) != '?') {
                int suit = sourceCard.charAt(0);
                int suitIndex = -1;
                for (int i = 0; i < 4; ++i) {
                    if (suits.charAt(i) - 'A' == suit - 'a') {
                        suitIndex = i;
                        suitValue = i;
                        break;
                    }
                }
            }
            if (sourceCard.length() > 2) {
                rank = (sourceCard.charAt(1) - '0') * 10 + (sourceCard.charAt(2) - '0');
            }
            else {
                rank = (sourceCard.charAt(1)- '0');
            }
            rankStr = getRankString(rank);
            int sourceCol = -1;
            if (sourceCard.charAt(0) == '?') {
                for (short c = 0; c < 8; c++) {
                    for (short i = 0; i < 4; i++) {
                        if (encoding[i] >= rank && encoding[i] <= rank + 4) {
                            fca = new FreeToColumnMove(c, encoding[i]);
                            if (fca.isValid(tempState)) {
                                suitStr = getSuit(encoding[i]);
                                String str = new String("the " + rankStr + " of " + suitStr);
                                possibleCards.add(str);
                                //if (!foundOne) {
                                    //fca = new FreeToColumnMove(c, encoding[i]);
                                /*}
                                else {
                                    return new FreeToColumnMove((short)(-1), encoding[i]);
                                }*/
                            }
                        }
                    }
                }
            }
            else {
                int cardInt = (rank - 1) * 4 + 1 + suitValue;
                for (short c = 0; c < 8; c++) {
                    for (short i = 0; i < 4; i++) {
                        if (encoding[i] == cardInt) {
                            fca = new FreeToColumnMove(c, encoding[i]);
                            if (fca.isValid(tempState)) {
                                break;
                            }
                        }
                    }
                }

            }
            if (possibleCards.size() >1) {
                sourceCol = -1;
            }
            boolean validAction = fca.isValid(tempState);
            if (validAction) {
                fca.setCardString("the " + rankStr + " of " + suitStr);
                fca.setSrcString(getCardSourceString(fca.cardSource(), true));
                fca.setDstString(getCardDestinationString(fca.cardDestination(), true));
                /*String[] actionObject = {"the " + rankStr + " of " + suitStr};
                Action action = new Action(Predicate.UNFREE, actionObject);
                this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(action));*/
                /*this.SendMessageToHuman("<Planner>  : " + Gen.movePerformed(" the " + rankStr + " of " + suitStr,
                        getCardSourceString(fca.cardSource()), getCardDestinationString(fca.cardDestination())));*/
            }
            else {
                if (possibleCards.size() ==2) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1)));
                }
                else if (possibleCards.size() ==3) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2)));
                }
                else if (possibleCards.size() ==4) {
                    this.SendMessageToHuman("\n<Planner> " + Gen.ambiguousTarget(rankStr, possibleCards.get(0), possibleCards.get(1), possibleCards.get(2), possibleCards.get(3)));
                }
            }
        }
        else {
            return null;
        }

        return fca;
    }
    public int encodeStringToCard (String card) {
        String ranks = ".A23456789TJQK";
        String suits = "CDHS";
        int suit = card.charAt(0);
        int suitIndex = -1;
        for (int i = 0; i < 4; ++i) {
            if (suits.charAt(i) - 'A' == suit - 'a') {
                suitIndex = i;
                break;
            }
        }
        int rank = -1;
        int rankIndex = -1;
        if (card.length() > 2) {
            rank = (card.charAt(1) - '0') * 10 + (card.charAt(2) - '0');
        }
        else {
            rank = (card.charAt(1)- '0');
            rankIndex = ranks.indexOf(card.charAt(1));

        }

        int cardInt = (rank - 1) * 4 + 1;
        cardInt += suitIndex;
        //int cardIntN = (sui)

        return  cardInt;
    }

    public int getRank (int rank) {
        return  1 + ((rank-1)>>2);  // rank extracted this way.return 0;

    }
    public String getSuit (int card) {
        String suits = "CDHS";
        int ss = ((card-1)%4);
        if (ss < 0) {
            char x = 'A';
        }
        char suitChar = suits.charAt(ss);
        if (suitChar == 'C') {
            return new String("clubs");
        }
        else if (suitChar == 'D') {
            return new String("diamonds");
        }
        else if (suitChar == 'H') {
            return new String("hearts");
        }
        else if (suitChar == 'S') {
            return new String("spades");
        }
        return "";
    }
    public String getSuit (int card, boolean flag) {
        String suits = "CDHS";
        if (card >=0 && card < 4) {
            char suitChar = suits.charAt(card);
            if (suitChar == 'C') {
                return new String("clubs");
            } else if (suitChar == 'D') {
                return new String("diamonds");
            } else if (suitChar == 'H') {
                return new String("hearts");
            } else if (suitChar == 'S') {
                return new String("spades");
            }
        }
        return "";
    }
    public String getCardSourceString (int source) {
        if (source >= 0) {
            String string  = "";
            if (source == 0) {
                string = "first";
            } else if (source == 1) {
                string = "second";
            } else if (source == 2) {
                string = "third";
            } else if (source == 3) {
                string = "fourth";
            } else if (source == 4) {
                string = "fifth";
            } else if (source == 5) {
                string = "sixth";
            } else if (source == 6) {
                string = "seventh";
            } else if (source == 7) {
                string = "eighth";
            }
            return " the " +  string +" stack ";
        }
        else if (source == -1) {
            return " the freecell";
        }
        else return "";
    }
    public String getCardSourceString (int source, boolean showColumn) {
        if (source >= 0) {
            String string  = "";
            if (source == 0) {
                string = "first";
            } else if (source == 1) {
                string = "second";
            } else if (source == 2) {
                string = "third";
            } else if (source == 3) {
                string = "fourth";
            } else if (source == 4) {
                string = "fifth";
            } else if (source == 5) {
                string = "sixth";
            } else if (source == 6) {
                string = "seventh";
            } else if (source == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";
        }
        else if (source == -1) {
            return " the freecell";
        }
        else return "";
    }

    public String getCardDestinationString (int destination) {
        if (destination == -1) {
            return " the freecell";
        }
        else if (destination == -2) {
            return " a homecell";
        }
        else if (destination >=0 ) {
            String string  = "";
            if (destination == 0) {
                string = "first";
            } else if (destination == 1) {
                string = "second";
            } else if (destination == 2) {
                string = "third";
            } else if (destination == 3) {
                string = "fourth";
            } else if (destination == 4) {
                string = "fifth";
            } else if (destination == 5) {
                string = "sixth";
            } else if (destination == 6) {
                string = "seventh";
            } else if (destination == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";// + (destination + 1) + " ";
        }

        else return "";
    }
    public String getCardDestinationString (int destination, boolean showColumn) {
        if (destination == -1) {
            return " the freecell";
        }
        else if (destination == -2) {
            return " a homecell";
        }
        else if (destination >=0 ) {
            String string  = "";
            if (destination == 0) {
                string = "first";
            } else if (destination == 1) {
                string = "second";
            } else if (destination == 2) {
                string = "third";
            } else if (destination == 3) {
                string = "fourth";
            } else if (destination == 4) {
                string = "fifth";
            } else if (destination == 5) {
                string = "sixth";
            } else if (destination == 6) {
                string = "seventh";
            } else if (destination == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";// + (destination + 1) + " ";
        }

        else return "";
    }

    public String getRankString (int str) {

        if (str == 1) {
            return new String("ace");
        }
        else if (str == 2) {
            return new String("two");
        }
        else if (str == 3) {
            return new String("three");
        }
        else if (str == 4) {
            return new String("four");
        }
        else if (str == 5) {
            return new String("five");
        }
        else if (str == 6) {
            return new String("six");
        }
        else if (str == 7) {
            return new String("seven");
        }
        else if (str == 8) {
            return new String("eight");
        }
        else if (str == 9) {
            return new String("nine");
        }
        else if (str == 10) {
            return new String("ten");
        }
        else if (str == 11) {
            return new String("jack");
        }
        else if (str == 12) {
            return new String("queen");
        }
        else if (str == 13) {
            return new String("king");
        }
        return "";

    }


    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        output.append(world_.toString() + "\n");
        for (int i = 0; i < agents_.size(); i++)
            output.append(getRewardsSum(i) + " : " + agents_.get(i).toString() + "\n");
        return output.toString();
    }
}
